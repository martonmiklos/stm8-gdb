/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/*****************************************************************************
|*
|*  VERSION CONTROL:    @(#)gdi.h	1.90 97/12/24
|*
|*  COPYRIGHT:          Copyright (c) 1995--1997 TASKING Inc.
|*
|*			Based on the ESPRIT 7325 OMI/DEBUG
|*			Generic Debug Instrument Interface specification.
|*                      (GDI).
|*
 ****************************************************************************/


#ifndef _GDI_H
#define _GDI_H

#define DI_GDI_VERSION	0x0126		/* GDI revision level */

#ifdef WIN32
	#ifdef _OMIGDI
//		#define OmiGdiExport __declspec(dllexport)
		#define OmiGdiExport __declspec(dllexport) WINAPI
	#else
		#define OmiGdiExport __declspec(dllimport) WINAPI
	#endif	// _OMIGDI
#else
	#define OmiGdiExport			// Win16 <--> .def
#endif // WIN32

/*
 * DiValueT size -- currently 64 bits
 */
#define cbDiValueT	8

/*
 * DiMemT size -- currently hard linked to DiValueT's size
 */
#define cbDiMaxBitsPerMemT	64
#define cbDiMaxUCharTsPerMemT	((cbDiMaxBitsPerMemT+7)/8)


/*
 * Data Representation:
 *
 * The data representation is defined by the native data representation
 * of the system on which the debugger is installed. The debug instrument
 * interface should convert data representations where apropriate.
 */

/* Integral Data Types */
#if (!defined(_M_I386) && defined(MSDOS)) || defined(_M_I8086_) || defined(_M_I80286_)
#define __BIT16
#elif defined(__osf__) && defined(__alpha)
#define	__BIT64		/* Alpha machines, 64 bit */
#else
#define __BIT32		/* most UNIX, VMS */
#endif


#ifdef __BIT16
typedef unsigned char	DiBoolT;
typedef unsigned char	DiUCharT;
typedef unsigned 	DiUInt16T;
typedef signed long 	DiInt32T;
typedef unsigned long 	DiUInt32T;
#else
#ifdef	__BIT64
typedef unsigned char	DiBoolT;
typedef unsigned char	DiUCharT;
typedef unsigned short	DiUInt16T;
typedef signed int	DiInt32T;
typedef unsigned int	DiUInt32T;
#else	/* __BIT32 */
typedef unsigned char	DiBoolT;
typedef unsigned char	DiUCharT;
typedef unsigned short	DiUInt16T;
typedef signed long	DiInt32T;
typedef unsigned long	DiUInt32T;
#endif
#endif


/* DiAbsFileT */
typedef DiUInt32T	DiAbsFileT, *pDiAbsFileT;

#define DI_ABSF_NONE			0x0000
#define DI_ABSF_FILENAME		0x0001
#define DI_ABSF_TIOF695			0x0002
#define DI_ABSF_HPMRI695		0x0004
#define DI_ABSF_MOTOROLA_S		0x0008
#define DI_ABSF_INTEL_HEX		0x0010
#define DI_ABSF_TEKTRONIX_HEX		0x0020
#define DI_ABSF_EXT_TEKTRONIX_HEX	0x0040
#define DI_ABSF_BINARY			0x0080


/* DiAccessT */
typedef DiUInt32T	DiAccessT, *pDiAccessT;

#define DI_ACC_NONE			0x0000
#define DI_ACC_EXECUTE			0x0001
#define DI_ACC_READ			0x0002
#define DI_ACC_WRITE			0x0004


typedef DiUInt32T	DiMemSpaceT;	/* for memory space identifiers */


typedef struct
{
    DiUCharT		val[cbDiValueT];
} DiValueT, *pDiValueT;
/* Note that sizeof(DiValueT) can be larger than cbDiValueT because of alignment */


/* DiLAddrT */
typedef struct
{
    DiValueT		v;
} DiLAddrT, *pDiLAddrT;


/* DiAddrT */
typedef struct
{
    DiMemSpaceT		dmsMemSpace;
    DiLAddrT		dlaLinAddress;
} DiAddrT, *pDiAddrT;

/* DiAddrRangeT */
typedef struct
{
    DiAddrT		daStart;
    DiUInt32T		dnSize;
} DiAddrRangeT, *pDiAddrRangeT;

/* DiAppliIoErrorT */
typedef enum
{
    DI_APPLI_IO_NO_ERROR			= 0,
    DI_APPLI_IO_UNKNOWN_ERROR		= 1,
    DI_APPLI_IO_END_OF_FILE			= 2,
    DI_APPLI_IO_BAD_CHANNEL			= 3,
    DI_APPLI_IO_CHANNEL_DISABLED	= 4
} DiAppliIoErrorT, *pDiAppliIoErrorT;

/* DiBackGroundT */
typedef struct
{
    DiBoolT		fBgSupport;
    DiMemSpaceT	dmsMemSpace;
} DiBackGroundT, *pDiBackGroundT;


/* DiBpCommandT */
typedef enum
{
    DI_BREAKPOINT_SET			= 0,
    DI_BREAKPOINT_REMOVE		= 1
} DiBpCommandT, *pDiBpCommandT;


/* DiBpMethodT */
typedef enum 
{
    DI_BPM_HARDWARE			= 0,
    DI_BPM_SOFTWARE			= 1,
    DI_BPM_NONE				= 2
} DiBpMethodT, *pDiBpMethodT;


/* DiBpTypeT */
typedef DiUInt32T	 DiBpTypeT, *pDiBpTypeT;

#define DI_BPT_EXECUTE			0x0001
#define DI_BPT_READ				0x0002
#define DI_BPT_WRITE			0x0004
#define DI_BPT_VALUE			0x0008
#define DI_BPT_TIME				0x0010
#define DI_BPT_CYCLES			0x0020
#define DI_BPT_INSTRUCTIONS		0x0040
#define DI_BPT_EXTERN			0x0080


/* DiBpSupportT: not in use anymore */
#if 0
	typedef struct
	{
	    DiMemSpaceT	dmsMemSpace;
	    DiBpTypeT	dbtBpTypeSupported;
	    DiBoolT		fExecFromBp;
	    DiBoolT		fBpSkidding;
	    DiUInt32T	nBpAvailable;
	} DiBpSupportT, *pDiBpSupportT;
#endif

/* DiTimeT */
typedef struct
{
    DiValueT	v;
} DiTimeT, *pDiTimeT;


/* DiBpT */
typedef struct
{
    DiBpTypeT		dbtBpType;
    union
    {
        struct
        {
            DiAddrT		daBp;
            DiUInt32T	dnSize;
			DiBpMethodT	dbmBpMethod;
		} exe;
        struct 
        {
            DiAddrT		daBp;
            DiUInt32T	dnSize;
            DiValueT	dnValue;
        } rwv;
        struct
        {
            DiTimeT		dtTime;
        } tim;
		struct
		{
			DiValueT	dvCycle;
		} cyc;
		struct
		{
			DiValueT	dvInstr;
		} inst;
    } u;
} DiBpT, *pDiBpT;

/* DiBpResultT */
typedef struct
{
    DiUInt32T	dnBpId;
    DiBpT		dbBp;
} DiBpResultT,	*pDiBpResultT;


/* DiValueStatusT */
typedef DiUInt32T	DiValueStatusT, *pDiValueStatusT;

#define DI_VS_DEFINED			0x0000
#define DI_VS_FLOATING			0x0001
#define DI_VS_UNINITIALIZED		0x0002
#define DI_VS_UNKNOWN			0x0003
#define DI_VS_READONLY			0x0004
#define DI_VS_WRITEONLY			0x0005
#define DI_VS_NO_MEMORY			0x0006


/* DiCallbackT */
typedef DiUInt32T	DiCallbackT, *pDiCallbackT;

#define	DI_CB_NONE			0x0000
#define	DI_CB_BREAK			0x0001
#define	DI_CB_EXCEPTION		0x0002
#define	DI_CB_SYNCHRONIZE	0x0004
#define	DI_CB_EXTERNBPSET	0x0008
#define	DI_CB_MISCELLANEOUS	0x0010
#define	DI_CB_EXITREQUEST	0x0020
#define	DI_CB_LOG			0x0040
#define	DI_CB_APPLI_INPUT	0x0080
#define	DI_CB_APPLI_OUTPUT	0x0100
#define	DI_CB_EVAL_EXPR		0x0200
#define	DI_CB_ADDRTOSYMBOL	0x0400
#define	DI_CB_DEBUG			0x0800
#define	DI_CB_RESOURCES		0x1000
#define	DI_CB_FEEDBACK		0x2000

/* Begin GDI ENHANCEMENT of version 126 !!! */
#define DI_CB_DISASSEMBLE	0x4000
#define DI_CB_INFOLINE		0x8000
#define DI_CB_VALID_OPCODE	0x10000
/* End GDI ENHANCEMENT of version 126 !!! */

/* DiCommChannelT */
typedef DiUInt32T	DiCommChannelT, *pDiCommChannelT;

#define	DI_COMM_NONE		0x0000
#define	DI_COMM_RS232		0x0001
#define	DI_COMM_RS422		0x0002
#define	DI_COMM_RS485		0x0004
#define	DI_COMM_TCPIP		0x0008
#define	DI_COMM_IPXSPX		0x0010
#define	DI_COMM_IPC			0x0020
#define	DI_COMM_RPC			0x0040
#define	DI_COMM_SCSI		0x0080
#define	DI_COMM_CAN			0x0100
#define	DI_COMM_I2C			0x0200
#define	DI_COMM_PROFIBUS	0x0400
#define	DI_COMM_INTERBUS	0x0800
#define	DI_COMM_TAP			0x1000
#define	DI_COMM_DEVICE_FILE	0x2000
#define	DI_COMM_ISA_BUS		0x4000
#define	DI_COMM_PARALLEL	0x8000


typedef char *	DiStringT;


/* DiRs232HandshakeT */
typedef enum
{
    DI_RS232_NONE			=  0,
    DI_RS232_RTS_CTS		=  1,
    DI_RS232_XON_XOFF		=  2
} DiRs232HandshakeT;


/* DiCommSetupT */
typedef struct
{
    DiCommChannelT	dccType;
    DiBoolT		fCheckConnection;
    union
    {
        struct
        {
            DiStringT			szPortName;
            DiUInt32T			dnBaudRate;
            DiRs232HandshakeT	drhHandshake;
            DiUInt32T			dnTimeoutFactor;
            DiBoolT				fExclusiveAccess;
        } Rs232;
        struct
        {
            /* to be defined */
            DiUInt32T		to_be_defined;
        } Rs422;
        struct
        {
            /* to be defined */
            DiUInt32T		to_be_defined;
        } Rs485;
        struct
        {
            DiStringT		szHost;
            DiStringT		szPort;		 
        } TcpIp;
        struct
        {
            DiStringT		szHost;
            DiStringT		szPort;		 
        } IpxSpx;
        struct
        {
            DiStringT		szMsgBox;			
        } IPC;
        struct
        {
            DiStringT		szHost;
            DiStringT		szRpcPortNumber;
        } RPC;
        struct
        {
            DiUInt32T		dnScsiId;
            DiUInt32T		dnLogicalUnitNumber;
        } SCSI;
        struct
        {
            DiUInt32T		dnBaudRate;
			DiUInt32T		dnTargetDeviceIdent;
			DiBoolT			fForceLongAddressing;
			DiUInt32T		dnTimeoutFactor;
			DiUInt32T		dnHostInterfaceNumber;
			DiUInt32T		dnHostPortNumber;
        } CAN;
        struct
        {
            /* to be defined */
            DiUInt32T		to_be_defined;
        } I2C;
        struct
        {
            /* to be defined */
            DiUInt32T		to_be_defined;
        } Profibus;
        struct 
        {
            /* to be defined */
            DiUInt32T		to_be_defined;
        } Interbus;
        struct
        {
            /* to be defined */
            DiUInt32T		to_be_defined;
        } TAP;
        struct
        {
            DiStringT		szFileName;
        } DeviceFile;
        struct
        {
            DiUInt32T		dnPortBase;
            DiUInt32T		dnMemBase;
        } ISA;
        struct
        {
            DiStringT		szPortName;
        } Parallel;
    } u;
} DiCommSetupT, *pDiCommSetupT;


/* DiCoverageT */
typedef DiUInt32T	DiCoverageT, *pDiCoverageT;

#define DI_COV_UNKNOWN			0x0000
#define DI_COV_EXECUTE			0x0001
#define DI_COV_READ				0x0002
#define DI_COV_WRITE			0x0004
#define DI_COV_ACCESS			0x0008


/* DiDebugInfoT */
typedef enum
{
    DI_UI_SOURCE			=  0,
    DI_UI_ADDRESS			=  1,
    DI_UI_SYMBOL			=  2,
    DI_UI_STRING			=  3
} DiDebugInfoT, *pDiDebugInfoT;


/* DiDownloadCommandT */
typedef enum
{
    DI_DNLD_INIT			=  0,
    DI_DNLD_WRITE			=  1,
    DI_DNLD_TERMINATE		=  2,
    DI_DNLD_ABORT			=  3
} DiDownloadCommandT, *pDiDownloadCommandT;


/* DiDownloadFormatT */
typedef struct
{
    DiAbsFileT		dafAbsFileFormat;
    DiUInt32T		dnBufferSize;
    DiAddrT			daAddress;
} DiDownloadFormatT, *pDiDownloadFormatT;


/* DiEndianT */
typedef enum
{
    DI_UNKNOWN_ENDIANNESS	= 0,
    DI_BIG_ENDIAN			= 1,
    DI_LITTLE_ENDIAN		= 2
} DiEndianT;


/* DiExecEnvT */
typedef struct
{
    DiUInt32T		dnNrItems;
    DiStringT		*pszName;
} DiExecEnvT, *pDiExecEnvT;


/* DiExitCauseT */
typedef DiUInt32T	DiExitCauseT, *pDiExitCauseT;
 
#define DI_WAIT_RUNNING			0x0000
#define DI_WAIT_SINGLESTEP		0x0001
#define DI_WAIT_CONTINUEUNTIL	0x0002
#define DI_WAIT_CODEBP			0x0004
#define DI_WAIT_DATABP			0x0008
#define DI_WAIT_CYCLES			0x0010
#define DI_WAIT_TIME			0x0020
#define DI_WAIT_INSTRUCTIONS	0x0040
#define DI_WAIT_USER			0x0080
#define DI_WAIT_EXTERNAL		0x0100
#define DI_WAIT_MISCELLANEOUS	0x0200
#define DI_WAIT_UNKNOWN			0x0400


/* DiExitStatusT */
typedef struct
{ 
    DiExitCauseT	dscCause;
    DiUInt32T		dwBpId;
    DiStringT		szReason;
} DiExitStatusT, *pDiExitStatusT;


typedef enum
{
    DI_TYPE_VOID,		/* no value */
    DI_TYPE_ADDR,		/* offset and space */
    DI_TYPE_STRING,
    DI_TYPE_VALUE
} DiTypeT, *pDiTypeT;


/* DiExprResultT */
typedef struct
{
    DiTypeT			dtType;
    union
    {
		DiAddrT			daAddr;
		DiStringT		szString;
		DiValueT		dvValue;
    } u;
} DiExprResultT, *pDiExprResultT;


/* DiFeaturesT */
typedef struct
{
    DiStringT			szIdentification;
    DiStringT			szVersion; 
    DiUInt32T			dnVersion;
    DiStringT			*pszConfig;		/* allowable configurations for this EE */
    DiUInt32T			dnConfigArrayItems;
    DiCommChannelT		dccIOChannel;
    DiBoolT				fMemorySetMapAvailable;
    DiBoolT				fMemorySetCpuMapAvailable;
    DiStringT			*pszMemoryType;
    DiUInt32T			dnMemTypeArrayItems;
    DiBoolT				fEnableReadaheadCache;
    DiBoolT				fTimerInCycles;
    DiUInt32T			dnTimerResolutionMantissa;
    DiInt32T			dnTimerResolutionExponent;
    DiDownloadFormatT	ddfDownloadFormat;
    DiDownloadFormatT	ddfAuxDownloadFormat;
    DiBoolT				fAuxiliaryDownloadPathAvailable;
    DiCallbackT			dcCallback;
    DiBoolT				fRegisterClassSupport;
    DiBoolT				fSingleStepSupport;
    DiBoolT				fContinueUntilSupport;
    DiBoolT				fContinueBackgroundSupport;
    DiUInt32T			dnNrCodeBpAvailable;
    DiUInt32T			dnNrDataBpAvailable;
    DiBoolT				fExecFromCodeBp;
    DiBoolT				fExecFromDataBp;
    DiBoolT				fUnifiedBpLogic;
    DiBoolT				fExecCycleCounterAvailable;
    DiBoolT				fExecTimeAvailable;
    DiBoolT				fInstrTraceAvailable;
    DiBoolT				fRawTraceAvailable;
    DiBoolT				fCoverageAvailable;
    DiBoolT				fProfilingAvailable;
    DiBoolT				fStateSaveRestoreAvailable;
    DiUInt32T			dnStateStoreMaxIndex;
    DiBackGroundT		*pdbgBackground;
    DiUInt32T			dnBackgroundArrayItems;
    DiBoolT				fDirectDiAccessAvailable;
    DiBoolT				fApplicationIOAvailable;
    DiBoolT				fKernelAware;
    DiBoolT				fMeeAvailable;		/* Allow use of the DiMee...() calls */
    DiUInt32T			dnNrCpusAvailable;	/* Nr of cpus in *this* Exec Env */
    DiEndianT			deWordEndianness;
    DiUInt32T			dnNrHardWareCodeBpAvailable;
    DiBoolT				fCodeHardWareBpSkids;
    void				*pReserved;
} DiFeaturesT, *pDiFeaturesT;


/* DiFeedbackCommandT */
typedef enum
{
    DI_FBC_START		=  0,
    DI_FBC_TICK			=  1,
    DI_FBC_STOP			=  2
} DiFeedbackCommandT, *pDiFeedbackCommandT;


/* DiInstrTraceItemT */
typedef struct
{
    DiAddrT		daPc;
    DiTimeT		dtTime;
} DiInstrTraceItemT, *pDiInstrTraceItemT;


/* DiInstrTraceT */
typedef struct
{
    DiUInt32T		dnNrInstr;
    DiInstrTraceItemT	*pditiItems;
} DiInstrTraceT, *pDiInstrTraceT;


/* DiMemT */
typedef struct
{
    DiUCharT		val[cbDiMaxUCharTsPerMemT];
} DiMemT, *pDiMemT;


/* DiMemValueT */
typedef struct
{
    DiMemT			dmValue;
    DiValueStatusT	dvsStatus;
} DiMemValueT, *pDiMemValueT;


/* DiMemoryMapItemT */
typedef struct
{
    DiAddrRangeT	darRange;
    DiStringT		szMemType;
    DiAccessT		daAccess;
} DiMemoryMapItemT, *pDiMemoryMapItemT;


/* DiMemoryMapT */
typedef struct
{
    DiUInt32T			dnNrItems;
    DiMemoryMapItemT	*pdmmiItems;
} DiMemoryMapT, *pDiMemoryMapT;


/* DiMemorySpaceInfoT */
typedef struct
{
    DiStringT		szMemSpaceName;
    DiMemSpaceT		dnMemSpaceId;
    DiBoolT			fGdiSupport;
} DiMemorySpaceInfoT, *pDiMemorySpaceInfoT;


/* DiMemoryToCpuItemT */
typedef struct
{
    DiAddrRangeT	darCpu;
    DiAddrT			daPhysMem;
} DiMemoryToCpuItemT, *pDiMemoryToCpuItemT;


/* DiMemoryToCpuT */
typedef struct
{
    DiUInt32T			dnNrItems;
    DiMemoryToCpuItemT	*pdmtciItems;
} DiMemoryToCpuT,		*pDiMemoryToCpuT;


/* DiMenuItemT */
typedef struct
{
    DiStringT		szMenuTitle;
    DiStringT		szDiCommand;
} DiMenuItemT, *pDiMenuItemT;


/* DiNewFramesT */
typedef struct 
{
    DiUInt32T		dnNrNewFrames;
    DiBoolT			fTraceBufferOverflow;
} DiNewFramesT, *pDiNewFramesT;


/* DiProfT */
typedef struct 
{
    DiAddrRangeT	darRange;
    DiTimeT			dtTime;
} DiProfT, *pDiProfT;


/* DiProfileT */
typedef struct 
{
    DiUInt32T		dnEntries;
    pDiProfT		*pdpProf;
} DiProfileT, *pDiProfileT;


/* DiRegisterInfoT */
typedef struct
{
    DiStringT		szRegName;
    DiUInt32T		dnRegNumber;
    DiBoolT			fGdiSupport;
} DiRegisterInfoT, *pDiRegisterInfoT;


/* DiRegisterValueT */
typedef struct
{
    DiValueT		dvValue;
    DiValueStatusT	dvsStatus;
} DiRegisterValueT, *pDiRegisterValueT;


/* DiResourceCommandT */
typedef enum
{
    DI_RSRC_GET				=  0,
    DI_RSRC_SET				=  1,
    DI_RSRC_SET_PERSISTENT	=  2
} DiResourceCommandT, *pDiResourceCommandT;


/* DiReturnT */
typedef enum
{
    DI_OK					= 0,
    DI_ERR_STATE			= 1,
    DI_ERR_PARAM			= 2,
    DI_ERR_COMMUNICATION	= 3,
    DI_ERR_NOTSUPPORTED		= 4,
    DI_ERR_NONFATAL			= 5,
    DI_ERR_CANCEL			= 6,
    DI_ERR_FATAL			= 7,
    DI_ERR_WARNING			= 21
} DiReturnT, *pDiReturnT;


/* DiTraceTypeT */
typedef enum
{
    DI_TRACE_INSTR			=  0,
    DI_TRACE_RAW			=  1,
    DI_TRACE_RAW1			=  2,
    DI_TRACE_RAW2			=  3
} DiTraceTypeT, *pDiTraceTypeT;


/* DiUserInfoT */
typedef struct
{
    DiDebugInfoT	ddiType;
    union
    {
        struct
        {
            DiStringT	szSrcFileName;
            DiUInt32T	dnSrcLineNumber;
            DiAddrT		daSrcAddress;
        } source;
        struct
        {	
            DiAddrT		daAddress;
        } instr;
        struct
        {
            DiStringT	szSymbolName;
            DiAddrT		daAddress;
        } symbol;
        struct
        {
            DiStringT	szString;
        } string; 
    } u;
} DiUserInfoT, *pDiUserInfoT;


/*********** function prototypes **********/

#ifdef __cplusplus
extern "C" {
#endif

typedef void    (*UdProcessEventsF)(void);
typedef void	(*CallbackF)(DiCallbackT, ...);
typedef void	(*PrintRawTraceF)( DiAddrT daPC, char *szString );

RETURN_TYPE	EVAL(DiGdiOpen) PARAMETERS(( DiUInt32T dnGdiVersionH, DiUInt32T dnGdiVersionL, DiUInt32T dnArgc, DiStringT *szArgv, UdProcessEventsF UdProcessEvents ));
RETURN_TYPE	EVAL(DiGdiClose) PARAMETERS(( DiBoolT fClose ));
RETURN_TYPE	EVAL(DiGdiGetFeatures) PARAMETERS(( pDiFeaturesT pdfFeatures ));
RETURN_TYPE	EVAL(DiGdiSetConfig) PARAMETERS(( DiStringT szConfig));
RETURN_TYPE	EVAL(DiGdiInitIO) PARAMETERS(( pDiCommSetupT pdcCommSetup ));
RETURN_TYPE	EVAL(DiGdiInitRegisterMap) PARAMETERS(( DiUInt32T dnEntries, DiRegisterInfoT *pdriRegister, DiUInt32T dnProgramCounter));
RETURN_TYPE	EVAL(DiGdiInitMemorySpaceMap) PARAMETERS(( DiUInt32T dnEntries, DiMemorySpaceInfoT *pdmiMemSpace ));
RETURN_TYPE	EVAL(DiGdiAddCallback) PARAMETERS(( DiCallbackT dcCallbackType, CallbackF Callback));
RETURN_TYPE	EVAL(DiGdiCancel) PARAMETERS(( void ));
RETURN_TYPE	EVAL(DiGdiSynchronize) PARAMETERS(( DiBoolT *pfUpdate ));
RETURN_TYPE	EVAL(DiDirectAddMenuItem) PARAMETERS(( DiUInt32T *pdnNrEntries, pDiMenuItemT *pdmiMenuItem ));
RETURN_TYPE	EVAL(DiDirectCommand) PARAMETERS(( DiStringT szCommand, DiUserInfoT *pduiUserInfo));
RETURN_TYPE	EVAL(DiDirectReadNoWait) PARAMETERS(( DiUInt32T dnNrToRead, char *pcBuffer, DiUInt32T *dnNrRead ));
RETURN_TYPE	EVAL(DiErrorGetMessage) PARAMETERS(( DiStringT *pszErrorMsg ));
RETURN_TYPE	EVAL(DiMemorySetMap) PARAMETERS(( DiMemoryMapT dmmMap ));
RETURN_TYPE	EVAL(DiMemoryGetMap) PARAMETERS(( DiMemoryMapT *pdmmMap ));
RETURN_TYPE	EVAL(DiMemorySetCpuMap) PARAMETERS(( DiMemoryToCpuT dmtcMap ));
RETURN_TYPE	EVAL(DiMemoryGetCpuMap) PARAMETERS(( DiMemoryToCpuT *pdmtcMap ));
RETURN_TYPE	EVAL(DiMemoryDownload) PARAMETERS(( DiBoolT fUseAuxiliaryPath, DiDownloadCommandT ddcDownloadCommand, DiDownloadFormatT ddfDownloadFormat, char *pchBuffer ));
RETURN_TYPE	EVAL(DiMemoryWrite) PARAMETERS(( DiAddrT daTarget, DiMemValueT *pdmvBuffer, DiUInt32T dnBufferItems ));
RETURN_TYPE	EVAL(DiMemoryRead) PARAMETERS(( DiAddrT daTarget, DiMemValueT *pdmvBuffer, DiUInt32T dnBufferItems ));
RETURN_TYPE	EVAL(DiRegisterWrite) PARAMETERS(( DiUInt32T dnRegNumber, DiRegisterValueT drvValue ));
RETURN_TYPE	EVAL(DiRegisterRead) PARAMETERS(( DiUInt32T dnRegNumber, pDiRegisterValueT drvValue ));
RETURN_TYPE	EVAL(DiRegisterClassCreate) PARAMETERS(( DiUInt32T *pdnRegClassId, DiUInt32T *pdnRegisterId, DiUInt32T dnRegisterIdEntries ));
RETURN_TYPE	EVAL(DiRegisterClassDelete) PARAMETERS(( DiUInt32T dnRegClassId ));
RETURN_TYPE	EVAL(DiRegisterClassWrite) PARAMETERS(( DiUInt32T dnRegClassId, DiRegisterValueT *pdrvClassValue ));
RETURN_TYPE	EVAL(DiRegisterClassRead) PARAMETERS(( DiUInt32T dnRegClassId, DiRegisterValueT *pdrvClassValue ));
RETURN_TYPE	EVAL(DiBreakpointSet) PARAMETERS(( DiBpResultT *pdnBreakpointId, DiBpT dbBreakpoint ));
RETURN_TYPE	EVAL(DiBreakpointClear) PARAMETERS(( DiUInt32T dnBreakpointId ));
RETURN_TYPE	EVAL(DiBreakpointClearAll) PARAMETERS(( void ));
RETURN_TYPE	EVAL(DiExecResetChild) PARAMETERS(( void ));
RETURN_TYPE	EVAL(DiExecSingleStep) PARAMETERS(( DiUInt32T dnNrInstructions ));
RETURN_TYPE	EVAL(DiExecContinueUntil) PARAMETERS(( DiAddrT adrUntil ));
RETURN_TYPE	EVAL(DiExecContinue) PARAMETERS(( void ));
RETURN_TYPE	EVAL(DiExecContinueBackground) PARAMETERS(( void ));
RETURN_TYPE	EVAL(DiExecGetStatus) PARAMETERS(( pDiExitStatusT pdesExitStatus ));
RETURN_TYPE	EVAL(DiExecStop) PARAMETERS(( void ));
RETURN_TYPE	EVAL(DiCommGetAcceptableSettings) PARAMETERS(( DiCommChannelT dccType, DiStringT szAttr, DiStringT **pszEntries, void *pReserved ));
RETURN_TYPE	EVAL(DiTraceSwitchOn) PARAMETERS(( DiBoolT fOn ));
RETURN_TYPE	EVAL(DiTraceGetInstructions) PARAMETERS(( DiUInt32T dnNrInstr, pDiInstrTraceT pditInstrTrace ));
RETURN_TYPE	EVAL(DiTracePrintRawInfo) PARAMETERS(( DiUInt32T dnNrFrames, DiTraceTypeT dttTraceType, PrintRawTraceF PrintRawTrace ));
RETURN_TYPE	EVAL(DiTraceGetNrOfNewFrames) PARAMETERS(( DiTraceTypeT dttTraceType, DiUInt32T dnNrMaxFrames, pDiNewFramesT pdnfNewFrames ));
RETURN_TYPE	EVAL(DiCoverageSwitchOn) PARAMETERS(( DiBoolT fOn ));
RETURN_TYPE	EVAL(DiCoverageGetInfo) PARAMETERS(( DiAddrT daStart, DiUInt32T dnSize, DiCoverageT *pdcCoverage ));
RETURN_TYPE	EVAL(DiProfilingSwitchOn) PARAMETERS(( DiBoolT fOn ));
RETURN_TYPE	EVAL(DiProfileGetInfo) PARAMETERS(( DiProfileT *dpProfile ));
RETURN_TYPE	EVAL(DiStateOpen) PARAMETERS(( DiUInt32T *pdnStateHandle, DiStringT szStateName ));
RETURN_TYPE	EVAL(DiStateSave) PARAMETERS(( DiUInt32T dnStateHandle, DiUInt32T dnIndex ));
RETURN_TYPE	EVAL(DiStateRestore) PARAMETERS(( DiUInt32T dnStateHandle, DiUInt32T dnIndex ));
RETURN_TYPE	EVAL(DiStateClose) PARAMETERS(( DiBoolT fDelete ));
RETURN_TYPE	EVAL(DiMeeEnumExecEnv) PARAMETERS(( DiExecEnvT *pdExecEnv ));
RETURN_TYPE	EVAL(DiMeeConnect) PARAMETERS(( DiUInt32T dnExecId ));
RETURN_TYPE	EVAL(DiMeeGetFeatures) PARAMETERS(( DiUInt32T dnExecId, pDiFeaturesT pdfFeatures ));
RETURN_TYPE	EVAL(DiMeeInitIO) PARAMETERS(( DiUInt32T dnExecId, pDiCommSetupT pdcCommSetup ));
RETURN_TYPE	EVAL(DiMeeSelect) PARAMETERS(( DiUInt32T dnExecId ));
RETURN_TYPE	EVAL(DiMeeDisconnect) PARAMETERS(( DiUInt32T dnExecId, DiBoolT fClose ));
RETURN_TYPE	EVAL(DiCpuSelect) PARAMETERS(( DiUInt32T dnCpuId ));
RETURN_TYPE	EVAL(DiCpuCurrent) PARAMETERS(( DiUInt32T *dnCpuId ));
RETURN_TYPE	EVAL(DiGdiVersion) PARAMETERS(( DiUInt32T *dnGdiVersion ));	/* shall return DI_GDI_VERSION */
RETURN_TYPE	EVAL(DiProcess) PARAMETERS(( void *information ));

#ifdef __cplusplus
}
#endif

#endif /* _GDI_H */
