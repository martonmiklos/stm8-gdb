/*	Copyright (C) 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "breakpoint.h"
#include "inferior.h"
#include "target.h"
#include "symfile.h"
#include "st7-utils.h"
#include "../opcodes/ins7.h"


/* 	JRIH, JRIL: Relative jump in function of external interrupt line. */

enum opcode_enum {
	BTJF,
	BTJT,

	JRC,
	JREQ,
	JRH,
	JRIH,
	JRIL,
	JRM,
	JRMI,
	JRNC,
	JRNE,
	JRNH,
	JRNM,
	JRNV,
	JRPL,
	JRSGE,
	JRSGT,
	JRSLE,
	JRSLT,
	JRUGT,
	JRULE,
	JRV,

	CALL,
	CALLF,
	CALLR,

	IRET,
	RET,
	RETF,

	JP,
	JPF,
	JRT
};

enum instruction_type_enum {
	bit_test_and_jump,
	jump_on_condition,
	subroutine_call,
	subroutine_return,
	jump
};

enum jump_type_enum {
	no_jump,
	unconditional_jump,
	conditional_jump
};

struct instruction_struct {
	char *opcode_string;
	enum opcode_enum opcode;
	enum instruction_type_enum instruction_type;
	enum jump_type_enum jump_type;
};

struct instruction_struct jump_instruction_array[] = {

	/* Bit test and jump. */
	{"BTJF", BTJF, bit_test_and_jump, conditional_jump},
	{"BTJT", BTJT, bit_test_and_jump, conditional_jump},

	/* Relative jump in function of Condition Code Register. */
	/* JRUGE is equivalent to JRNC. */
	/* JRULT is equivalent to JRC. */
	{"JRC", JRC, jump_on_condition, conditional_jump},
	{"JREQ", JREQ, jump_on_condition, conditional_jump},
	{"JRH", JRH, jump_on_condition, conditional_jump},
	{"JRIH", JRIH, jump_on_condition, conditional_jump},
	{"JRIL", JRIL, jump_on_condition, conditional_jump},
	{"JRM", JRM, jump_on_condition, conditional_jump},
	{"JRMI", JRMI, jump_on_condition, conditional_jump},
	{"JRNC", JRNC, jump_on_condition, conditional_jump},
	{"JRNE", JRNE, jump_on_condition, conditional_jump},
	{"JRNH", JRNH, jump_on_condition, conditional_jump},
	{"JRNM", JRNM, jump_on_condition, conditional_jump},
	{"JRNV", JRNV, jump_on_condition, conditional_jump},
	{"JRPL", JRPL, jump_on_condition, conditional_jump},
	{"JRSGE", JRSGE, jump_on_condition, conditional_jump},
	{"JRSGT", JRSGT, jump_on_condition, conditional_jump},
	{"JRSLE", JRSLE, jump_on_condition, conditional_jump},
	{"JRSLT", JRSLT, jump_on_condition, conditional_jump},
	{"JRUGT", JRUGT, jump_on_condition, conditional_jump},
	{"JRULE", JRULE, jump_on_condition, conditional_jump},
	{"JRV", JRV, jump_on_condition, conditional_jump},

	/* Call instructions. */
	{"CALL", CALL, subroutine_call, unconditional_jump},
	{"CALLF", CALLF, subroutine_call, unconditional_jump},
	{"CALLR", CALLR, subroutine_call, unconditional_jump},

	/* Return instructions. */
	{"IRET", IRET, subroutine_return, unconditional_jump},
	{"RET", RET, subroutine_return, unconditional_jump},
	{"RETF", RETF, subroutine_return, unconditional_jump},

	/* Unconditional jump instructions. */
	/* JRA is equivalent to JRT. */
	/* Special operation code 0x82 for interrupt (INT) is equivalent to JPF */
	{"INT", JPF, jump, unconditional_jump},
	{"JP", JP, jump, unconditional_jump},
	{"JPF", JPF, jump, unconditional_jump},
	{"JRT", JRT, jump, unconditional_jump}
};

#define JUMP_INSTRUCTION_NUMBER \
	(sizeof(jump_instruction_array) / sizeof(struct instruction_struct))

static enum jump_type_enum
get_instruction_type
(char *disass_instr_buffer, CORE_ADDR pc, CORE_ADDR next_pc, CORE_ADDR *jump_address);
static void add_step_breakpoint(CORE_ADDR address, int software_breakpoint, char *shadow_contents);
static void remove_step_breakpoints(void);
static int step_breakpoint_inserted_here_p(CORE_ADDR pc);

typedef char instruction_copy[BREAKPOINT_MAX];

static instruction_copy instruction_copy_array[3];

/* ICD */
static int hardware_breakpoints_disabled;

/* SoftwareSingleStep is used to know if the last program execution 
	was due to a software single step. */
int SoftwareSingleStep = 0;

/* Non-zero if we just simulated a single-step. This is needed because we
   cannot remove the breakpoints in the inferior process until after the
   `wait' in `wait_for_inferior'. */

int one_stepped;

#define ONLY_INSERTED_BREAKPOINTS 1

/* single_step() is called just before we want to resume the inferior, if we
   want to single-step it but there is no hardware or kernel single-step
   support. We find all the possible targets of the coming instruction and
   breakpoint them.

   single_step is also called just after the inferior stops. If we had
   set up a simulated single-step, we undo our damage.  */

/* signal is the signal to give the inferior (zero for none)
   but we don't need it. */
void single_step (enum target_signal signal) 
{
  if (stepping_mode == hardware_stepping_mode) {
    return;
  }

  if (!one_stepped)
    {
      CORE_ADDR pc; 
      int instruction_length;
      char disass_instr_buffer[STRING_LENGTH];
      enum jump_type_enum jump_type;
      CORE_ADDR jump_address;
      CORE_ADDR next_pc;
	  int memory_check = check_disassembly;
	  char limitation_error_message[] =
	  "SOFT_STEP_LIMIT Software stepping can't handle instructions looping on themselves";

	  one_stepped = 1;

     /* ICD */
	  hardware_breakpoints_disabled = 0;

      pc = read_pc ();

      instruction_length = disassemble_instruction(pc, disass_instr_buffer, memory_check);
	  if (instruction_length == -1) {
		  error("Can't disassemble at 0x%x while single stepping.", pc);
	  }
	  next_pc = pc + instruction_length ;

      /* 
       * Gives jump information about current instruction "disass_instr_buffer":
       * "jump_type" is the type of jump:
			no_jump, conditional_jump, unconditional_jump
       * "jump_address" gives the address of the jump in case of a jump instruction.
       */
      jump_type = get_instruction_type(disass_instr_buffer, pc, next_pc, &jump_address);

	  /* Don't forget to set a breakpoint on the reset handler. */
	  if (!breakpoint_inserted_here_p(MCU_reset_address) && pc != MCU_reset_address)
	  {
		  /* For ICD targets, only set a breakpoint on the reset handler if it is in RAM
			as the number of hardware breakpoints in the debug module is limited to 2.
			For ICD targets, software stepping won't handle application resets
			if the reset handler is in ROM!
			This restriction should be documented. */
		  if (debug_instrument_has_a_limited_number_of_hardware_breakpoints()) {
			if (is_software_breakpoint_possible(MCU_reset_address)) {
				add_step_breakpoint(MCU_reset_address, 1, instruction_copy_array[0]);
			}
		  } else {
			add_step_breakpoint(MCU_reset_address, 0, instruction_copy_array[0]);
		  }
	  }
      /* Set break on next PC if no jump. */
      if (jump_type == no_jump)
		{
		  int software_breakpoint = 0;

		  if (breakpoint_inserted_here_p(next_pc))
			return;

		  /* ICD */
		  if (is_software_breakpoint_possible(next_pc)) {
			software_breakpoint = 1;
		  } else {
			disable_hardware_breakpoints_if_necessary(1,
													  &hardware_breakpoints_disabled,
													  ONLY_INSERTED_BREAKPOINTS);
		  }

		  add_step_breakpoint(next_pc, software_breakpoint, instruction_copy_array[1]);
		}
      /* Set breakpoint on jump_address for unconditional jump. */
      else if (jump_type == unconditional_jump)
		{
		  int software_breakpoint = 0;

		  if (!debug_instrument_supports_restart_from_breakpoint()) {
			if (jump_address == pc) {
			  /* can't handle instructions looping on themselves */
			  caution_message("%s", limitation_error_message);
			  gdb_flush(gdb_stderr);
			  return;
			}
		  }

		 /* However we have a problematic case:

		 	jp jump1
				...
			instruction_before_jump1: nop
			jump1: ...

			where jump1 = instruction_before_jump1 + 1
			and the beginning of the code is in RAM while jump1 is at the start
			of a ROM zone.
			
			If there is a software breakpoint at jump1 - 1 and a hardware breakpoint
			at jump1, we have to be careful not to be confused while rectifying
			the PC in bpstat_stop_status (after a break due to a software breakpoint,
			the PC must be decremented).
			We can easily resolve the case if we take into account the stop reason.
		*/	
		/*
			A stepping breakpoint has already been set on the reset routine.
			The STM7+ has a boot ROM from which you can jump to the reset routine.
			Don't add a second stepping breakpoint on the reset routine
			otherwise the first byte of this routine is permanently modified!
		*/
		  if (breakpoint_inserted_here_p(jump_address) || step_breakpoint_inserted_here_p(jump_address))
			return;

		  /* ICD */
		  if (is_software_breakpoint_possible(jump_address)) {
			software_breakpoint = 1;
		  } else {
			disable_hardware_breakpoints_if_necessary(1,
													  &hardware_breakpoints_disabled,
													  ONLY_INSERTED_BREAKPOINTS);
		  }

		  add_step_breakpoint(jump_address, software_breakpoint, instruction_copy_array[1]);
		}
      /* Set breakpoint on next PC and on jump_address for conditional jump. */
      else if (jump_type == conditional_jump)
		{
		  /* ICD */
		  int hardware_breakpoints_needed = 0;
		  int break_at_next_pc;
		  int break_at_jump_address;
		  int ignore_jump_address = 0;
		  int software_breakpoint1;
		  int software_breakpoint2;

		  if (!debug_instrument_supports_restart_from_breakpoint()) {
			if (jump_address == pc) {
			  /* can't handle instructions looping on themselves */
			  caution_message("%s", limitation_error_message);
			  gdb_flush(gdb_stderr);
			  ignore_jump_address = 1;
			}
		  }

		  /*
			Special case for ICD:

						jreq jump
			no_jump:	nop
			jump:		...

			where address(jump) = address(no_jump) + 1.

			If the debugger stops at jump, it can be:
			a) a hardware breakpoint set by GDB "break" command (jump is at the start
			of a ROM zone) at jump,
			b) or an ICD advanced breakpoint at jump,
			c) or a software breakpoint at no_jump (no_jump is in a RAM zone).
			The software breakpoint at no_jump can be either an existing breakpoint
			or a breakpoint added for software stepping.
			We should not confuse cases a and b with case c.

			Solution 1)
			If we use the reason returned by the ICD DLL, we can easily find out
			whether we stopped due to a software or to a hardware breakpoint.

			However we can't handle the case where ICD advanced breakpoints
			have been used to stop on stack write accesses as a software breakpoint
			causes the stack to be written to and is thus seen as an advanced breakpoint.
			
			Solution 2)
			Another way to solve the problem is to evaluate the condition of the jump
			to know the real PC after the execution of the conditional jump instruction
			but it might involve an intrusive read access in the case
			of a bit test and jump instruction.
			This solution implies that the debugger disassembles the current instruction
			before it is executed. This would be extra work if the debugger is not stepping
			by software means.

			Solution 3)
			Another way is to disable ICD advanced breakpoints to be sure
			that there is no interference between hardware breakpoints
			and software breakpoints.
			This solution, as well as solution 2, doesn't work when the debugger
			is executing the program without stepping.
			We can't disable ICD advanced breakpoints all the time.
			When executing the program normally, we still have the possible confusion
			between a software breakpoint at PC - 1 and a hardware breakpoint at PC
			whatever the instruction sequence.

			We choose solution 1 so we have nothing to do here.
			*/
		  break_at_next_pc = breakpoint_inserted_here_p(next_pc);
		  if (!break_at_next_pc) {
			software_breakpoint1 = is_software_breakpoint_possible(next_pc);
			if (!software_breakpoint1)
				hardware_breakpoints_needed++;
		  }
		  if (!ignore_jump_address && jump_address != next_pc) {
			break_at_jump_address = breakpoint_inserted_here_p(jump_address);
			if (!break_at_jump_address) {
				software_breakpoint2 = is_software_breakpoint_possible(jump_address);
				if (!software_breakpoint2)
					hardware_breakpoints_needed++;
			}
		  }
		  if (hardware_breakpoints_needed)
			disable_hardware_breakpoints_if_necessary(hardware_breakpoints_needed,
													  &hardware_breakpoints_disabled,
													  ONLY_INSERTED_BREAKPOINTS);

		  if (!break_at_next_pc)
			add_step_breakpoint(next_pc, software_breakpoint1, instruction_copy_array[1]);
		  if (!ignore_jump_address
			  && jump_address != next_pc
			  && !break_at_jump_address)
			add_step_breakpoint(jump_address, software_breakpoint2, instruction_copy_array[2]);
		}
    }
  else
    {
      /* Remove step breakpoints */
	  remove_step_breakpoints();

      /* ICD */
	  hardware_breakpoints_disabled = 0;

      one_stepped = 0;
    }
}

static CORE_ADDR get_bit_test_and_jump_address(char *operands);
static CORE_ADDR get_relative_address(char *operands, CORE_ADDR next_pc);
static CORE_ADDR get_call_or_jump_address(char *operands, CORE_ADDR pc);
static CORE_ADDR get_farcall_address(char *operand);
static CORE_ADDR get_return_address(enum opcode_enum opcode, CORE_ADDR pc);

/* get_instruction_type returns the type of instruction:

	- conditional jump instruction
	- unconditional jump instruction
	- no jump instruction

	as well as the jump address for jump instructions.
	*/
enum jump_type_enum
get_instruction_type
(char *disass_instr_buffer, CORE_ADDR pc, CORE_ADDR next_pc, CORE_ADDR *jump_address)
{
	char *opcode_string;
	char *operands;
	int index = 0;
	int index_max = JUMP_INSTRUCTION_NUMBER;

	opcode_string = strtok(disass_instr_buffer, " ");
	operands = strtok(NULL, " ");
	while (index < index_max) {
		if (strcmp(opcode_string, jump_instruction_array[index].opcode_string) == 0) {
			break;
		}
		index++;
	}
	if (index == index_max) {
		return no_jump;
	}
	switch(jump_instruction_array[index].instruction_type) {
		case bit_test_and_jump:
			*jump_address = get_bit_test_and_jump_address(operands);
			break;
		case jump_on_condition:
			*jump_address = get_relative_address(operands, next_pc);
			break;
		case subroutine_call:
			switch (jump_instruction_array[index].opcode) {
				case CALL:
					*jump_address = get_call_or_jump_address(operands, pc);
					break;
				case CALLF:
					*jump_address = get_farcall_address(operands);
					break;
				case CALLR:
					*jump_address = get_relative_address(operands, next_pc);
					break;
			}
			break;
		case subroutine_return:
			*jump_address = get_return_address(jump_instruction_array[index].opcode, pc);
			break;
		case jump:
			switch (jump_instruction_array[index].opcode) {
				case JP:
					*jump_address = get_call_or_jump_address(operands, pc);
					break;
				case JPF:
					*jump_address = get_farcall_address(operands);
					break;
				case JRT:
					*jump_address = get_relative_address(operands, next_pc);
					break;
			}
			break;
	}
	if (*jump_address == (CORE_ADDR) -1) {
		error("Can't execute software single step due to bad instruction: %s %s!", opcode_string, operands);
	}
	return jump_instruction_array[index].jump_type;
}

#define BIT_TEST_AND_JUMP_DIRECT_ADDRESSING_MODE "0x%x,#%d,0x%x"
#define BIT_TEST_AND_JUMP_SHORT_INDIRECT_RELATIVE_ADDRESSING_MODE \
	"[0x%x.B],#%d,0x%x"

/* Get the destination address of bit test and jump instructions:
	BTJF
	BTJT
	There are two possible addressing modes for the source address
	but only one addressing mode for the destination address.
	Note that the 8-bit relative destination address
	has already been converted into a direct address by the GDB7 disassembler.
	*/
static CORE_ADDR get_bit_test_and_jump_address(char *operands)
{
	unsigned int direct_tested_byte_address;
	unsigned int indirect_tested_byte_address;
	int position;
	unsigned int jump_address = -1;

	if (sscanf(operands,
				BIT_TEST_AND_JUMP_DIRECT_ADDRESSING_MODE,
				&direct_tested_byte_address, &position, &jump_address)
				== 3) {
		/* Direct source address. */
	} else if (sscanf(operands,
						BIT_TEST_AND_JUMP_SHORT_INDIRECT_RELATIVE_ADDRESSING_MODE,
						&indirect_tested_byte_address, &position, &jump_address)
						== 3) {
		/* Short indirect source address. */
	}
	return (CORE_ADDR)jump_address;
}

#define DIRECT_ADDRESSING_MODE "0x%x"
#define X_INDIRECT_ADDRESSING_MODE "(X)"
#define Y_INDIRECT_ADDRESSING_MODE "(Y)"

#define DIRECT_X_INDEXED_ADDRESSING_MODE "(0x%x,X)"
#define DIRECT_X_INDEXED_ADDRESSING_MODE_SUFFIX ",X)"

#define DIRECT_Y_INDEXED_ADDRESSING_MODE "(0x%x,Y)"
#define DIRECT_Y_INDEXED_ADDRESSING_MODE_SUFFIX ",Y)"

#define SHORT_INDIRECT_ADDRESSING_MODE "[0x%x.B]"
#define SHORT_INDIRECT_ADDRESSING_MODE_SUFFIX ".B]"

#define SHORT_INDIRECT_X_INDEXED_ADDRESSING_MODE "([0x%x.B],X)"
#define SHORT_INDIRECT_X_INDEXED_ADDRESSING_MODE_SUFFIX ".B],X)"

#define SHORT_INDIRECT_Y_INDEXED_ADDRESSING_MODE "([0x%x.B],Y)"
#define SHORT_INDIRECT_Y_INDEXED_ADDRESSING_MODE_SUFFIX ".B],Y)"

#define LONG_INDIRECT_ADDRESSING_MODE "[0x%x.W]"
#define LONG_INDIRECT_ADDRESSING_MODE_SUFFIX ".W]"

#define LONG_INDIRECT_X_INDEXED_ADDRESSING_MODE "([0x%x.W],X)"
#define LONG_INDIRECT_X_INDEXED_ADDRESSING_MODE_SUFFIX ".W],X)"

#define LONG_INDIRECT_Y_INDEXED_ADDRESSING_MODE "([0x%x.W],Y)"
#define LONG_INDIRECT_Y_INDEXED_ADDRESSING_MODE_SUFFIX ".W],Y)"

extern ULONGEST read_memory_nobpt_unsigned_integer (CORE_ADDR memaddr, int len);

/* Get the address from the operand of a CALL or a JUMP instruction
	taking into account all the possible addressing modes.
	*/
static CORE_ADDR get_call_or_jump_address(char *operands, CORE_ADDR pc)
{
	unsigned int address = -1;
	unsigned int indirect_address;

	/* Direct addressing mode. */
	if (sscanf(operands, DIRECT_ADDRESSING_MODE, &address) == 1) {
	}
	/* X indirect addressing mode. */
	else if (strcmp(operands, X_INDIRECT_ADDRESSING_MODE) == 0) {
		address = read_register(X_REGNUM);
	}
	/* Y indirect addressing mode. */
	else if (strcmp(operands, Y_INDIRECT_ADDRESSING_MODE) == 0) {
		address = read_register(Y_REGNUM);
	}
	/* Direct X indexed addressing mode. */
	else if (strstr(operands, DIRECT_X_INDEXED_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operands, DIRECT_X_INDEXED_ADDRESSING_MODE, &address) == 1) {
		address += read_register(X_REGNUM);
	}
	/* Direct Y indexed addressing mode. */
	else if (strstr(operands, DIRECT_Y_INDEXED_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operands, DIRECT_Y_INDEXED_ADDRESSING_MODE, &address) == 1) {
		address += read_register(Y_REGNUM);
	}

	/* Test for short indirect mode after short indirect X and Y indexed mode. */

	/* Short indirect X indexed addressing mode. */
	else if (strstr(operands, SHORT_INDIRECT_X_INDEXED_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operands, SHORT_INDIRECT_X_INDEXED_ADDRESSING_MODE, &indirect_address) == 1) {
		address = read_memory_nobpt_unsigned_integer(indirect_address, 1);
		address += read_register(X_REGNUM);
	}
	/* Short indirect Y indexed addressing mode. */
	else if (strstr(operands, SHORT_INDIRECT_Y_INDEXED_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operands, SHORT_INDIRECT_Y_INDEXED_ADDRESSING_MODE, &indirect_address) == 1) {
		address = read_memory_nobpt_unsigned_integer(indirect_address, 1);
		address += read_register(Y_REGNUM);
	}
	/* Short indirect addressing mode. */
	else if (strstr(operands, SHORT_INDIRECT_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operands, SHORT_INDIRECT_ADDRESSING_MODE, &indirect_address) == 1) {
		address = read_memory_nobpt_unsigned_integer(indirect_address, 1);
	}

	/* Test for long indirect mode after long indirect X and Y indexed mode. */

	/* Long indirect X indexed addressing mode. */
	else if (strstr(operands, LONG_INDIRECT_X_INDEXED_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operands, LONG_INDIRECT_X_INDEXED_ADDRESSING_MODE, &indirect_address) == 1) {
		address = read_memory_nobpt_unsigned_integer(indirect_address, 2);
		address += read_register(X_REGNUM);
	}
	/* Long indirect Y indexed addressing mode. */
	else if (strstr(operands, LONG_INDIRECT_Y_INDEXED_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operands, LONG_INDIRECT_Y_INDEXED_ADDRESSING_MODE, &indirect_address) == 1) {
		address = read_memory_nobpt_unsigned_integer(indirect_address, 2);
		address += read_register(Y_REGNUM);
	}
	/* Long indirect addressing mode. */
	else if (strstr(operands, LONG_INDIRECT_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operands, LONG_INDIRECT_ADDRESSING_MODE, &indirect_address) == 1) {
		address = read_memory_nobpt_unsigned_integer(indirect_address, 2);
	}

	if (address != (unsigned int)-1) {
		/* If there is no error, take into account the segment in the case of STM7. */
		address += (pc & 0xff0000);
	}
	return (CORE_ADDR)address;
}

#define SHORT_INDIRECT_RELATIVE_ADDRESSING_MODE "[0x%x.B]"

/* Get the relative address from the instruction operand:
	the address is relative to the program counter after the current instruction.
*/
static CORE_ADDR get_relative_address(char *operands, CORE_ADDR next_pc)
{
	unsigned int absolute_address = -1;
	unsigned int indirect_relative_address;
	/* The relative address is a signed char! */
	char relative_address;

	if (sscanf(operands, DIRECT_ADDRESSING_MODE, &absolute_address) == 1) {
		/* Direct relative address. */
		/* This relative address has already been converted into an absolute address
			by the disassembler. */
	} else if (sscanf(operands, SHORT_INDIRECT_RELATIVE_ADDRESSING_MODE, &indirect_relative_address) == 1) {
		/* Short indirect relative address. */
		relative_address = read_memory_nobpt_unsigned_integer(indirect_relative_address, 1);
		absolute_address = next_pc + (int)relative_address;
	}
	return (CORE_ADDR)absolute_address;
}

#define EXTENDED_INDIRECT_ADDRESSING_MODE "[0x%x.E]"
#define EXTENDED_INDIRECT_ADDRESSING_MODE_SUFFIX ".E]"

/* Get the extended address (3 bytes) from the instruction operand.
*/
static CORE_ADDR get_farcall_address(char *operand)
{
	unsigned int address = -1;
	unsigned int indirect_address;

	/* Direct addressing mode. */
	if (sscanf(operand, DIRECT_ADDRESSING_MODE, &address) == 1) {
	}
	/* Extended indirect addressing mode. */
	else if (strstr(operand, EXTENDED_INDIRECT_ADDRESSING_MODE_SUFFIX)
				&& sscanf(operand, EXTENDED_INDIRECT_ADDRESSING_MODE, &indirect_address) == 1) {
		address = read_memory_nobpt_unsigned_integer(indirect_address, 3);
	}
	return (CORE_ADDR)address;
}

/* Get the return address after one of the 3 types of return instruction:
	RET		--> return from a near function
	IRET	--> return from an interrupt handler
	RETF	--> return from a far function
	The return address (2 or 3 bytes) is read in the stack.
*/
static CORE_ADDR get_return_address(enum opcode_enum opcode, CORE_ADDR pc)
{
	int saved_PC_size = 2;
	int additional_stack_offset = 0;
	CORE_ADDR step_sp;
	CORE_ADDR saved_PC_address;
	CORE_ADDR return_address;
	
	switch (opcode) {
		case IRET:
			if (mcu_core == STM7P_CORE) {
				/* FIXME: if there is a separate register file, no register is stacked! */
				saved_PC_size = 3;
				/* The Y, X, A and CC registers are pushed onto the stack */
				additional_stack_offset = 6;
			} else {
				/* The X, A and CC registers are pushed onto the stack */
				additional_stack_offset = 3;
				if (is_a_STM7_far_interrupt()) {
					saved_PC_size = 3;
				}
			}
			break;
		case RETF:
			saved_PC_size = 3;
			break;
	}
	step_sp = read_sp();
	saved_PC_address = step_sp + 1 + additional_stack_offset;
	return_address = read_memory_nobpt_unsigned_integer(saved_PC_address, saved_PC_size);
	if (opcode == RET) {
		/* For RET instruction, take into account segment in the case of STM7. */
		return_address += (pc & 0xff0000);
	}
	return (CORE_ADDR)return_address;
}

struct breakpoint_list_type
{
  CORE_ADDR address;
  char *shadow_contents;
  struct breakpoint_list_type *next;
}; 

static struct breakpoint_list_type *step_breakpoint_list_header;
static struct breakpoint_list_type *step_breakpoint_list;

/* Insert a breakpoint through the debugging target
	and store the parameters in a list so as to remove it later.
*/
static void add_step_breakpoint(CORE_ADDR address, int software_breakpoint, char *shadow_contents)
{
	if (software_breakpoint) {
		set_software_breakpoint_implementation_method(shadow_contents);
	} else {
		set_hardware_breakpoint_implementation_method(shadow_contents);
	}
	target_insert_breakpoint (address, shadow_contents);

	if (step_breakpoint_list_header == NULL) {
		step_breakpoint_list_header = (struct breakpoint_list_type *) xmalloc(sizeof(struct breakpoint_list_type));
		step_breakpoint_list = step_breakpoint_list_header;
	} else {
		step_breakpoint_list->next
			= (struct breakpoint_list_type *) xmalloc(sizeof(struct breakpoint_list_type));
		step_breakpoint_list = step_breakpoint_list->next;
    }
	step_breakpoint_list->address = address;
	step_breakpoint_list->shadow_contents = shadow_contents;
	step_breakpoint_list->next = NULL;
}

/* Remove all the breakpoints inserted for software single step.
*/
static void remove_step_breakpoints(void)
{
	int stopped_due_to_software_step_breakpoint = 0;
	step_breakpoint_list = step_breakpoint_list_header;

	while (step_breakpoint_list != NULL) {
		/* We do not handle the case where advanced breakpoints have been used
			to stop when the stack is written to. */

		/* If we want to take into account this case, we can't use the stop reason
			which is not correct any more for software breakpoints as they use the TRAP
			instruction which saves the context into the stack.
			Moreover we need to disable hardware breakpoints in the case
			mentioned above:

						jreq jump
			no_jump:	nop
			jump:		...

			where address(jump) = address(no_jump) + 1.

			Otherwise we might confuse a software breakpoint at no_jump
			with a hardware breakpoint at jump. */

		if (software_breakpoint_implementation_method(step_breakpoint_list->shadow_contents)
			/* We stopped due to a software breakpoint. */
			&& stop_reason_code == di_stopped_by_code_breakpoint
			&& stopped_by_software_breakpoint) {
			if (step_breakpoint_list->address == (stop_pc - DECR_PC_AFTER_BREAK)) {
				stopped_due_to_software_step_breakpoint = 1;
			}
		}
		target_remove_breakpoint(step_breakpoint_list->address, step_breakpoint_list->shadow_contents);
		step_breakpoint_list_header = step_breakpoint_list_header->next;
		free(step_breakpoint_list);
		step_breakpoint_list = step_breakpoint_list_header;
    }
	/* ICD */
	if (stopped_due_to_software_step_breakpoint) {
		write_pc(stop_pc - DECR_PC_AFTER_BREAK);
		stop_pc -= DECR_PC_AFTER_BREAK;
		stop_reason_code = di_single_step;
	}
	if (hardware_breakpoints_disabled)
		restore_hardware_breakpoints(hardware_breakpoints_disabled);
}

/* Is a software breakpoint already inserted here?
*/
static int
step_breakpoint_inserted_here_p(CORE_ADDR pc)
{
	struct breakpoint_list_type *step_breakpoint = step_breakpoint_list_header;
	while (step_breakpoint != NULL) {
		if (step_breakpoint->address == pc) {
			return 1;
		}
		step_breakpoint = step_breakpoint_list->next;
    }
	return 0;
}

/* Set the stepping mode.
*/
static void
set_stepping_mode_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;

  if (strcmp(target_shortname, "gdi") != 0)
    error("set_stepping_mode not available for current target: %s.", target_shortname);

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);

  if (arg_number > 1)
    error("set_stepping_mode can't have more than one argument.");

  if (arg_number == 0)
	printf_filtered("%s\n",
	                (stepping_mode == hardware_stepping_mode) ? "hardware" : "software");
  else
    {
      szarg = next_arg (&arg_list); /* get next argument */
      if (strcmp(szarg, "hardware") == 0) {
		if (debug_instrument_supports_single_step()) {
		  stepping_mode = hardware_stepping_mode;
		} else {
		  error("set_stepping_mode: debug instrument doesn't support single step.");
		}
	  } else if (strcmp(szarg, "software") == 0) {
		stepping_mode = software_stepping_mode;
      } else {
		error("set_stepping_mode: unknown option \"%s\".", szarg);
	  }
    }
  do_cleanups(old_cleanup_chain);
}

/* Display the stepping mode.
*/
static void
show_stepping_mode_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;

  if (strcmp(target_shortname, "gdi") != 0)
    error("show_stepping_mode not available for current target: %s.", target_shortname);

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);

  if (arg_number > 0)
    error("show_stepping_mode has no argument.");

  printf_filtered("%s\n",
	                (stepping_mode == hardware_stepping_mode) ? "hardware" : "software");
  do_cleanups(old_cleanup_chain);
}


void
/* The "_initialize_" keyword is automatically searched at the beginning of the line.
   The "void" keyword must be placed on the preceding line. */
_initialize_st7_stepping(void)
{
	stepping_mode = hardware_stepping_mode;

    add_com ("set_stepping_mode", class_st7,
				(void (*) (char *, int))set_stepping_mode_command,
				"set stepping mode\n\
Usage:\n\
set_stepping_mode {hardware|software}.");

    add_com ("show_stepping_mode", class_st7,
				(void (*) (char *, int))show_stepping_mode_command,
				"show stepping mode\n\
Usage:\n\
show_stepping_mode\n\
hardware\n\
software.");
}
