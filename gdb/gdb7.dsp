# Microsoft Developer Studio Project File - Name="gdb7" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

#	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009 STMicroelectronics

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=gdb7 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "gdb7.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "gdb7.mak" CFG="gdb7 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "gdb7 - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "gdb7 - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName "gdb7"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "gdb7 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /GX /O2 /I "config" /I "../include" /I "." /I "../readline" /I "../include/opcode" /I "../bfd" /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "TARGET_ST7" /D "HOST_HIWARE" /D "HOST_CRASH_B" /D "HAVE_CONFIG_H" /D "HOST_WIN32" /D "HOST_STVISUAL" /D "ALMOST_STDC" /D "TARGET_GDI" /D "BOOL_TYPE" /D "EXIT_CALLBACK" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib libiberty.lib opcodes.lib readline.lib bfd.lib /nologo /subsystem:console /map /machine:I386

!ELSEIF  "$(CFG)" == "gdb7 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /Gm /GX /ZI /Od /I "config" /I "../include" /I "." /I "../readline" /I "../include/opcode" /I "../bfd" /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "TARGET_ST7" /D "HOST_HIWARE" /D "HOST_CRASH_B" /D "HAVE_CONFIG_H" /D "HOST_WIN32" /D "HOST_STVISUAL" /D "ALMOST_STDC" /D "TARGET_GDI" /D "BOOL_TYPE" /D "EXIT_CALLBACK" /FR /YX /FD /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib libiberty.lib opcodes.lib readline.lib bfd.lib /nologo /subsystem:console /incremental:no /map /debug /machine:I386 /pdbtype:sept
# SUBTRACT LINK32 /nodefaultlib

!ENDIF 

# Begin Target

# Name "gdb7 - Win32 Release"
# Name "gdb7 - Win32 Debug"
# Begin Group "Sources"

# PROP Default_Filter ".c"
# Begin Source File

SOURCE=.\annotate.c
# End Source File
# Begin Source File

SOURCE=.\bcache.c
# End Source File
# Begin Source File

SOURCE=.\blockframe.c
# End Source File
# Begin Source File

SOURCE=.\breakpoint.c
# End Source File
# Begin Source File

SOURCE=.\buildsym.c
# End Source File
# Begin Source File

SOURCE=".\c-exp.tab.c"
# End Source File
# Begin Source File

SOURCE=".\c-lang.c"
# End Source File
# Begin Source File

SOURCE=".\c-typeprint.c"
# End Source File
# Begin Source File

SOURCE=".\c-valprint.c"
# End Source File
# Begin Source File

SOURCE=.\cbread.c
# End Source File
# Begin Source File

SOURCE=".\ch-exp.c"
# End Source File
# Begin Source File

SOURCE=".\ch-lang.c"
# End Source File
# Begin Source File

SOURCE=".\ch-typeprint.c"
# End Source File
# Begin Source File

SOURCE=".\ch-valprint.c"
# End Source File
# Begin Source File

SOURCE=.\command.c
# End Source File
# Begin Source File

SOURCE=.\complaints.c
# End Source File
# Begin Source File

SOURCE=.\copying.c
# End Source File
# Begin Source File

SOURCE=.\corefile.c
# End Source File
# Begin Source File

SOURCE=".\cp-valprint.c"
# End Source File
# Begin Source File

SOURCE=".\critical-section.c"
# End Source File
# Begin Source File

SOURCE=.\dbxread.c
# End Source File
# Begin Source File

SOURCE=.\dcache.c
# End Source File
# Begin Source File

SOURCE=.\demangle.c
# End Source File
# Begin Source File

SOURCE=.\dwarf2read.c
# End Source File
# Begin Source File

SOURCE=.\dwarfread.c
# End Source File
# Begin Source File

SOURCE=.\elfread.c
# End Source File
# Begin Source File

SOURCE=.\environ.c
# End Source File
# Begin Source File

SOURCE=.\eval.c
# End Source File
# Begin Source File

SOURCE=.\exec.c
# End Source File
# Begin Source File

SOURCE=.\expprint.c
# End Source File
# Begin Source File

SOURCE=".\f-exp.tab.c"
# End Source File
# Begin Source File

SOURCE=".\f-lang.c"
# End Source File
# Begin Source File

SOURCE=".\f-typeprint.c"
# End Source File
# Begin Source File

SOURCE=".\f-valprint.c"
# End Source File
# Begin Source File

SOURCE=.\findvar.c
# End Source File
# Begin Source File

SOURCE=.\gdbtypes.c
# End Source File
# Begin Source File

SOURCE=".\gnu-regex.c"
# End Source File
# Begin Source File

SOURCE=.\hiread.c
# End Source File
# Begin Source File

SOURCE=.\infcmd.c
# End Source File
# Begin Source File

SOURCE=.\inflow.c
# End Source File
# Begin Source File

SOURCE=.\infrun.c
# End Source File
# Begin Source File

SOURCE=.\language.c
# End Source File
# Begin Source File

SOURCE=.\main.c
# End Source File
# Begin Source File

SOURCE=.\maint.c
# End Source File
# Begin Source File

SOURCE=.\mdebugread.c
# End Source File
# Begin Source File

SOURCE=".\mem-break.c"
# End Source File
# Begin Source File

SOURCE=.\minsyms.c
# End Source File
# Begin Source File

SOURCE=".\msc-xdep.c"
# End Source File
# Begin Source File

SOURCE=.\objfiles.c
# End Source File
# Begin Source File

SOURCE=.\parse.c
# End Source File
# Begin Source File

SOURCE=.\printcmd.c
# End Source File
# Begin Source File

SOURCE=".\remote-gdi.c"
# End Source File
# Begin Source File

SOURCE=".\scm-exp.c"
# End Source File
# Begin Source File

SOURCE=".\scm-lang.c"
# End Source File
# Begin Source File

SOURCE=".\scm-valprint.c"
# End Source File
# Begin Source File

SOURCE=".\ser-tcp.c"
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=".\ser-unix.c"
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\serial.c
# End Source File
# Begin Source File

SOURCE=.\source.c
# End Source File
# Begin Source File

SOURCE=".\st7-stepping.c"
# End Source File
# Begin Source File

SOURCE=".\st7-tdep.c"
# End Source File
# Begin Source File

SOURCE=".\st7emu-extensions.c"
# End Source File
# Begin Source File

SOURCE=.\stabsread.c
# End Source File
# Begin Source File

SOURCE=.\stack.c
# End Source File
# Begin Source File

SOURCE=.\stvisual.c
# End Source File
# Begin Source File

SOURCE=.\symfile.c
# End Source File
# Begin Source File

SOURCE=.\symmisc.c
# End Source File
# Begin Source File

SOURCE=.\symtab.c
# End Source File
# Begin Source File

SOURCE=.\target.c
# End Source File
# Begin Source File

SOURCE=.\thread.c
# End Source File
# Begin Source File

SOURCE=.\top.c
# End Source File
# Begin Source File

SOURCE=.\typeprint.c
# End Source File
# Begin Source File

SOURCE=.\utils.c
# End Source File
# Begin Source File

SOURCE=.\valarith.c
# End Source File
# Begin Source File

SOURCE=.\valops.c
# End Source File
# Begin Source File

SOURCE=.\valprint.c
# End Source File
# Begin Source File

SOURCE=.\values.c
# End Source File
# Begin Source File

SOURCE=.\version.c
# End Source File
# End Group
# Begin Group "Headers"

# PROP Default_Filter ".h"
# Begin Source File

SOURCE=.\annotate.h
# End Source File
# Begin Source File

SOURCE=..\include\ansidecl.h
# End Source File
# Begin Source File

SOURCE=..\include\aout\aout64.h
# End Source File
# Begin Source File

SOURCE=.\bcache.h
# End Source File
# Begin Source File

SOURCE=..\bfd\bfd.h
# End Source File
# Begin Source File

SOURCE=..\include\bfdlink.h
# End Source File
# Begin Source File

SOURCE=.\breakpoint.h
# End Source File
# Begin Source File

SOURCE=.\buildsym.h
# End Source File
# Begin Source File

SOURCE=".\c-lang.h"
# End Source File
# Begin Source File

SOURCE=".\call-cmds.h"
# End Source File
# Begin Source File

SOURCE=..\include\callback.h
# End Source File
# Begin Source File

SOURCE=".\ch-lang.h"
# End Source File
# Begin Source File

SOURCE=..\readline\chardefs.h
# End Source File
# Begin Source File

SOURCE=.\command.h
# End Source File
# Begin Source File

SOURCE=..\include\elf\common.h
# End Source File
# Begin Source File

SOURCE=.\complaints.h
# End Source File
# Begin Source File

SOURCE=.\config.h
# End Source File
# Begin Source File

SOURCE=".\critical-section.h"
# End Source File
# Begin Source File

SOURCE=.\dcache.h
# End Source File
# Begin Source File

SOURCE=.\defs.h
# End Source File
# Begin Source File

SOURCE=..\include\demangle.h
# End Source File
# Begin Source File

SOURCE="..\include\dis-asm.h"
# End Source File
# Begin Source File

SOURCE=..\include\elf\dwarf.h
# End Source File
# Begin Source File

SOURCE=..\include\elf\dwarf2.h
# End Source File
# Begin Source File

SOURCE=..\include\coff\ecoff.h
# End Source File
# Begin Source File

SOURCE="..\bfd\elf-bfd.h"
# End Source File
# Begin Source File

SOURCE=.\environ.h
# End Source File
# Begin Source File

SOURCE=.\expression.h
# End Source File
# Begin Source File

SOURCE=..\include\elf\external.h
# End Source File
# Begin Source File

SOURCE=".\f-lang.h"
# End Source File
# Begin Source File

SOURCE=..\include\floatformat.h
# End Source File
# Begin Source File

SOURCE="..\include\fopen-bin.h"
# End Source File
# Begin Source File

SOURCE=.\frame.h
# End Source File
# Begin Source File

SOURCE=".\gdb-stabs.h"
# End Source File
# Begin Source File

SOURCE=.\gdb_stat.h
# End Source File
# Begin Source File

SOURCE=.\gdb_string.h
# End Source File
# Begin Source File

SOURCE=.\gdbcmd.h
# End Source File
# Begin Source File

SOURCE=.\gdbcore.h
# End Source File
# Begin Source File

SOURCE=.\gdbthread.h
# End Source File
# Begin Source File

SOURCE=.\gdbtypes.h
# End Source File
# Begin Source File

SOURCE=.\gdi.h
# End Source File
# Begin Source File

SOURCE=..\include\getopt.h
# End Source File
# Begin Source File

SOURCE=".\gnu-regex.h"
# End Source File
# Begin Source File

SOURCE=..\include\hicross.h
# End Source File
# Begin Source File

SOURCE=..\readline\history.h
# End Source File
# Begin Source File

SOURCE=.\inferior.h
# End Source File
# Begin Source File

SOURCE=..\include\coff\internal.h
# End Source File
# Begin Source File

SOURCE=..\include\elf\internal.h
# End Source File
# Begin Source File

SOURCE=".\jv-lang.h"
# End Source File
# Begin Source File

SOURCE=..\readline\keymaps.h
# End Source File
# Begin Source File

SOURCE=.\language.h
# End Source File
# Begin Source File

SOURCE=..\bfd\libaout.h
# End Source File
# Begin Source File

SOURCE=..\bfd\libcbmap.h
# End Source File
# Begin Source File

SOURCE=..\bfd\libcoff.h
# End Source File
# Begin Source File

SOURCE=..\bfd\libecoff.h
# End Source File
# Begin Source File

SOURCE=..\bfd\libhicross.h
# End Source File
# Begin Source File

SOURCE=..\include\libiberty.h
# End Source File
# Begin Source File

SOURCE=".\m2-lang.h"
# End Source File
# Begin Source File

SOURCE=..\include\elf\mips.h
# End Source File
# Begin Source File

SOURCE=.\nm.h
# End Source File
# Begin Source File

SOURCE=.\objfiles.h
# End Source File
# Begin Source File

SOURCE=..\include\obstack.h
# End Source File
# Begin Source File

SOURCE=..\include\os9k.h
# End Source File
# Begin Source File

SOURCE=".\parser-defs.h"
# End Source File
# Begin Source File

SOURCE=".\partial-stab.h"
# End Source File
# Begin Source File

SOURCE=..\include\progress.h
# End Source File
# Begin Source File

SOURCE=..\readline\readline.h
# End Source File
# Begin Source File

SOURCE=".\remote-utils.h"
# End Source File
# Begin Source File

SOURCE=".\scm-lang.h"
# End Source File
# Begin Source File

SOURCE=".\scm-tags.h"
# End Source File
# Begin Source File

SOURCE=.\serial.h
# End Source File
# Begin Source File

SOURCE=.\signals.h
# End Source File
# Begin Source File

SOURCE=".\st7-utils.h"
# End Source File
# Begin Source File

SOURCE=.\st7.h
# End Source File
# Begin Source File

SOURCE=..\include\aout\stab.def
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\include\aout\stab_gnu.h
# End Source File
# Begin Source File

SOURCE=.\stabsread.h
# End Source File
# Begin Source File

SOURCE=..\include\coff\sym.h
# End Source File
# Begin Source File

SOURCE=..\include\coff\symconst.h
# End Source File
# Begin Source File

SOURCE=.\symfile.h
# End Source File
# Begin Source File

SOURCE=.\symtab.h
# End Source File
# Begin Source File

SOURCE=.\target.h
# End Source File
# Begin Source File

SOURCE=.\terminal.h
# End Source File
# Begin Source File

SOURCE=".\config\st7\tm-st7.h"
# End Source File
# Begin Source File

SOURCE=.\top.h
# End Source File
# Begin Source File

SOURCE=.\tracepoint.h
# End Source File
# Begin Source File

SOURCE=.\typeprint.h
# End Source File
# Begin Source File

SOURCE=.\valprint.h
# End Source File
# Begin Source File

SOURCE=.\value.h
# End Source File
# Begin Source File

SOURCE=..\include\wait.h
# End Source File
# Begin Source File

SOURCE=".\win32-gdidll-funcnames.h"
# End Source File
# Begin Source File

SOURCE=.\xcoffsolib.h
# End Source File
# Begin Source File

SOURCE=".\config\i386\xm-cygwin32.h"
# End Source File
# Begin Source File

SOURCE=.\xm.h
# End Source File
# End Group
# End Target
# End Project
