/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* Target-dependent code for the ST7 architecture, for GDB, the GNU Debugger.
   Copyright 1988, 1989, 1990, 1991, 1992, 1993 Free Software Foundation, Inc.

This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

#include <string.h>

#include "defs.h"
#include "frame.h"
#include "inferior.h"
#include "symtab.h"
#include "value.h"
#include "gdbcmd.h"
#include "language.h"
#include "gdbcore.h"
#include "symfile.h"
#include "objfiles.h"
#include "hicross.h"
#include "buildsym.h"

/* New class commands */
#include "command.h"
#include "../opcodes/ins7.h"

extern struct cmd_list_element	*cmdlist;

/* TARGET_STM7P is defined! */
#if defined(TARGET_STM7P)

int st7_global_reg_size [NUM_REGS] = { /* size in bits */
/* 0x0 : "PC" : "pc" */0x20,
/* 0x1 : "SP" : "sp" */0x10,
/* 0x2 : "A" : "A" */0x8,
/* 0x3 : "X" : "X" */0x10,
/* 0x4 : "Y" : "Y" */0x10,
/* 0x5 : "CC" : "CC" */0x8,
/* 0x6 : "DATE" : "DATE" */0x40,
/* 0x7 : "DATE_LIMIT" : "DATE_LIMIT" */0x40,
/* 0x8 : "SP_OVERFLOW" : "SP_OVERFLOW" */0x10,
/* 0x9 : "SP_UNDERFLOW" : "SP_UNDERFLOW" */0x10,
/* 0xa : "NB_INST" : "NB_INST" */0x40,
/* 0xb : "NB_INST_MAX" : "NB_INST_MAX" */0x40,
/* 0xc : "FRAME_POINTER" : "FRAME_POINTER" */0x10,
/* 0xd : "TIME" : "TIME" */0x40,
/* 0xe : "TIME_LIMIT" : "TIME_LIMIT" */0x40,
/* 0xf : "CPU_FREQUENCY" : "CPU_FREQUENCY" */0x20,
/* 0x10 : "X_A_REGNUM" : "X_A_REGNUM" */0x10,
/* 0x11 : "XH_REGNUM" : "XH_REGNUM" */0x8,
/* 0x12 : "XL_REGNUM" : "XL_REGNUM" */0x8,
/* 0x13 : "YH_REGNUM" : "YH_REGNUM" */0x8,
/* 0x14 : "YL_REGNUM" : "YL_REGNUM" */0x8,
/* 0x15 : "cx_X_REGNUM" : "cx_X_REGNUM" */0x20,
/* 0x16 : "BX_REGNUM" : "BX_REGNUM" */0x10,
/* 0x17 : "CX_REGNUM" : "CX_REGNUM" */0x10,
/* 0x18 : "BH_REGNUM" : "BH_REGNUM" */0x8,
/* 0x19 : "BL_REGNUM" : "BL_REGNUM" */0x8,
/* 0x1a : "CH_REGNUM" : "CH_REGNUM" */0x8,
/* 0x1b : "CL_REGNUM" : "CL_REGNUM" */0x8,
/* 0x1c : "BX_CX_REGNUM" : "BX_CX_REGNUM" */0x20,
/* 0x1d : "BL_CX_REGNUM" : "BL_CX_REGNUM" */0x20
} ;

int st7_global_reg_byte_pos [NUM_REGS] = { /* pos in bytes */
/* 0x0 : "PC" : "pc" */0x0,
/* 0x1 : "SP" : "sp" */0x4,
/* 0x2 : "A" : "A" */0x6,
/* 0x3 : "X" : "X" */0x7,
/* 0x4 : "Y" : "Y" */0x9,
/* 0x5 : "CC" : "CC" */0xb,
/* 0x6 : "DATE" : "DATE" */0xc,
/* 0x7 : "DATE_LIMIT" : "DATE_LIMIT" */0x14,
/* 0x8 : "SP_OVERFLOW" : "SP_OVERFLOW" */0x1c,
/* 0x9 : "SP_UNDERFLOW" : "SP_UNDERFLOW" */0x1e,
/* 0xa : "NB_INST" : "NB_INST" */0x20,
/* 0xb : "NB_INST_MAX" : "NB_INST_MAX" */0x28,
/* 0xc : "FRAME_POINTER" : "FRAME_POINTER" */0x30,
/* 0xd : "TIME" : "TIME" */0x32,
/* 0xe : "TIME_LIMIT" : "TIME_LIMIT" */0x3a,
/* 0xf : "CPU_FREQUENCY" : "CPU_FREQUENCY" */0x42,
/* 0x10 : "X_A_REGNUM" : "X_A_REGNUM" */0x46,
/* 0x11 : "XH_REGNUM" : "XH_REGNUM" */0x48,
/* 0x12 : "XL_REGNUM" : "XL_REGNUM" */0x49,
/* 0x13 : "YH_REGNUM" : "YH_REGNUM" */0x4a,
/* 0x14 : "YL_REGNUM" : "YL_REGNUM" */0x4b,
/* 0x15 : "cx_X_REGNUM" : "cx_X_REGNUM" */0x4c,
/* 0x16 : "BX_REGNUM" : "BX_REGNUM" */0x50,
/* 0x17 : "CX_REGNUM" : "CX_REGNUM" */0x52,
/* 0x18 : "BH_REGNUM" : "BH_REGNUM" */0x54,
/* 0x19 : "BL_REGNUM" : "BL_REGNUM" */0x55,
/* 0x1a : "CH_REGNUM" : "CH_REGNUM" */0x56,
/* 0x1b : "CL_REGNUM" : "CL_REGNUM" */0x57,
/* 0x1c : "BX_CX_REGNUM" : "BX_CX_REGNUM" */0x58,
/* 0x1d : "BL_CX_REGNUM" : "BL_CX_REGNUM" */0x5c
} ;

#elif defined(TARGET_STM7)

int st7_global_reg_size [NUM_REGS] = { /* size in bits */
/* 0x0 : "PC" : "pc" */0x20,
/* 0x1 : "SP" : "sp" */0x10,
/* 0x2 : "A" : "A" */0x8,
/* 0x3 : "X" : "X" */0x8,
/* 0x4 : "Y" : "Y" */0x8,
/* 0x5 : "CC" : "CC" */0x8,
/* 0x6 : "DATE" : "DATE" */0x40,
/* 0x7 : "DATE_LIMIT" : "DATE_LIMIT" */0x40,
/* 0x8 : "SP_OVERFLOW" : "SP_OVERFLOW" */0x10,
/* 0x9 : "SP_UNDERFLOW" : "SP_UNDERFLOW" */0x10,
/* 0xa : "NB_INST" : "NB_INST" */0x40,
/* 0xb : "NB_INST_MAX" : "NB_INST_MAX" */0x40,
/* 0xc : "FRAME_POINTER" : "FRAME_POINTER" */0x10,
/* 0xd : "TIME" : "TIME" */0x40,
/* 0xe : "TIME_LIMIT" : "TIME_LIMIT" */0x40,
/* 0xf : "CPU_FREQUENCY" : "CPU_FREQUENCY" */0x20
} ;

int st7_global_reg_byte_pos [NUM_REGS] = { /* pos in bytes */
/* 0x0 : "PC" : "pc" */0x0,
/* 0x1 : "SP" : "sp" */0x4,
/* 0x2 : "A" : "A" */0x6,
/* 0x3 : "X" : "X" */0x7,
/* 0x4 : "Y" : "Y" */0x8,
/* 0x5 : "CC" : "CC" */0x9,
/* 0x6 : "DATE" : "DATE" */0xa,
/* 0x7 : "DATE_LIMIT" : "DATE_LIMIT" */0x12,
/* 0x8 : "SP_OVERFLOW" : "SP_OVERFLOW" */0x1a,
/* 0x9 : "SP_UNDERFLOW" : "SP_UNDERFLOW" */0x1c,
/* 0xa : "NB_INST" : "NB_INST" */0x1e,
/* 0xb : "NB_INST_MAX" : "NB_INST_MAX" */0x26,
/* 0xc : "FRAME_POINTER" : "FRAME_POINTER" */0x2e,
/* 0xd : "TIME" : "TIME" */0x30,
/* 0xe : "TIME_LIMIT" : "TIME_LIMIT" */0x38,
/* 0xf : "CPU_FREQUENCY" : "CPU_FREQUENCY" */0x40
} ;

#else /* ST7 */

int st7_global_reg_size [NUM_REGS] = { /* size in bits */
/* 0x0 : "PC" : "pc" */0x10,
/* 0x1 : "SP" : "sp" */0x10,
/* 0x2 : "A" : "A" */0x8,
/* 0x3 : "X" : "X" */0x8,
/* 0x4 : "Y" : "Y" */0x8,
/* 0x5 : "CC" : "CC" */0x8,
/* 0x6 : "DATE" : "DATE" */0x40,
/* 0x7 : "DATE_LIMIT" : "DATE_LIMIT" */0x40,
/* 0x8 : "SP_OVERFLOW" : "SP_OVERFLOW" */0x10,
/* 0x9 : "SP_UNDERFLOW" : "SP_UNDERFLOW" */0x10,
/* 0xa : "NB_INST" : "NB_INST" */0x40,
/* 0xb : "NB_INST_MAX" : "NB_INST_MAX" */0x40,
/* 0xc : "FRAME_POINTER" : "FRAME_POINTER" */0x10,
/* 0xd : "TIME" : "TIME" */0x40,
/* 0xe : "TIME_LIMIT" : "TIME_LIMIT" */0x40,
/* 0xf : "CPU_FREQUENCY" : "CPU_FREQUENCY" */0x20
} ;

int st7_global_reg_byte_pos [NUM_REGS] = { /* pos in bytes */
/* 0x0 : "PC" : "pc" */0x0,
/* 0x1 : "SP" : "sp" */0x2,
/* 0x2 : "A" : "A" */0x4,
/* 0x3 : "X" : "X" */0x5,
/* 0x4 : "Y" : "Y" */0x6,
/* 0x5 : "CC" : "CC" */0x7,
/* 0x6 : "DATE" : "DATE" */0x8,
/* 0x7 : "DATE_LIMIT" : "DATE_LIMIT" */0x10,
/* 0x8 : "SP_OVERFLOW" : "SP_OVERFLOW" */0x18,
/* 0x9 : "SP_UNDERFLOW" : "SP_UNDERFLOW" */0x1a,
/* 0xa : "NB_INST" : "NB_INST" */0x1c,
/* 0xb : "NB_INST_MAX" : "NB_INST_MAX" */0x24,
/* 0xc : "FRAME_POINTER" : "FRAME_POINTER" */0x2c,
/* 0xd : "TIME" : "TIME" */0x2e,
/* 0xe : "TIME_LIMIT" : "TIME_LIMIT" */0x36,
/* 0xf : "CPU_FREQUENCY" : "CPU_FREQUENCY" */0x3e
} ;

#endif

/* The "SP_OVERFLOW" register is set to SP min: 0x100 for the ST72334 */
static CORE_ADDR get_sp_min(void)
{
	return read_register (SP_OVERFLOW_REGNUM);
}

/* The "SP_UNDERFLOW" register is set to SP max: 0x1ff for the ST72334 */
static CORE_ADDR get_sp_max(void)
{
	return read_register (SP_UNDERFLOW_REGNUM);
}

enum compiler_vendor_enum {
	no_compiler,
	cosmic_compiler,
	metrowerks_compiler,
	raisonance_compiler
};

static enum compiler_vendor_enum compiler_vendor = no_compiler;

/* Recognize compiler vendor from DWARF2 attribute producer. */
void set_compiler_vendor(char *producer)
{
	if (strstr(producer, "COSMIC")) {
		compiler_vendor = cosmic_compiler;
	} else if (strstr(producer, "METROWERKS")) {
		compiler_vendor = metrowerks_compiler;
	} else if (strstr(producer, "Raisonance")) {
		compiler_vendor = raisonance_compiler;
	} else {
		error("GDB7 internal error: compiler vendor %s not supported.", producer);
	}
}

int is_cosmic_compiler_used(struct objfile *symfile_objfile)
{
	return (symfile_objfile
			&& bfd_get_flavour(symfile_objfile->obfd) == bfd_target_elf_flavour
			&& compiler_vendor == cosmic_compiler);
}

static int is_metrowerks_compiler_used(struct objfile *symfile_objfile)
{
	return (symfile_objfile
			&& (bfd_get_flavour(symfile_objfile->obfd) == bfd_target_hicross_flavour
				|| (bfd_get_flavour(symfile_objfile->obfd) == bfd_target_elf_flavour
					&& compiler_vendor == metrowerks_compiler)));
}

int is_raisonance_compiler_used(struct objfile *symfile_objfile)
{
	return (symfile_objfile
			&& bfd_get_flavour(symfile_objfile->obfd) == bfd_target_elf_flavour
			&& compiler_vendor == raisonance_compiler);
}

static int hiware_library_routine_alters_stack_pointer(char *function_name)
{
  /* These HIWARE library routines pop up the arguments that have been pushed
	 before the call. */
  if (function_name &&
	  (STREQ(function_name, "_IDIVS") /* Unstack 2 bytes. */
	   || STREQ(function_name, "_IDIVU") /* Unstack 2 bytes. */
	   || STREQ(function_name, "_IMODS") /* Unstack 2 bytes. */
	   || STREQ(function_name, "_IMODU") /* Unstack 2 bytes. */
	   || STREQ(function_name, "_IMULS") /* Unstack 2 bytes. */
	   || STREQ(function_name, "_IMULU") /* Unstack 2 bytes. */
	   || STREQ(function_name, "_LASR") /* Unstack 1 byte. */
	   || STREQ(function_name, "_LDIVS") /* Unstack 4 bytes. */
	   || STREQ(function_name, "_LDIVU") /* Unstack 4 bytes. */
	   || STREQ(function_name, "_LIMULS") /* Unstack 2 bytes. */
	   || STREQ(function_name, "_LIMULU") /* Unstack 2 bytes. */
	   || STREQ(function_name, "_LLSL") /* Unstack 1 byte. */
	   || STREQ(function_name, "_LLSR") /* Unstack 1 byte. */
	   || STREQ(function_name, "_LMODS") /* Unstack 4 bytes. */
	   || STREQ(function_name, "_LMODU") /* Unstack 4 bytes. */
	   || STREQ(function_name, "_LMULS") /* Unstack 4 bytes. */
	   || STREQ(function_name, "_LMULU"))) /* Unstack 4 bytes. */
    return 1;
  else
	return 0;
}

static int raisonance_library_routine_alters_stack_pointer(char *function_name)
{
  /* These Raisonance library routines alter the stack pointer. */
  if (function_name &&
	  (
	      STREQ(function_name, "?C?add2416") /* Unstack 2 bytes. */
	   || STREQ(function_name, "?C?add3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?addix32") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?and3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?cps3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?cpu3232") /* Unstack 4 byte. */
	   || STREQ(function_name, "?C?divs3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?divsix32") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?divu3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?divuix32") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?indcall") /* Unstack 2 bytes. */
	   || STREQ(function_name, "?C?mods3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?modu3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?mulu3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?muluix32") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?or3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?pop4") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?sub2416") /* Unstack 2 bytes. */
	   || STREQ(function_name, "?C?sub3232") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?subix32") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?switch") /* Unstack 3 bytes. */
	   || STREQ(function_name, "?C?xor3232") /* Unstack 4 bytes. */

	   /* Float routines */
	   || STREQ(function_name, "?C?addfp") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?addfpix") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?cpfp") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?divfp") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?divfpix") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?mulfp") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?mulfpix") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?pow8fp") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?subfp") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?subfpix") /* Unstack 4 bytes. */
	   || STREQ(function_name, "?C?uchartofp_stack") /* Stack 4 bytes. */
	   || STREQ(function_name, "_?unpack_tstfp?") /* Stack 7 bytes. */
	   || STREQ(function_name, "_?fp_exit?") /* Unstack 9 bytes. */
	   || STREQ(function_name, "_?infinite_result?") /* Unstack 11 bytes. */
	   || STREQ(function_name, "_?nan_result?") /* Unstack 11 bytes. */
	   || STREQ(function_name, "_?zero_result?") /* Unstack 11 bytes. */

	   || STREQ(function_name, "?C?mv2_cx2is12") /* Unstack 3 bytes. */
	   || STREQ(function_name, "?C?mv2_cx2is3") /* Unstack 3 bytes. */
	   || STREQ(function_name, "?C?mv3_bc2is") /* Unstack 3 bytes. */
	   || STREQ(function_name, "?C?mv3_bc2is3") /* Unstack 3 bytes. */
	   || STREQ(function_name, "?C?mv4_bc2is") /* Unstack 2 bytes. */
	   || STREQ(function_name, "?C?mv4_bc2is12") /* Unstack 3 bytes. */
	   || STREQ(function_name, "?C?mv4_bc2is3") /* Unstack 3 bytes. */
	   || STREQ(function_name, "?C?mv4_bc2sk") /* Stack 4 bytes. */
	   || STREQ(function_name, "?C?mv4_ix2sk") /* Stack 4 bytes. */
	   || STREQ(function_name, "?C?mv4_iy2sk") /* Stack 4 bytes. */
	   || STREQ(function_name, "?C?mv4_pg2sk") /* Stack 4 bytes. */
	   || STREQ(function_name, "?C?mv4_pg2sk1") /* Stack 4 bytes. */
	   )) 
    return 1;
  else
	return 0;
}

int library_routine_alters_stack_pointer(char *function_name)
{
	if (symfile_objfile) {
		if (is_metrowerks_compiler_used(symfile_objfile)) {
			return hiware_library_routine_alters_stack_pointer(function_name);
		} else if (is_raisonance_compiler_used(symfile_objfile)) {
			return raisonance_library_routine_alters_stack_pointer(function_name);
		} else {
			return 0;
		}
	} else {
		error("No symbol table is loaded; can't find compiler vendor.");
		return 0;
	}
}

/* Some Raisonance library routines modify the return PC! */
static
void handle_raisonance_library_routine_altering_return_pc(char *function_name, CORE_ADDR *return_pc)
{
	/* These Raisonance library routines alter the return PC. */
	if (function_name)  {
		if (   STREQ(function_name, "?C?mv4_pg2bc1")
			|| STREQ(function_name, "?C?mv4_pg2ix1")
			|| STREQ(function_name, "?C?mv4_pg2sk1")
			) {
			/* Increment PC by 1 byte. */
			*return_pc += 1;
		} else if (    STREQ(function_name, "?C?mv4_pg2bc")
					|| STREQ(function_name, "?C?mv4_pg2ib")
					|| STREQ(function_name, "?C?mv4_pg2isa")
					|| STREQ(function_name, "?C?mv4_pg2ix")
					|| STREQ(function_name, "?C?mv4_pg2sk")
			) {
			/* Increment PC by 4 bytes. */
			*return_pc += 4;
		}
	} else {
		error("No function name.");
	}
}

/* Some library routines modify the return PC! */
void handle_library_routine_altering_return_pc(char *function_name, CORE_ADDR *return_pc)
{
	if (symfile_objfile) {
		if (is_raisonance_compiler_used(symfile_objfile)) {
			handle_raisonance_library_routine_altering_return_pc(function_name, return_pc);
		}
	} else {
		error("No symbol table is loaded; can't find compiler vendor.");
	}
}

/* Is it a stub function reaching a pointed far function?
   Cosmic STM7 compiler
*/
int is_Cosmic_STM7_far_function_pointer_call(char *function_name)
{
  /* These Cosmic library routines push the pointed function address into the stack
	and jump into the pointed function using a RETF. */
  if (symfile_objfile) {
	if (is_cosmic_compiler_used(symfile_objfile)) {
		if (function_name
			&& (STREQ(function_name, "c_callfx")
				|| STREQ(function_name, "c_callfy")
				|| STREQ(function_name, "d_callfx")
				|| STREQ(function_name, "d_callfy"))) {
			return 1;
		} else {
			return 0;
		}
	} else {
		return 0;
	}
  } else {
	error("No symbol table is loaded; can't find compiler vendor.");
	return 0;
  }
}

/* Get address of pointed function for far pointers
   Cosmic STM7 compiler
*/
CORE_ADDR get_Cosmic_STM7_far_function_address(char *stub_function_name)
{
	CORE_ADDR func_addr;
	char pcl, pch, pce;
	char *pseudo_reg_name;
	CORE_ADDR pseudo_reg_addr;
	if (STREQ(stub_function_name, "c_callfx") || STREQ(stub_function_name, "d_callfx")) {
		/*  PCL is in X
			PCH is in c_x +1
			PCE is in c_x
			*/
		pcl = (char)read_register(X_REGNUM);
		pseudo_reg_name = "&c_x";
	} else {
		/*  PCL is in Y
			PCH is in c_y +1
			PCE is in c_y
			*/
		pcl = (char)read_register(Y_REGNUM);
		pseudo_reg_name = "&c_y";
	}
	pseudo_reg_addr = parse_and_eval_address_1(&pseudo_reg_name);
	read_memory_nobpt(pseudo_reg_addr + 1, &pch, 1);
	read_memory_nobpt(pseudo_reg_addr, &pce, 1);
	func_addr = (unsigned char)pcl + (((unsigned char)pch) << 8) + (((unsigned char)pce) << 16);
	return func_addr;
}

/* Compute the stack pointer offset after the execution of a function call.
	It is zero for user functions.
	Take into account Hiware library routines which alter the stack pointer. 
*/
static int stack_pointer_offset_after_hiware_function_execution(char *function_name)
{
	/* These HIWARE library routines pop up the arguments that have been pushed before the call. */
	if (STREQ(function_name, "_LASR") /* Unstack 1 byte. */
		|| STREQ(function_name, "_LLSL") /* Unstack 1 byte. */
		|| STREQ(function_name, "_LLSR") /* Unstack 1 byte. */
		) {
		return -1;
	} else if (STREQ(function_name, "_IDIVS") /* Unstack 2 bytes. */
			   || STREQ(function_name, "_IDIVU") /* Unstack 2 bytes. */
			   || STREQ(function_name, "_IMODS") /* Unstack 2 bytes. */
			   || STREQ(function_name, "_IMODU") /* Unstack 2 bytes. */
			   || STREQ(function_name, "_IMULS") /* Unstack 2 bytes. */
			   || STREQ(function_name, "_IMULU") /* Unstack 2 bytes. */
			   || STREQ(function_name, "_LIMULS") /* Unstack 2 bytes. */
			   || STREQ(function_name, "_LIMULU") /* Unstack 2 bytes. */
			   ) {
		return -2;
	} else if (STREQ(function_name, "_LDIVS") /* Unstack 4 bytes. */
			   || STREQ(function_name, "_LDIVU") /* Unstack 4 bytes. */
			   || STREQ(function_name, "_LMODS") /* Unstack 4 bytes. */
			   || STREQ(function_name, "_LMODU") /* Unstack 4 bytes. */
			   || STREQ(function_name, "_LMULS") /* Unstack 4 bytes. */
			   || STREQ(function_name, "_LMULU") /* Unstack 4 bytes. */
			   ) {
		return -4;
	} else {
		return 0;
	}
}

/* Compute the stack pointer offset after the execution of a function call.
	It is zero for user functions.
	Take into account Raisonance library routines which alter the stack pointer. 
*/
static int
stack_pointer_offset_after_raisonance_function_execution(char *function_name, int far_call)
{
	int near_call = !far_call;
	/* These Raisonance library routines alter the stack pointer. */
	if (STREQ(function_name, "_?unpack_tstfp?")
		) {
		return 7; /* Stack 7 bytes. */
	} else if (STREQ(function_name, "?C?uchartofp_stack")
				|| STREQ(function_name, "?C?mv4_bc2sk")
				|| STREQ(function_name, "?C?mv4_ix2sk")
				|| STREQ(function_name, "?C?mv4_iy2sk")
				|| STREQ(function_name, "?C?mv4_pg2sk")
				|| STREQ(function_name, "?C?mv4_pg2sk1")
				) {
		return 4; /* Stack 4 bytes. */
	} else if (STREQ(function_name, "?C?add2416")
				|| STREQ(function_name, "?C?sub2416")
				|| (STREQ(function_name, "?C?indcall") && near_call)
				|| STREQ(function_name, "?C?mv4_bc2is")
				) {
		return -2; /* Unstack 2 bytes. */
	} else if ((STREQ(function_name, "?C?indcall") && far_call)
				|| STREQ(function_name, "?C?switch")
				|| STREQ(function_name, "?C?mv2_cx2is12")
				|| STREQ(function_name, "?C?mv2_cx2is3")
				|| STREQ(function_name, "?C?mv3_bc2is")
				|| STREQ(function_name, "?C?mv3_bc2is3")
				|| STREQ(function_name, "?C?mv4_bc2is12")
				|| STREQ(function_name, "?C?mv4_bc2is3")
				) {
		return -3; /* Unstack 3 bytes. */
	} else if (STREQ(function_name, "?C?add3232")
				|| STREQ(function_name, "?C?addix32")
				|| STREQ(function_name, "?C?and3232")
				|| STREQ(function_name, "?C?cps3232")
				|| STREQ(function_name, "?C?cpu3232")
				|| STREQ(function_name, "?C?divs3232")
				|| STREQ(function_name, "?C?divsix32")
				|| STREQ(function_name, "?C?divu3232")
				|| STREQ(function_name, "?C?divuix32")
				|| STREQ(function_name, "?C?mods3232")
				|| STREQ(function_name, "?C?modu3232")
				|| STREQ(function_name, "?C?mulu3232")
				|| STREQ(function_name, "?C?muluix32")
				|| STREQ(function_name, "?C?or3232")
				|| STREQ(function_name, "?C?sub3232")
				|| STREQ(function_name, "?C?subix32")
				|| STREQ(function_name, "?C?xor3232")
				|| STREQ(function_name, "?C?pop4")
				|| STREQ(function_name, "?C?addfp")
				|| STREQ(function_name, "?C?addfpix")
				|| STREQ(function_name, "?C?cpfp")
				|| STREQ(function_name, "?C?divfp")
				|| STREQ(function_name, "?C?divfpix")
				|| STREQ(function_name, "?C?mulfp")
				|| STREQ(function_name, "?C?mulfpix")
				|| STREQ(function_name, "?C?pow8fp")
				|| STREQ(function_name, "?C?subfp")
				|| STREQ(function_name, "?C?subfpix")
				) {
		return -4; /* Unstack 4 bytes. */
	} else if (STREQ(function_name, "_?fp_exit?")
				) {
		return -9; /* Unstack 9 bytes. */
	} else if (STREQ(function_name, "_?infinite_result?")
				|| STREQ(function_name, "_?nan_result?")
				|| STREQ(function_name, "_?zero_result?")
				) {
		return -11; /* Unstack 11 bytes. */
	} else {
		return 0;
	}
}

/* Compute the stack pointer offset after the execution of a function call.
	It is zero for user functions. */
static int stack_pointer_offset_after_function_execution(char *function_name, int far_call)
{
	if (function_name) {
		if (symfile_objfile) {
			if (is_metrowerks_compiler_used(symfile_objfile)) {
				return stack_pointer_offset_after_hiware_function_execution(function_name);
			} else if (is_raisonance_compiler_used(symfile_objfile)) {
				return stack_pointer_offset_after_raisonance_function_execution(function_name, far_call);
			} else {
				return 0;
			}
		} else {
			error("No symbol table is loaded; can't find compiler vendor.");
			return 0;
		}
	} else {
		error("No function name.");
		return 0;
	}
}

void init_extra_frame_info(struct frame_info *fi)
{
  fi->init_saved_regs = 0;
  fi->init_sp_after_call = 0;
  fi->sp_after_call = 0;
  fi->init_call_type = 0;
  fi->call_type = 0;
  fi->interrupt_handler = 0;
}

static int stack_pointer_offset(CORE_ADDR start_pc,
								CORE_ADDR stop_pc,
								int current_frame,
								int *offset);

static CORE_ADDR compute_sp_after_call(struct frame_info *frame);

CORE_ADDR
get_sp_after_call(struct frame_info *frame)
{
  if (!frame->init_sp_after_call)
	{
	  frame->sp_after_call = compute_sp_after_call(frame);
	  frame->init_sp_after_call = 1;
	}
  return frame->sp_after_call;
}

struct list_int
{
  int n;
  int *list;
};

static void add_int_to_list(int i, struct list_int *li)
{
  li->n += 1;
  if (li->n == 1)
	li->list = (int *) xmalloc(sizeof(int));
  else
	li->list = (int *) xrealloc(li->list, li->n * sizeof(int));
  li->list[li->n - 1] = i;
}

static int identical_elements_in_list(struct list_int li)
{
  int ix;

  if (li.n == 0)
	return 0;
  for (ix = 1; ix < li.n; ix++)
	{
	  if (li.list[ix] != li.list[0])
		return 0;
	}
  return 1;
}

static int empty_list(struct list_int li)
{
  return li.n == 0;
}

struct result
{
  CORE_ADDR start_address;
  int SP_offset;
};

struct result_list
{
  int n;
  struct result *list;
};

static void add_result_to_list(CORE_ADDR address, int offset, struct result_list *rl)
{
  rl->n += 1;
  if (rl->n == 1)
	rl->list = (struct result *) xmalloc(sizeof(struct result));
  else
	rl->list = (struct result *) xrealloc(rl->list, rl->n * sizeof(struct result));
  (rl->list[rl->n - 1]).start_address = address;
  (rl->list[rl->n - 1]).SP_offset = offset;
}

static int get_result_from_list(CORE_ADDR address, struct result_list *rl, int *offset)
{
  int i;

  for (i = 0; i < rl->n; i++)
	{
	  if ((rl->list[i]).start_address == address)
		{
		  *offset = (rl->list[i]).SP_offset;
		  return 1;
		}
	}
  return 0;
}

static void initialize_result_list(struct result_list *rl)
{
  rl->n = 0;
}

static FILE *debug_file;

static void display_result_list(struct result_list rl)
{
	int i;

	if (rl.n)
	  fprintf(debug_file, "Start address and SP offset list:\n");
	for (i = 0; i < rl.n; i++)
	  fprintf(debug_file, "\t0x%04x %d\n", (rl.list[i]).start_address, (rl.list[i]).SP_offset);
}

static void free_result_list(struct result_list rl)
{
  if (rl.n)
    free(rl.list);
}

CORE_ADDR
st7_skip_prologue (CORE_ADDR start_pc);

static int
get_real_function(struct frame_info *frame, struct symbol **function);

/* This function is used to compute the value of the stack pointer
   just after the call that is before having executed the first
   instruction of the called function.
   We need to do this because the ST7 has no frame pointer.

   We return 0 if we can't find the value of the stack pointer
   at the beginning of the called function.
*/
static CORE_ADDR
compute_sp_after_call(struct frame_info *frame)
{
  int sp_offset_to_last_C_line_reached = 0;
  int total_sp_offset = 0;
  int debug_info = 0;
  int crash_barrier_executable = 0;
  CORE_ADDR start_pc;

  if (symfile_objfile)
	{
	  if (/* Executable in HIWARE proprietary format generated by the HIWARE compiler. */
		  bfd_get_flavour(symfile_objfile->obfd) == bfd_target_hicross_flavour
		  /* Executable in ELF/DWARF2 format generated by the HIWARE compiler:
			 the .debug_loc section should not be empty. */
		  || (/* Executable in ELF/DWARF2 format. */
			  bfd_get_flavour(symfile_objfile->obfd) == bfd_target_elf_flavour
			  /* Test for non empty ".debug_loc" section. */
			  && dwarf_loc_size != 0))
		{
		  /* The HIWARE compiler provides a list of (PC offset, SP offset) pairs:
			 at a given offset from the PC, the SP offset is recorded when it changes.
			 This list is recorded differently according to the executable format:
			 - it is stored individually in each function for the HIWARE proprietary format,
			 - it is stored with other function lists in the .debug_loc section
			 for the ELF/DWARF2 format. */
	      struct symbol *func_sym = get_frame_function(frame);
		  struct block *func_block = NULL;
		  if (func_sym)
			{
			  func_block = SYMBOL_BLOCK_VALUE(func_sym);
			}
		  if (func_block)
			{
			  if (func_block->nPC_SP_pairs)
				{
				  /* We have debug information. */
				  int pc_offset = frame->pc - BLOCK_START(func_block);
				  hicross_proc_PC_SP_pair_type *PC_SP_pairs = func_block->PC_SP_pairs;
				  int n_pairs = func_block->nPC_SP_pairs;
				  int pc_range_found = 0;
			
				  while (!pc_range_found && n_pairs)
					{
					  if ((get_current_frame() == frame && pc_offset < PC_SP_pairs->PC)
						  /* If we are not in the current frame, frame->pc is not the current PC
						     but the PC after the return of the calling function. What we would
						     like to have is the PC before the call. Change the comparison because
						     in some cases, the SP before the call is not the same as the SP
						     after the call. So make sure we don't reach the return PC. */
						  || (get_current_frame() != frame && pc_offset <= PC_SP_pairs->PC))					  
						pc_range_found = 1;
					  else
						total_sp_offset = PC_SP_pairs->SP;
					  n_pairs--;
					  if (n_pairs)
						PC_SP_pairs++;
					}
				  return frame->frame + total_sp_offset;
				}
			  else
			    /* No (PC offset, SP offset) pair therefore no stack pointer variation. */
				return frame->frame;
			}
		}

	  /* Executable in ELF/DWARF2 format generated by the COSMIC compiler. */
	  if (/* Executable in ELF/DWARF2 format. */
		  bfd_get_flavour(symfile_objfile->obfd) == bfd_target_elf_flavour
		  /* Test for non empty ".debug_frame" section. */
		  && dwarf_frame_size != 0)
		{
		  /* The COSMIC compiler generates a .debug_frame section from which
		     the value of the SP offset can be obtained. */
		  struct symtab_and_line sal;
		  int where[NUM_REGS];

		  /* If we are not in the current frame, we should look at the line including
		     (frame->pc - 1) since the return PC is the PC after the call.
			 This is taken into account by the second parameter "notcurrent" of find_pc_line.
			 This will allow to take into account the case where the call to the function
			 is the last assembly language instruction of the line. */
		  sal = find_pc_line(frame->pc, get_current_frame() != frame);
		  if (dwarf2_find_saved_regs(frame, where))
			{
			  if (where[SP_REGNUM] != UNKNOWN_VALUE)
				{
				  sp_offset_to_last_C_line_reached = where[SP_REGNUM];
				  /* However the COSMIC compiler doesn't generate stack pointer information
					 for each assembly language instruction but for each C line.
				     If we are in the middle of a C instruction, we need to disassemble. */
				  if (frame->pc == sal.pc)
					{
					  total_sp_offset = sp_offset_to_last_C_line_reached;
					  return frame->frame + total_sp_offset;
					}
				}
			}
	      if (sal.symtab)
			{
			  debug_info = 1;
			  start_pc = sal.pc;
			}
		}

	  /* Executable in ELF/DWARF2 format generated by the RAISONANCE compiler. */
	  if (is_raisonance_compiler_used(symfile_objfile))
		{
		  /* The RAISONANCE compiler doesn't modify the SP between two lines except during the prologue.
			 Just compute the SP variation within the current line and add it to the SP variation in the prologue. */
		  struct symtab_and_line sal;
	      struct symbol *func_sym;
		  /* If we are not in the current frame, we should look at the line including
		     (frame->pc - 1) since the return PC is the PC after the call.
			 This is taken into account by the second parameter "notcurrent" of find_pc_line.
			 This will allow to take into account the case where the call to the function
			 is the last assembly language instruction of the line. */
		  sal = find_pc_line(frame->pc, get_current_frame() != frame);
		  func_sym = get_frame_function(frame);

		  /* Debug information has been found.
			 The code is in a C source file and a function has been found. */
	      if (sal.symtab && (sal.symtab)->language == language_c && func_sym)
			{
			  CORE_ADDR pc_after_prologue;
			  start_pc = BLOCK_START(SYMBOL_BLOCK_VALUE(func_sym));
			  pc_after_prologue = st7_skip_prologue(start_pc);
			  /* Check that we are not inside the prologue.
				 Say that we are in the current frame otherwise the last instruction is skipped. */
			  if ((pc_after_prologue <= frame->pc)
				  && stack_pointer_offset(start_pc, pc_after_prologue, 1, &sp_offset_to_last_C_line_reached))
				{
				  if (frame->pc == sal.pc)
					{
					  total_sp_offset = sp_offset_to_last_C_line_reached;
					  return frame->frame + total_sp_offset;
					}
				  start_pc = sal.pc;
				}
			  debug_info = 1;
			}
		}

	  if (CRASH_BARRIER_EXECUTABLE_FILE())
		{
		  struct symtab_and_line sal;
		  /* Executable in CRASH BARRIER format. */
		  crash_barrier_executable = 1;
		  sal = find_pc_line(frame->pc, 0);
		  if (sal.symtab)
			{
			  struct symbol *function;

			  debug_info = 1;

			  if (get_real_function(frame, &function))
				start_pc = BLOCK_START(SYMBOL_BLOCK_VALUE(function));
			  else
				return 0;
			}
		  else
			return 0;
		}
	  if (!debug_info)
		{
		  /* There is no debug information: we try to find a minimal symbol. */
		  struct minimal_symbol *func_minsym;
		  func_minsym = lookup_minimal_symbol_by_pc(frame->pc);
		  if (func_minsym)
			start_pc = SYMBOL_VALUE_ADDRESS(func_minsym);
		  else
			return 0;
		  /* When there is no debug information, sometimes we find a minimal symbol which is irrelevant
			and we end up with "GDB7 internal error: probable infinite recursion in compute_stack_pointer_offset."
			in compute_stack_pointer_offset.
			lookup_minimal_symbol_by_pc returns the closest minimal symbol with a lower address.
			If we are too far from frame->pc, don't bother to disassemble as it will take too much time. */
		  if ((frame->pc - start_pc) > 0x400) {
			return 0;
		  }
		}
	  /* If there is debug information and we reached here, that means that we have
		 either an executable generated by the Crash Barrier assembler with no stack information
		 or generated by the COSMIC C compiler with incomplete stack information
		 or generated by the RAISONANCE C compiler with no stack information.

		 In the second case, we add to the current frame pointer,
		 the stack pointer offset (found in the debug information)
		 between the beginning of the function and the beginning of the current C line 
		 and the stack pointer offset between the beginning of the current line and
		 the current assembly language instruction.

		 In the third case, we add to the current frame pointer,
		 the stack pointer offset in the prologue
		 and the stack pointer offset between the beginning of the current C line and
		 the current assembly language instruction.
	  */
	  /* If there is no debug information, we need to disassemble the code between
		 the start of the function and the last PC reached before we can compute
		 the stack pointer offset.
	  */
	  if (stack_pointer_offset(start_pc, frame->pc, get_current_frame() == frame,
		                       &total_sp_offset))
		{
		  if (debug_info && !crash_barrier_executable) {
			total_sp_offset += sp_offset_to_last_C_line_reached;
		  }
		  return frame->frame + total_sp_offset;
		}
	}
  return 0;
}

/* Compute the value of the stack pointer after the end of the prologue. */
static CORE_ADDR
compute_sp_after_prologue(struct frame_info *frame)
{
  int prologue_sp_offset;
  CORE_ADDR start_pc, pc_after_prologue;
  struct symbol *func_sym;
  func_sym = get_frame_function(frame);
  if (func_sym)
	{
	  start_pc = BLOCK_START(SYMBOL_BLOCK_VALUE(func_sym));
	}
  else
	{
       struct minimal_symbol *func_minsym;
	   func_minsym = lookup_minimal_symbol_by_pc(frame->pc);
	   if (func_minsym)
	   {
		 start_pc = SYMBOL_VALUE_ADDRESS(func_minsym);
	   }
	   else
	   {
		 return 0;
	   }
	}
  pc_after_prologue = st7_skip_prologue(start_pc);
  if (stack_pointer_offset(start_pc, pc_after_prologue, 1, &prologue_sp_offset))
	{
	  CORE_ADDR sp_after_call = get_sp_after_call(frame);
	  if (sp_after_call) {
		return sp_after_call - prologue_sp_offset;
	  }
	}
  return 0;
}

static int
dis_asm_read_memory_nobpt(bfd_vma memaddr, bfd_byte *myaddr, int len, disassemble_info *info)
{
  return read_memory_nobpt(memaddr, (char *) myaddr, len);
}

static void restore_disassembler_read_memory_function(void)
{
  tm_print_insn_info.read_memory_func = dis_asm_read_memory;
}

/* Disassemble instruction starting at "memaddr"
   and store the disassembled instruction in "disass_instr_buffer".
   Test if memory is readable in function of "memory_check".
   Return the length of the disassembled instruction
   or -1 if there is an error.
*/
int disassemble_instruction(bfd_vma memaddr, char *disass_instr_buffer, int memory_check)
{
  int length = -1;
  struct cleanup *cleanups = NULL;

  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
	tm_print_insn_info.endian = BFD_ENDIAN_BIG;
  else
	tm_print_insn_info.endian = BFD_ENDIAN_LITTLE;

  if (target_architecture != NULL)
	tm_print_insn_info.mach = target_architecture->mach;

  tm_print_insn_info.read_memory_func = dis_asm_read_memory_nobpt;
  cleanups = make_cleanup((void (*) (char *))restore_disassembler_read_memory_function,
	                      (char *)NULL);

  if (!memory_check || is_disassemble_possible(memaddr)) {
	length = get_disassembled_instruction(memaddr, &tm_print_insn_info, disass_instr_buffer);
  }
  do_cleanups(cleanups);
  return length;
}

#ifdef debug_compute_stack_pointer_offset
static
int get_code_and_disassemble_instruction(bfd_vma memaddr, char *hexa_instr_buffer, char *disass_instr_buffer)
{
  int length;
  struct cleanup *cleanups = NULL;

  if (TARGET_BYTE_ORDER == BIG_ENDIAN)
	tm_print_insn_info.endian = BFD_ENDIAN_BIG;
  else
	tm_print_insn_info.endian = BFD_ENDIAN_LITTLE;

  if (target_architecture != NULL)
	tm_print_insn_info.mach = target_architecture->mach;

  tm_print_insn_info.read_memory_func = dis_asm_read_memory_nobpt;
  cleanups = make_cleanup((void (*) (char *))restore_disassembler_read_memory_function,
	                      (char *)NULL);
  length = get_code_and_disassembled_instruction(memaddr, &tm_print_insn_info, hexa_instr_buffer, disass_instr_buffer);
  do_cleanups(cleanups);
  return length;
}
#endif

static int is_valid_integer(const char *string, long *result)
{
  long value;
  char *end = NULL;

  value = strtol(string, &end, 0);
  if (result)
    *result = value;
  return (end && (end != string) && (*end == '\0' || isspace (*end)));
}

static int compute_stack_pointer_offset_for_all_paths
	(CORE_ADDR start_pc,
	 CORE_ADDR stop_pc,
	 int current_frame,
	 int *offset,
	 int *cant_compute_stack_offset,
	 CORE_ADDR *pc_max,
	 int *recursion_indicator,
	 struct result_list *list_startPC_SPoffset);

static int is_a_near_function_call(char *mnemonic)
{
  if (STREQ(mnemonic, "CALL") || STREQ(mnemonic, "CALLR")) {
	  return NEAR_CALL;
  } else {
	  return 0;
  }
}

static int is_a_far_function_call(char *mnemonic)
{
  if (STREQ(mnemonic, "CALLF")) {
	  return FAR_CALL;
  } else {
	  return 0;
  }
}

static int is_a_function_call(char *mnemonic)
{
  int function_call;
  function_call = is_a_near_function_call(mnemonic);
  if (!function_call) {
    function_call = is_a_far_function_call(mnemonic);
  }
  return function_call;
}

int is_a_function_call_stack_offset(CORE_ADDR stack_offset)
{
	return (stack_offset == 2 /* CALL or CALLR */
			|| stack_offset == 3 /* CALLF */
			);
}
				 
/* We disassemble the assembly language instructions
   from "start_pc" to "stop_pc" and compute the stack pointer offset.
   This function is used for applications compiled with the Cosmic compiler
   which doesn't generate sufficient debug information (stack offset only for C lines)
   and for applications assembled with the Crash Barrier assembler.
   What is needed is the variation of the stack pointer at each instruction address.

   We have to recognize the following instructions:

   - PUSH ...
   - PUSHW ...	(STM7P)
   - SUBW SP	(STM7P)

   - POP ...
   - POPW ... (STM7P)
   - ADDW SP  (STM7P)

   and the following instruction sequences:

   - LD A,S
     SUB A,#nn
     LD S,A

   - LD X:A,SP (STM7)
	 SUB A,#nn
	 EXG A,X
	 SBC A,#0
	 EXG A,X
	 LD SP,X:A

   - LD A,S
     ADD A,#nn
     LD S,A

   - LD X:A,SP (STM7)
	 ADD A,#nn
	 EXG A,X
	 ADC A,#0
	 EXG A,X
	 LD SP,X:A

  which modify the stack pointer.

  We also have to take into account conditional and unconditional jumps
  as well as function calls.
  We have to follow the two possible next instructions after a conditional jump.
  If we don't have a direct addressing mode, we stop disassembling
  and return. We don't want to have to read the memory or the ST7 registers
  which might have changed.

  When there is a jump, we don't go backwards or beyond the current PC.
  This strategy will prevent the debugger from entering infinite loops
  and ensure that we converge on the current PC. It seems to work well
  and to allow to compute the stack pointer offset in most cases.

  If we realize that we can't find the sequence of instructions executed
  from the start PC to the stop PC, we stop disassembling and return.

  "current_frame" is used to know if we are in the current frame. If not,
  we have to stop disassembling one instruction before the return PC.

  "list_SP_offset" is the list of the SP offsets corresponding to
  all the different instruction sequences which lead to the current PC.
  We will check that the SP offset is the same for all sequences.

  "cant_compute_stack_offset" is used to indicate that although we couldn't
  compute the offset.

  "pc_max" is used to store the address of the instruction following
  the last reached instruction of the current function. It is used
  when there are functions which have no name.

  "recursion_indicator" is used to try to detect loops.

  "list_startPC_SPoffset" is used to retrieve already computed results.
*/
//#define debug_compute_stack_pointer_offset

static void compute_stack_pointer_offset(CORE_ADDR start_pc,
					 CORE_ADDR stop_pc,
					 int current_frame,  
					 struct list_int *list_SP_offset,
					 int *cant_compute_stack_offset,
					 CORE_ADDR *pc_max,
					 int *recursion_indicator,
					 struct result_list *list_startPC_SPoffset)
{
  CORE_ADDR pc = start_pc;
  char disass_buffer[STRING_LENGTH];
  /* SP offset coming from A offset. */
  long A_offset;
  /* SP offset coming from X offset. */
  long X_offset;
  /* Have we found "LD A,S"? */
  int move_S_into_A = 0;
  /* Have we found "LD X:A,SP"? */
  int move_SP_into_XA = 0;
  /* Have we found "EXG A,X"? */
  int exchange_A_X = 0;
  /* Have we found "ADD A,#value" or "SUB A,#value"? */
  int update_A = 0;
  /* Have we found "ADC A,#value" or "SBC A,#value"? */
  /* A and X will be exchanged later. */
  int update_X = 0;
  /* Instruction counter. */
  int instruction_counter = 0;
  /* "SP_offset" is used to store the current SP offset before a branch. */
  int SP_offset = 0;
  int SP_offset_rest;

  /* Test to prevent infinite recursion. It shouldn't be necessary. */
  (*recursion_indicator)++;
#ifdef debug_compute_stack_pointer_offset
  fprintf(debug_file, "compute_stack_pointer_offset %d\n", *recursion_indicator);
  fflush(debug_file);
#endif
  /* Don't try to go beyond 256 as it will take too much time.
	It is probably an error but just return. */
  if (*recursion_indicator > 256) {
	*cant_compute_stack_offset = 1;
	return;
	/*
	error("GDB7 internal error: probable infinite recursion in compute_stack_pointer_offset.");
	*/
  }
  while (pc != stop_pc)
    {
      char *mnemonic;
      char *operand1;
      char *operand2;
      int add;
      int jrxx;
	  int instruction_length;
	  int memory_check = check_disassembly;
#ifdef debug_compute_stack_pointer_offset
      CORE_ADDR prev_pc;
      char hexa_buffer[STRING_LENGTH];
#endif

      if (pc > stop_pc) {
		*cant_compute_stack_offset = 1;
		return;
	  }
      instruction_counter++;
      if (get_result_from_list(pc, list_startPC_SPoffset, &SP_offset_rest)) {
		add_int_to_list(SP_offset + SP_offset_rest, list_SP_offset);
		return;
	  }
#ifdef debug_compute_stack_pointer_offset
	  prev_pc = pc;
	  pc += get_code_and_disassemble_instruction(pc, hexa_buffer, disass_buffer);
	  fprintf(debug_file, "0x%04x:%s %s\n", prev_pc, hexa_buffer, disass_buffer);
#else
      instruction_length = disassemble_instruction(pc, disass_buffer, memory_check);
	  if (instruction_length == -1)  {
		error("Can't disassemble at 0x%x while unwinding stack.", pc);
		*cant_compute_stack_offset = 1;
		return;
	  }
	  pc += instruction_length;
#endif
      if (pc > *pc_max) {
		*pc_max = pc;
	  }
      
      /* The PC saved in the stack is the PC after the call instruction
		and NOT the PC of the call instruction.
		If we are not in the current frame, we must stop disassembling
		just when we reach the called function.
      */
      if (pc == stop_pc && !current_frame) {
		break;
	  }

      mnemonic = strtok(disass_buffer, " \t");
      if (STREQ(mnemonic, "PUSH")) {
		SP_offset++;
      } else if (STREQ(mnemonic, "POP")) {
		SP_offset--;
      } else if (STREQ(mnemonic, "PUSHW")) { /* STM7P */
		SP_offset += 2;
      } else if (STREQ(mnemonic, "POPW")) { /* STM7P */
		SP_offset -= 2;
	  }
      /* The stack pointer is reset. In this case the absolute value of SP
		can be easily computed but not the SP offset relatively
		to the start of the function. */
      else if (STREQ(mnemonic, "RSP")) { /* ST7 or STM7 */
		*cant_compute_stack_offset = 1;
		return;
	  }
      /* Recognize the sequences which modify the stack pointer:
		ST7 core
		LD A,S
		ADD A,#10		or			SUB A,#10
		LD S,A
	 
		STM7 core
		LD X:A,SP
		ADD A,#10		or			SUB A,#10
		EXG A,X
		ADC A,#0		or			SBC A,#0
		EXG A,X
		LD SP,X:A
      */
	  else if (STREQ(mnemonic, "LD")) {
		operand1 = strtok(NULL, " \t,");
		operand2 = strtok(NULL, " \t,");
		/* LD A,S */
		if (STREQ(operand1, "A") && STREQ(operand2, "S")) {
		  move_S_into_A = instruction_counter;
		/* LD X:A,SP */
		} else if (STREQ(operand1, "X:A") && STREQ(operand2, "SP")) {
		  move_SP_into_XA = instruction_counter;
	    /* LD S,... */
		} else if (STREQ(operand1, "S")) {
	      /* LD S,A */
	      if (STREQ(operand2, "A")
			  && update_A == (instruction_counter - 1)
			  && move_S_into_A == (instruction_counter - 2)) {
			SP_offset += A_offset;
		  } else {
			warning("Can't compute SP variation after %s %s,%s.",
					mnemonic, operand1, operand2);
			*cant_compute_stack_offset = 1;
			return;
		  }
		} else if (STREQ(operand1, "SP")) {
	      if (/* This is the only possible operand. */
		      STREQ(operand2, "X:A")
		      && exchange_A_X == (instruction_counter - 1)
			  && update_X == (instruction_counter - 2)) {
		    SP_offset += (X_offset << 8) + A_offset;
		  } else {
			*cant_compute_stack_offset = 1;
		    return;
		  }
	    }
	  }
	  else if (STREQ(mnemonic, "LDW")) { /* STM7P */
		operand1 = strtok(NULL, " \t,");
		operand2 = strtok(NULL, " \t,");
		/* LDW SP,... */
		if (STREQ(operand1, "SP")) {
			/* Absolute value: can't compute stack pointer offset */
			*cant_compute_stack_offset = 1;
			return;
	    }
	  }
      else if (((add = STREQ(mnemonic, "ADD")) || STREQ(mnemonic, "SUB"))
			   && (move_S_into_A == (instruction_counter - 1)
				   || move_SP_into_XA == (instruction_counter - 1))) {
		operand1 = strtok(NULL, " \t,");
		operand2 = strtok(NULL, " \t,");
		if (STREQ(operand1, "A")) {
	      if (operand2[0] == '#') {
			/* ADD A,#... or SUB A,#... */
			if (is_valid_integer(operand2 + 1, &A_offset)) {
			  if (add) {
				A_offset = -A_offset;
			  }
		      update_A = instruction_counter;
		    }
		  }
	    }
	  }
	  else if ((add = STREQ(mnemonic, "ADDW")) || STREQ(mnemonic, "SUBW")) { /* STM7P */
		operand1 = strtok(NULL, " \t,");
		operand2 = strtok(NULL, " \t,");
		if (STREQ(operand1, "SP")) {
	      if (operand2[0] == '#') {
			/* ADDW SP,#... or SUBW SP,#... */
			/* Take care: when data are pushed to the stack, the stack pointer is decreased!
			   SUBW SP,#... is used to reserve space in the stack
			*/
			long offset;
			if (is_valid_integer(operand2 + 1, &offset)) {
			  if (add) {
				SP_offset -= offset;
			  } else {
				SP_offset += offset;
			  }
		    }
		  }
	    }
	  }
      else if (((add = STREQ(mnemonic, "ADC")) || STREQ(mnemonic, "SBC"))
			   && exchange_A_X == (instruction_counter - 1)
	           && update_A == (instruction_counter - 2)) {
		operand1 = strtok(NULL, " \t,");
		operand2 = strtok(NULL, " \t,");
		if (STREQ(operand1, "A")) {
	      if (operand2[0] == '#') {
			/* ADC A,#... or SBC A,#... */
			if (is_valid_integer(operand2 + 1, &X_offset)) {
			  if (add) {
				X_offset = -X_offset;
			  }
		      update_X = instruction_counter;
		    }
		  }
	    }
	  }
      else if (STREQ(mnemonic, "EXG")
			   && ((update_A == (instruction_counter - 1)
				    && move_SP_into_XA == (instruction_counter - 2))
				   || (update_X == (instruction_counter - 1)
					   && exchange_A_X == (instruction_counter - 2)))) {
		operand1 = strtok(NULL, " \t,");
		operand2 = strtok(NULL, " \t,");
		if (STREQ(operand1, "A") && STREQ(operand2, "X")) {
	      /* EXG A,X */
	      exchange_A_X = instruction_counter;
	    }
	  }
      /* If we reach a return instruction, we won't be able to reach the stop PC
		 since we return from the current function. */
      else if (STREQ(mnemonic, "IRET") || STREQ(mnemonic, "RET") || STREQ(mnemonic, "RETF")) {
		/* Only in this case, we don't set *cant_compute_stack_offset although we can't reach the stop PC.
		   There might be a function without a name (Cosmic compiler) after the return instruction. */
		return;
	  }
      /* We would need to disassemble the routines called by CALL, CALLF or CALLR
		to be sure that they don't modify the stack.
		We just assume that a routine doesn't modify the stack.
		However we take into account a few non standard library routines
		that alter the stack. */
      else if (is_a_function_call(mnemonic)) {
		long call_address;
	  
		operand1 = strtok(NULL, " \t,");
		/* Direct addressing mode. */
		if (is_valid_integer(operand1, &call_address)) {
	      struct minimal_symbol *func_minsym;
	      func_minsym = lookup_minimal_symbol_by_pc(call_address);
		  if (func_minsym && SYMBOL_VALUE_ADDRESS(func_minsym) == call_address) {
			int far_call = is_a_far_function_call(mnemonic);
			SP_offset += stack_pointer_offset_after_function_execution(SYMBOL_NAME(func_minsym), far_call);
		  }
	    }
	  }
      /* We need to jump to the address specified after JP, JRA or JRT. */
      else if (STREQ(mnemonic, "JP") || STREQ(mnemonic, "JRA") || STREQ(mnemonic, "JRT")
			   || STREQ(mnemonic, "JPF")) {
		long jump_address;
	  
		operand1 = strtok(NULL, " \t,");
		/* Direct addressing mode. */
		if (is_valid_integer(operand1, &jump_address)) {
	      if (/* We assume that if we have gone beyond the stop_pc, we can't reach it. */
			  jump_address <= stop_pc
			  /* We don't want to enter an infinite loop and go again
				through the same address: the best thing to do is not to go backwards. */
			  && jump_address >= pc) {
			pc = jump_address;
		  } else {
			/* Don't set *cant_compute_stack_offset; there might be other paths to the stop PC .*/
			return;
		  }
		} else {
	    /* Not a direct addressing mode. */
		  *cant_compute_stack_offset = 1;
		  return;
		}
	  }
      /* JRF means never jump. */
      else if (STREQ(mnemonic, "JRF")) {
	  }
      /* We don't know what is the next instruction
		that is executed after a conditional jump.
		We have to take into account both possibilities. */
      else if ((jrxx = STREQN(mnemonic, "JR", 2))
			   || STREQ(mnemonic, "BTJF") || STREQ(mnemonic, "BTJT")) {
		char *operand3;
		char *jump_address_string;
		long jump_address;
		int result;
		
		operand1 = strtok(NULL, " \t,");
		if (jrxx) {
		  jump_address_string = operand1;
		} else {
	      operand2 = strtok(NULL, " \t,");
	      operand3 = strtok(NULL, " \t,");
	      jump_address_string = operand3;
	    }
		if (is_valid_integer(jump_address_string, &jump_address)) {
	      if (/* We assume that if we have gone beyond the stop_pc, we can't reach it. */
			  jump_address <= stop_pc
			  /* We don't want to enter an infinite loop and go again
				through the same address: the best thing to do is not to go backwards. */
			  && jump_address > pc) {
			if (compute_stack_pointer_offset_for_all_paths(jump_address,
															stop_pc,
															current_frame,
															&SP_offset_rest,
															cant_compute_stack_offset,
															pc_max,
															recursion_indicator,
															list_startPC_SPoffset)) {
			  add_int_to_list(SP_offset + SP_offset_rest, list_SP_offset);
			}
			/* Try with the second alternative if the first one fails. */ 
		  }
		} else {
		  /* Not a direct addressing mode. */
		  *cant_compute_stack_offset = 1;
		  return;
		}
	  }
    }
	add_int_to_list(SP_offset, list_SP_offset);
}

/* Compute stack pointer offset after execution of code located between "start_pc" and "stop_pc".
   The computed offset is returned through the "offset" parameter.
   If the offset can't be computed, compute_stack_pointer_offset_for_all_paths returns 0 otherwise 1. 
   "current_frame" is equal to 1 if we are in the current frame.
   "cant_compute_stack_offset" is used to indicate that the offset couldn't be computed.
   "pc_max" is used to know if we have reached "stop_pc".
   "recursion_indicator" is used to detect loops.
   "list_startPC_SPoffset" is used to store intermediate results.
   */
static int
compute_stack_pointer_offset_for_all_paths
	(CORE_ADDR start_pc,
	 CORE_ADDR stop_pc,
	 int current_frame,
	 int *offset,
	 int *cant_compute_stack_offset,
	 CORE_ADDR *pc_max,
	 int *recursion_indicator,
	 struct result_list *list_startPC_SPoffset)
{
  struct list_int list_SP_offset;

  /* Don't compute twice the stack pointer offset for the same address range. */
  if (get_result_from_list(start_pc, list_startPC_SPoffset, offset)) {
	return 1;
  }
  *cant_compute_stack_offset = 0;
  list_SP_offset.n = 0;
  compute_stack_pointer_offset(start_pc, stop_pc, current_frame,
	                           &list_SP_offset, cant_compute_stack_offset,
							   pc_max, recursion_indicator, list_startPC_SPoffset);

  if (identical_elements_in_list(list_SP_offset)) {
	  *offset = list_SP_offset.list[0];
	  /* Store the result so as to retrieve it later if needed. */
	  add_result_to_list(start_pc, *offset, list_startPC_SPoffset);
	  free(list_SP_offset.list);
	  return 1;
  } else if (empty_list(list_SP_offset)) {
	return 0;
  } else {
	  /* We found different SP offsets! */
	  *cant_compute_stack_offset = 1;
	  free(list_SP_offset.list);
	  return 0;
  }
}

static int stack_pointer_offset(CORE_ADDR start_pc,
								CORE_ADDR stop_pc,
								int current_frame,
								int *offset)
{
  int cant_compute_stack_offset;
  CORE_ADDR pc_max = start_pc;
  int recursion_indicator = 0;
  struct result_list list_startPC_SPoffset;
  int result;
#ifdef debug_compute_stack_pointer_offset
  char *debug_file_name = "debug_compute_stack_pointer_offset.txt";
  debug_file = fopen(debug_file_name, "w");
#endif

  initialize_result_list(&list_startPC_SPoffset);
  result = compute_stack_pointer_offset_for_all_paths(start_pc, stop_pc, current_frame,
	                                                  offset, &cant_compute_stack_offset,
										              &pc_max, &recursion_indicator,
													  &list_startPC_SPoffset);

#ifdef debug_compute_stack_pointer_offset
  display_result_list(list_startPC_SPoffset);
  fclose(debug_file);
#endif
  free_result_list(list_startPC_SPoffset);

  /* Maybe special case: function not in the symbol table. */
  if (!result && !cant_compute_stack_offset) {
	  if (pc_max <= stop_pc) {
		  struct minimal_symbol *minsym;

		  minsym = lookup_minimal_symbol_by_pc(pc_max);
		  /* We should find a minimal symbol.
		     Minimal debug information is not stripped from the executable file. */
		  if (minsym) {
			  if (SYMBOL_VALUE_ADDRESS(minsym) != pc_max)
			    /* There is no minimal symbol at pc_max: it might be the start of a function
				   which has no name and which is located after the function
				   corresponding to minsym. */
				/* See c_fmul function (used in the COSMIC library routine for floating point
				   multiplication):
				   it calls sub-functions for which no symbols have been defined.
				   These sub-functions are located just after the end of the c_fmul function. */
				return stack_pointer_offset(pc_max, stop_pc, current_frame, offset);
		}
	}
  }
  return result;
}

static enum function_call_type get_function_call_type(struct frame_info *frame);

/* Stack layout */

/* FUNCTION CALL: 2-byte return address
 *
 *     0x1FF
 *
 *          |                |
 *          +----------------+
 *          | PCL            | return address
 *          +----------------+
 *          | PCH            |
 *          +----------------+
 *          | saved registers| optional
 *          +----------------+
 *          | local variables| optional
 *          +----------------+
 *          |                |
 *
 *     0x100
 */

/* FUNCTION FAR CALL:  3-byte return address
 *
 *		0x1FF
 *
 *          |                |
 *          +----------------+
 *          | PCL            | return address
 *          +----------------+
 *          | PCH            |
 *          +----------------+
 *          | PCE            |
 *          +----------------+
 *          | saved registers| optional
 *          +----------------+
 *          | local variables| optional
 *          +----------------+
 *          |                |
 *
 *     0x100
 */

 /* ST7/STM7 INTERRUPT
 *
 *     0x1FF
 *
 *          |                |
 *          +----------------+
 *          | PCL            | return address
 *          +----------------+
 *          | PCH            |
 *          +----------------+
 *          | X              |
 *          +----------------+
 *          | A              |
 *          +----------------+
 *          | CC             |
 *          +----------------+
 *          | saved registers| optional
 *          +----------------+
 *          | local variables| optional
 *          +----------------+
 *          |                |
 *
 *     0x100
 */

 /* STM7 INTERRUPT IN FAR CODE
 *
 *     0x1FF
 *
 *          |                |
 *          +----------------+
 *          | PCL            | return address
 *          +----------------+
 *          | PCH            |
 *          +----------------+
 *          | PCE            |
 *          +----------------+
 *          | X              |
 *          +----------------+
 *          | A              |
 *          +----------------+
 *          | CC             |
 *          +----------------+
 *          | saved registers| optional
 *          +----------------+
 *          | local variables| optional
 *          +----------------+
 *          |                |
 *
 *     0x100
 */

 /* STM7P INTERRUPT
 *
 *     0x1FF
 *
 *          +----------------+
 *          | PCL            | return address
 *          +----------------+
 *          | PCH            |
 *          +----------------+
 *          | PCE            |
 *          +----------------+
 *          | YL             |
 *          +----------------+
 *          | YH             |
 *          +----------------+
 *          | XL             |
 *          +----------------+
 *          | XH             |
 *          +----------------+
 *          | A              |
 *          +----------------+
 *          | CC             |
 *          +----------------+
 *          | saved registers| optional
 *          +----------------+
 *          | local variables| optional
 *          +----------------+
 *
 *     0x100
 */

 /* There is no frame pointer register, so we use SP as the frame pointer
	but the stack pointer may vary inside a function.
    FP = SP
	result is in fsr
 */

static void
do_st7_frame_find_saved_regs(struct frame_info *fi, struct frame_saved_regs *fsr)
{
  int regnum;
  enum function_call_type call_type;
  int pc_size = 2;
  CORE_ADDR sp_after_call;
  CORE_ADDR sp_after_previous_call_prologue = 0;
  CORE_ADDR sp_max;

  for (regnum = 0; regnum < NUM_REGS; regnum++) {
    fsr->regs[regnum] = 0;
  }

  call_type = get_function_call_type(fi);
  if (call_type == ERROR_CALL_TYPE) {
	return;
  }

  if (call_type == FAR_CALL) { /* ST7 or STM7 */
	pc_size++;
  }

  sp_after_call = get_sp_after_call(fi);
  if (fi->prev)
  {
	sp_after_previous_call_prologue = compute_sp_after_prologue(fi->prev);
  }
  sp_max = get_sp_max();

  if (call_type == INTERRUPT_HANDLER)
	{
	  if (mcu_core == STM7P_CORE) {
		/* FIXME: if there is a separate register file, no register is stacked! */
		/* For an interruption, besides the PC, the Y, X, A and CC registers
			are also saved onto the stack. */
		if (sp_after_previous_call_prologue) {
			fsr->regs[SP_REGNUM] = sp_after_previous_call_prologue;
		} else {
			fsr->regs[SP_REGNUM] = sp_after_call + 9;
		}
		fsr->regs[PC_REGNUM] = sp_after_call + 7;
	  } else {
		/* For an interruption, besides the PC, the X, A and CC registers
			are also saved onto the stack. */
		if (sp_after_previous_call_prologue) {
			fsr->regs[SP_REGNUM] = sp_after_previous_call_prologue;
		} else {
			fsr->regs[SP_REGNUM] = sp_after_call + 5;
			if (is_a_STM7_far_interrupt()) { /* STM7 */
				/* PCE is also pushed. */
				fsr->regs[SP_REGNUM] += 1;
			}
		}
		fsr->regs[PC_REGNUM] = sp_after_call + 4;
	  }
	}
  else if (sp_after_call != sp_max) 
	{
	  if (sp_after_previous_call_prologue) {
		fsr->regs[SP_REGNUM] = sp_after_previous_call_prologue;
	  } else {
		fsr->regs[SP_REGNUM] = sp_after_call + pc_size;
	  }
	  fsr->regs[PC_REGNUM] = sp_after_call + 1;
	}
  if (fi->prev)
  {
	CORE_ADDR sp_after_previous_call = get_sp_after_call(fi->prev);
	if (sp_after_previous_call) {
	  fsr->regs[FRAME_POINTER_REGNUM] = sp_after_previous_call;
	} else {
	  fsr->regs[FRAME_POINTER_REGNUM] = fsr->regs[SP_REGNUM];
	}
  }
}

void
st7_frame_find_saved_regs (struct frame_info *fi,
						   /* result */
						   struct frame_saved_regs *fsr) 
{
  int regnum;
  if (fi->init_saved_regs)
  {
	for (regnum = 0; regnum < NUM_REGS; regnum++)
	  fsr->regs[regnum] = fi->saved_regs[regnum];
  }
  else
  {
	do_st7_frame_find_saved_regs (fi, fsr);
	for (regnum = 0; regnum < NUM_REGS; regnum++)
	  fi->saved_regs[regnum] = fsr->regs[regnum];
	fi->init_saved_regs = 1;
  }
}

/* This function is used to find the function associated to a given frame
   for applications written in ST assembly language.

   If the symbol returned by get_frame_function is not a function but an ordinary label,
   get_real_function looks recursively for the closest label before the current label.
   As the near and far directives, which enable the ST assembler to distinguish functions
   from ordinary labels, have only been added with the STM7 assembler in version 4.30,
   get_real_function just returns the first label found if the assembler target is the ST7.
   Moreover for an ST7 application, the developer might not use "near" keywords.
*/
static int
get_real_function(struct frame_info *frame, struct symbol **function)
{
  struct symbol *func_sym = get_frame_function(frame);
  enum function_call_type call_type;
  if (func_sym) {
	  struct block *b;
	  b = SYMBOL_BLOCK_VALUE(func_sym);
	  if (!b) {
		warning("Symbol with null block value.");
		return 0;
	  }
	  call_type = BLOCK_CALL_TYPE(b);
	  /* Take into account assembly language labels which are not subroutines but not for ST7! */
	  /* Special case is the reset handler. */
	  if (call_type == NO_CALL
		  && compiler_target != ST7_CORE
		  && !is_reset_handler(func_sym))
	  {
		  CORE_ADDR func_addr, saved_frame_pc;
		  int result;
		  /* We have reached a label which is not at the start of a function.
			 Look for a symbol located before the label. */
		  func_addr = BLOCK_START(SYMBOL_BLOCK_VALUE(func_sym));
		  saved_frame_pc = frame->pc;
		  frame->pc = func_addr - 1;
		  /* Be aware that there might be no function at address - 1:
	         in the following example, there is no code at 0x10ffff!	 
			segment byte at 1000 'P'
			.start
				jpf jump_label
			segment byte at 110000 'P'
			.jump_label.l
				nop
			*/
		  result = get_real_function(frame, function);
		  frame->pc = saved_frame_pc;
		  return result;
	  } else {
		  *function = func_sym;
		  return 1;
	  }
  } else {/* No function symbol. */
	  return 0;
  }
}

static int is_global_symbol(struct symbol *sym)
{
  int i;
  struct block *b = BLOCK_SUPERBLOCK(SYMBOL_BLOCK_VALUE(sym));
  struct block *last_b;

  while (b)
	{
	  last_b = b;
	  b = BLOCK_SUPERBLOCK(b);
	}
  b = last_b;

  for (i = 0; i < b->nsyms; i++)
	{
	  if (b->sym[i] == sym)
		return 1;
	}
  return 0;
}

static enum function_call_type compute_function_call_type(struct frame_info *frame, CORE_ADDR *func_addr);

/* function_addr is not used
   it was added in order to check that the return PC read in the stack matches a call
   but the planned check doesn't work in all cases. */
static enum function_call_type
get_function_call_type(struct frame_info *frame)
{
  if (!frame->init_call_type)
	{
	  CORE_ADDR function_addr;
	  frame->call_type = compute_function_call_type(frame, &function_addr);
	  frame->function_addr = function_addr;
	  frame->init_call_type = 1;
	}
  return frame->call_type;
}

/* Compute function call type.
   It is not so easy in assembly language.
   Versions of Crash Barrier assembly tool chain before 4.30 (STM7)
   don't support "near" nor "far" keywords which allow to differentiate
   ordinary labels from functions.
   We also assume that for an ST7 application, the application developer
   might not use "near" keywords.
*/
static enum function_call_type compute_function_call_type(struct frame_info *frame, CORE_ADDR *func_addr)
{
  struct symbol *func_sym = get_frame_function(frame);
  enum function_call_type call_type = UNKNOWN_CALL_TYPE;
  if (func_sym)
  {
	  /* MSVC 6.0 must be bugged: I can't dereference func_sym when I don't declare
	     "func_name" as a local variable. Anyway I just want to know the function name. */
	  char *func_name = SYMBOL_NAME(func_sym);
	  struct block *b;
	  b = SYMBOL_BLOCK_VALUE(func_sym);
	  if (!b) {
		  warning("Symbol with null block value.");
		  return ERROR_CALL_TYPE;
	  }
	  call_type = BLOCK_CALL_TYPE(b);
	  *func_addr = BLOCK_START(SYMBOL_BLOCK_VALUE(func_sym));
	  /* Take into account assembly language labels which are not subroutines. */
	  if (call_type == NO_CALL
		  /* For ST7, there are only near calls, no worry. */
		  && mcu_core != ST7_CORE
		  && compiler_target != ST7_CORE
		  /* Special case is the reset handler. */
		  && !is_reset_handler(func_sym))
		{
		  CORE_ADDR restored_frame_pc = frame->pc;
		  /* We have reached a label which is not at the start of a function.
			 Look for a symbol located before the label. */
		  frame->pc = *func_addr - 1;
		  call_type = compute_function_call_type(frame, func_addr);
		  /* There might be no function at address - 1:
	         in the following example, there is no code at 0x10ffff!	 
			segment byte at 1000 'P'
			.start
				jpf jump_label
			segment byte at 110000 'P'
			.jump_label.l
				nop
			*/
		  frame->pc = restored_frame_pc;
		  return call_type;
		}
  } else { /* No function symbol. */
	  struct minimal_symbol *func_minsym;
	  func_minsym = lookup_minimal_symbol_by_pc(frame->pc);
	  if (func_minsym) {
		*func_addr = SYMBOL_VALUE_ADDRESS(func_minsym);
	  } else {
		*func_addr = UNKNOWN_VALUE;
		return ERROR_CALL_TYPE;
	  }
  }
  /* For the ST7 Crash Barrier assembler, we want to be compatible
	 and we must accept functions not declared as near. */
  if ((mcu_core == ST7_CORE || compiler_target == ST7_CORE)
	  && (call_type == UNKNOWN_CALL_TYPE /* ASM 4.20: no information available */
		  || call_type == NO_CALL /* ASM 4.30: "near" keyword omitted */)) {
	call_type = NEAR_CALL;
  }
  return call_type; 
}

static CORE_ADDR compute_frame_saved_pc(CORE_ADDR pc_addr, int pc_size, int check);

/*
   Given a GDB frame, determine the address of the calling function's frame.
   This will be used to create a new GDB frame struct, and then
   `INIT_EXTRA_FRAME_INFO' and `INIT_FRAME_PC' will be called for the new frame.
   Return a pointer to the calling frame.
*/
CORE_ADDR
st7_frame_chain(struct frame_info *thisframe)
{
  enum function_call_type call_type;
  int additional_stack_offset = 0;
  CORE_ADDR sp_after_call;
  int saved_pc_size = 2;

  call_type = get_function_call_type(thisframe);
  if (call_type == ERROR_CALL_TYPE || call_type == NO_CALL) {
	return 0;
  }
  if (mcu_core != ST7_CORE && call_type == UNKNOWN_CALL_TYPE && !guess_call_type) {
	return 0;
  }

  /* For an interruption, besides the PC additional registers are saved in the stack */
  if (call_type == INTERRUPT_HANDLER) {
	  if (mcu_core == STM7P_CORE) {
		/* FIXME: if there is a separate register file, no register is stacked! */
		saved_pc_size = 3;
		/* The Y, X, A and CC registers are pushed onto the stack */
		additional_stack_offset = 6;
	  } else {
		/* The X, A and CC registers are pushed onto the stack */
		additional_stack_offset = 3;
		if (is_a_STM7_far_interrupt()) { /* PCE is also pushed. */
			saved_pc_size = 3;
		}
	  }
  }
  if (call_type == FAR_CALL) {
	saved_pc_size = 3;
  }
  sp_after_call = get_sp_after_call(thisframe);
  if (sp_after_call) {
    if (mcu_core != ST7_CORE && call_type == UNKNOWN_CALL_TYPE) {
	  /* Try to guess call type. */
      int check = 1;
      CORE_ADDR saved_pc_address = sp_after_call + 1;
	  /* Don't try to find the caller of the reset routine. */
	  if (saved_pc_address > get_sp_max()) {
		return 0;
	  }
      /* Try first near call (call or callr) before far call (callf). */
      if (compute_frame_saved_pc(saved_pc_address, saved_pc_size, check) == UNKNOWN_VALUE) {
        saved_pc_size = 3;
        if (compute_frame_saved_pc(saved_pc_address, saved_pc_size, check) == UNKNOWN_VALUE) {
          return 0;
        } else {
          thisframe->call_type = FAR_CALL;
        }
      } else {
        thisframe->call_type = NEAR_CALL;
      }
    }
	return sp_after_call + additional_stack_offset + saved_pc_size;
  } else {
	return 0;
  }
}

/* "preceding_instruction_fits_size" disassembles the instruction located at "pc" - "pc_offset"
   preceding the instruction located at "pc".
   The disassembled instruction is stored in "disass_buffer".
   "preceding_instruction_fits_size" returns 1 only if the disassembled instruction just ends
   at "pc".
   "preceding_instruction_fits_size" is called by "check_previous_instruction_is_a_call".
 */

static int preceding_instruction_fits_size(CORE_ADDR pc, int pc_offset, char *disass_buffer)
{
  int previous_address = pc - pc_offset;
  int instruction_length;
  /* Always check that memory is readable while guessing call type. */
  int memory_check = 1;
  /* Don't forget limit case. */
  if (previous_address < 0) {
	return 0;
  }
  instruction_length = disassemble_instruction(previous_address, disass_buffer, memory_check);
  if (instruction_length == -1) {
	  return 0;
  }
  return instruction_length == pc_offset;
}

static unsigned char last_PCE; /* STM7 */

/* Compute the function return address:
	the function return address is saved into the stack */
/* If there is no debug information,
	should we look for a "callf" instruction even if a "call" instruction
	has already been found (saved_pc != UNKNOWN_VALUE) ???
	If we read at a random location, we might have side effects.
	It is possible although highly unlikely that at the same time
	if we look for a 2-byte return PC in the stack,
	we find a "call" in the program memory just before the return PC
	and if we look for a 3-byte return PC in the stack,
	we also find a "callf" in the program memory just before the return PC.
	If this case occurred, a test for the called function address should be performed!
	For the moment, we only test for a far call if the test for a near call has failed. */
/* If st7_frame_chain has been called, then call_type has already been patched.
	Patch it for other cases:
	FRAME_SAVED_PC is also used in generic_frame_chain_valid,
	SAVED_PC_AFTER_CALL is also used in wait_for_inferior (step_over_function). */

CORE_ADDR
st7_frame_saved_pc (struct frame_info *frame)
{
  enum function_call_type call_type;
  int additional_stack_offset = 0;
  CORE_ADDR saved_pc;
  int saved_pc_size = 2;
  CORE_ADDR saved_pc_addr;
  int check;

  call_type = get_function_call_type(frame);
  if (call_type == ERROR_CALL_TYPE) {
    return UNKNOWN_VALUE;
  }
  if (mcu_core != ST7_CORE && call_type == UNKNOWN_CALL_TYPE && !guess_call_type) {
	return UNKNOWN_VALUE;
  }

  /* For an interruption, besides the PC additional registers are saved in the stack */
  if (call_type == INTERRUPT_HANDLER) {
	  if (mcu_core == STM7P_CORE) {
		/* FIXME: if there is a separate register file, no register is stacked! */
		saved_pc_size = 3;
		/* The Y, X, A and CC registers are pushed onto the stack */
		additional_stack_offset = 6;
	  } else {
		/* The X, A and CC registers are pushed onto the stack */
		additional_stack_offset = 3;
		if (is_a_STM7_far_interrupt()) { /* PCE is also pushed. */
			saved_pc_size = 3;
		}
	  }
  }

  if (call_type == FAR_CALL) {
	saved_pc_size = 3;
  }

  /* Initialize last PCE if needed. */
  if (get_current_frame() == frame) {
	last_PCE = stop_pc >> 16;
  }

  saved_pc_addr = get_sp_after_call(frame) + additional_stack_offset + 1;
  check = (call_type != INTERRUPT_HANDLER);
  /* Don't try to find the caller of the reset routine. */
  if (saved_pc_addr > get_sp_max()) {
	return UNKNOWN_VALUE;
  }
  /* If st7_frame_chain has been called, then call_type may have already been patched
     so we don't know here if the call type was read from the debug information
     or if it was guessed by st7_frame_chain. */
  saved_pc = compute_frame_saved_pc(saved_pc_addr, saved_pc_size, check);

  /* If st7_frame_chain has been called, then call_type has already been patched. */
  if (mcu_core != ST7_CORE && call_type == UNKNOWN_CALL_TYPE) {
	if (saved_pc == UNKNOWN_VALUE) {
		/* Try for a far call. */
		saved_pc_size = 3;
		saved_pc = compute_frame_saved_pc(saved_pc_addr, saved_pc_size, check);
		if (saved_pc != UNKNOWN_VALUE) {
			frame->call_type = FAR_CALL;
		}
	} else {
		frame->call_type = NEAR_CALL;
	}
  }
  return saved_pc;
}

ULONGEST read_memory_nobpt_unsigned_integer (CORE_ADDR memaddr, int len)
{
  char buf[sizeof (ULONGEST)];

  read_memory_nobpt (memaddr, buf, len);
  return extract_unsigned_integer (buf, len);
}

static int check_previous_instruction_is_a_call(CORE_ADDR saved_pc, int pc_size);

static CORE_ADDR compute_frame_saved_pc(CORE_ADDR pc_addr, int pc_size, int check)
{
  CORE_ADDR saved_pc;

  /* Check that all PC bytes are located within the stack limits. */
  if (pc_addr < get_sp_min() || (pc_addr + pc_size - 1) > get_sp_max()) {
	return UNKNOWN_VALUE;
  }
  /* Read the saved PC in the stack. */
  saved_pc = read_memory_nobpt_unsigned_integer(pc_addr, pc_size);
  if (pc_size == 2) { /* We need the 24-bit address. */
	saved_pc += last_PCE << 16;
  }
  /* If we are in an interrupt handler, there is no way to check if the saved PC is correct. */
  if (!check) {
	return saved_pc;
  }
  /* Check if there is a call instruction just before the saved PC. */
  if (check_previous_instruction_is_a_call(saved_pc, pc_size)) {
	  if (pc_size == 3) { /* PC Extended has been saved in the stack. */
		last_PCE = saved_pc >> 16;
	  }
	  return saved_pc;
  } else {
	return UNKNOWN_VALUE;
  }
}

/* Test is the instruction before the return PC is a call instruction. */
/* "check_previous_instruction_is_a_call" is called by "compute_frame_saved_pc". */
/* If it is a direct call, we could check that the called address is equal to the start of the current function.
   But it is difficult to find the real start of the called function:
	- the current function might have been entered by a jump instruction (optimization of sequence call ret),
	- maybe there is no name for that function if it has been generated  by the compiler.
   Anyway if we have an indirect addressing mode, we would have to read the contents of the memory or registers
   which could have been modified after the call.
   So it is not possible to be sure that the candidate return pc is really the return address of a call. */
/* More details about function address check for a direct call:
	Normally it should be a call to the current function and one could compare
	the call operand with the start of the current function but there are exceptions;
	1)
	If it is a call but not to the current function, it may still be OK
	if we have jumped to the current function instead of having called it.
	Examine the following code for the HIWARE _FADD library routine:
	_FADD:
		CALL _UNP_FR0
		CALL _UNP_FR1
		CALL _FADD_01
		JP _P_FR0
	If we are in _P_FR0, the return address corresponds to the return of the call to _FADD.
	2)
	But we have to take into account the case of a function without a name.
	Such a function can be generated by the HIWARE compiler.
	We should find a minimal symbol as minimal debug information is not stripped from the executable file.
	If the function called by the preceding instruction has no name,
	we could try to check that the current PC is greater than the address of the routine
	but we can't assume that the current PC must be greater than the start address of the called routine.
	We have to take into account jumps to instructions before the entry point.
	Look at the following functions called by the COSMIC c_fmul library routine:
					0xeb18:	LD 0x85,A
					0xeb1a:	LD A,X
					0xeb1b:	ADC A,0x84
					0xeb1d:	LD 0x84,A
					0xeb1f:	JRNC 0xeb23
				1:	0xeb21:	INC Y
				2:	0xeb23:	RET

					0xeb24:	LD 0x81,A
					0xeb26:	LD A,X
					0xeb27:	ADC A,0x85
					0xeb29:	LD 0x85,A
					0xeb2b:	JRNC 0xeb23		--> 2
					0xeb2d:	INC 0x84
					0xeb2f:	JREQ 0xeb21		--> 1
					0xeb31:	RET						*/
/* An ST7 call instruction consists of 1, 2, 3 or 4 bytes.
   We try instructions obtained by disassembling at pc - 1, pc - 2, pc - 3
   and pc - 4.
   Note that there might be several of these instructions that end at pc.
   For example, if we have the following byte sequence: <CD AD 2C> at pc - 3.
   AD2C --> CALLR 0x2c (CALLR short),
   CDAD2C --> CALL 0xad2c (CALL long). */

static int check_previous_instruction_is_a_call(CORE_ADDR saved_pc, int pc_size)
{
  char disass_buffer[STRING_LENGTH];
  char *mnemonic;

  if (pc_size == 3)
	{
	  /* Check for CALLF instruction. */
	  if (preceding_instruction_fits_size(saved_pc, 4, disass_buffer))
		{
		  mnemonic = strtok(disass_buffer, " \t");
		  if (is_a_far_function_call(mnemonic)) {
			return 1;
		  }
		}
	}
  else
	{
	  /* Try first CALL <long address> (3 bytes) then CALLR <short address> (2 bytes). */
	  int pc_offset_array[] = {3, 2, 4, 1};
	  int i;

	  /* Go through all the possible preceding instructions
		 but try first with the most probable instructions to reduce checking time. */
	  for (i = 0; i < 4; i++)
		{
		  if (preceding_instruction_fits_size(saved_pc, pc_offset_array[i], disass_buffer))
			{
			  mnemonic = strtok(disass_buffer, " \t");
			  if (is_a_near_function_call(mnemonic)) {
				return 1;
			  }
			}
		}
	}
  return 0;
}

CORE_ADDR
st7_frame_args_address (struct frame_info *frame)
{
  /* To make LOC_ARG work for Cosmic dwarf implementation,
     st7_frame_args_address must return 0, but the 0 value has
	 a special meaning for gdb, that there is no frame arg !
	 We remove this special meaning everywhere it is tested */
  return 0;
}

CORE_ADDR MCU_reset_address;
void reinit_reset_address (void)
{
  if (mcu_core == STM7P_CORE) {
	//MCU_reset_address = read_memory_nobpt_unsigned_integer(MCU_reset_vector, 4) & 0xffffff;
	MCU_reset_address = MCU_reset_vector;
  } else {
	MCU_reset_address = read_memory_nobpt_unsigned_integer(MCU_reset_vector, 2);
  }
}

static int is_reset_handler (struct symbol *func)
{
  return BLOCK_START(SYMBOL_BLOCK_VALUE(func)) == MCU_reset_address;
}

int
st7_frame_chain_valid (CORE_ADDR addr, struct frame_info *thisframe)
{
  /* 
	 PUSH : (SP--) <= dst
	 POP: dst <= (++SP)
	 Suppose that SP_MIN = 0x100 and SP_MAX = 0x1ff.
	 We test that 0x100 <= SP <= 0x1ff
	 but when there is an overflow (SP < 0x100), the ST7 automatically sets
	 the stack pointer to 0x1ff so that no write access is ever made
	 at address 0x99.
	 Take care: if SP_MIN is equal to 0, (SP_MIN - 1) is not equal to - 1
	 in unsigned long arithmetic.
  */
  return (get_sp_min() <= addr && addr <= get_sp_max());
}

/* Skip prologues. Returns either PC itself if the prologue can't be found;
   otherwise returns the address that follows the prologue. */

CORE_ADDR
st7_skip_prologue (CORE_ADDR start_pc)
{
/*
 * (pm) original version
 * (rj) adapted from the ST9
 * Gives address of the first instruction after the prologue. 
 * It is given by the end address of the first line of the function.
 * NOTE1: that includes all the following cases:
 *  ---------------
 * int testint2(int x)
 * { x += 1; 
 * }
 *  ---------------
 * int testint5(int x){ x += 1;  x += 1;return x;} 
 *  ---------------
 * int testint5(int x){ x += 1;  x += 1;return x;} int testint6(int x){ testint4(x);}
 *  -------------
 * NOTE2: that includes the case of a function with no prologue (optimised code); 
 * see c code below:
 *  --------------- c code ----------------
 * void main(void)
 * {
 *
 *
 *   f_in_text();
 *}
 */
  struct symtab_and_line sal;
  struct symtab *s;
  int i;

  /* Find out the symbol table of the source which contains "start_pc". 
     Note: we use "find_pc_line" to be sure to get the right symtab, in case of #include code
	 in the middle of a subroutine: "find_pc_line" looks at all the symtabs that share the
	 block_vector containing "start_pc". */
  sal = find_pc_line (start_pc, 0);

  /* FIXME! Without debug information, the prologue is not skipped. */
  if ( !sal.symtab )
    return start_pc;   

  s = sal.symtab;
  
  /* Case of assembly code:
     in assembly code, the notion of prologue doesn't exist. An assembly procedure has no 
     automatic generated prologue: a prologue must be user written. */
  if (s && s->language != language_c)
    return start_pc;

  /* There is debug information, but no line information.
     This case cannot occur, because we have used "find_pc_line" to get the symbol table. */
  if (!s->linetable)
    return start_pc; 

  /* Find out the limit for the extent of the prologue; 
     that means find out the second line directive from the beginning of the function, 
     which gives the end address of the prologue. */
  for (i=0; i < s->linetable->nitems; i++)
    {
      if (s->linetable->item[i].pc == start_pc)
		 // First line directive of current function
		{
		  if (i+1 < s->linetable->nitems)
			{ 
			  if (s->linetable->item[i+1].pc == start_pc)
				/* Empty prologue: second item has the same address as the first one. */
				return start_pc;
	          else  
				{
				  /* Non empty prologue:
				   * address of first instruction after prologue is end address
				   * of prologue line (because prologue is seen as a line). */
				  if (sal.end)
					return sal.end;
				  else
					{
					  warning ("Debug information is not valid");
					  return start_pc; 	      
					}		
				}			      
			}
		  else // i+1 >=  s->linetable->nitems
			{
			  warning ("Debug information is not valid");
			  return start_pc; 	      
			}
		}
	}

  return start_pc;
}

/* Raisonance compiler pseudo registers */
enum Raisonance_register {
	BX,
	CX
};

/* Get the address of a structure returned by a function
	compiled with the Raisonance compiler:
	the address is stored in pseudo register reg */
static CORE_ADDR Raisonance_get_returned_structure_address_in_pseudo_reg(enum Raisonance_register reg)
{
	unsigned char ret_addr_high, ret_addr_low;
	CORE_ADDR ret_addr;
	char *string_arg = (reg == BX) ? "&'?BH'" : "&'?CH'";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, &ret_addr_high, 1);
	string_arg = (reg == BX) ? "&'?BL'" : "&'?CL'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, &ret_addr_low, 1);
	ret_addr = (ret_addr_high << 8) | ret_addr_low;
	return ret_addr;
}

/* Get the address of a structure returned by a function
	compiled with the Raisonance compiler */
static CORE_ADDR Raisonance_get_returned_structure_address()
{
	if (compiler_target == ST7_CORE || compiler_target == STM7_CORE) {
		enum Raisonance_register pseudo_reg;
		if (compiler_target == ST7_CORE) {
			pseudo_reg = BX;	/* ST7 */
		} else {
			pseudo_reg = CX;	/* STM7 */
		}
		return Raisonance_get_returned_structure_address_in_pseudo_reg(pseudo_reg);
	} else if (compiler_target == STM7P_CORE) {
		return read_register(X_REGNUM);
	} else {
		error("Invalid core in Raisonance_get_returned_structure_address.");
		return -1;
	}
}

/* 3-byte data are returned in BL CH CL by Raisonance compiler */
static void Raisonance_extract_3_byte_returned_value(char *valbuf)
{
	char *string_arg = "&'?BL'";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf, 1);
	string_arg = "&'?CH'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf+1, 1);
	string_arg = "&'?CL'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf+2, 1);
}

/* 4-byte data are returned in BH BL CH CL by Raisonance compiler */
static void Raisonance_extract_4_byte_returned_value(char *valbuf)
{
	char *string_arg = "&'?BH'";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf, 1);
	string_arg = "&'?BL'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf+1, 1);
	string_arg = "&'?CH'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf+2, 1);
	string_arg = "&'?CL'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf+3, 1);
}

/* Extract structure returned by ST7/STM7 Cosmic compiler */
static Cosmic_extract_ST7_returned_struct(unsigned int length, char *valbuf)
{
	char *string_arg = "c_x";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf, length);
}

/* 3-byte data are returned in c_x (2 bytes) and X (1 byte) by ST7/STM7 Cosmic compiler */
static void Cosmic_extract_ST7_3_byte_returned_value(char *valbuf)
{
	char *string_arg = "&c_x";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf, 2);
	valbuf[2] = (char)read_register(X_REGNUM); /* LSB in X */
}

/* 4-byte data are returned in c_lreg by Cosmic compiler */
static void Cosmic_extract_4_byte_returned_value(char *valbuf)
{
	char *string_arg = "&c_lreg";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	read_memory_nobpt(sym_addr, valbuf, 4);
}

/* Extract the return value of a function for the ST7/STM7 core */
static void st7_extract_return_value(struct type *type, char *regbuf, char *valbuf)
{
  if (!symfile_objfile) {
	error("No symbol table is loaded; can't compute return value.");
  }
  /* With the Raisonance compiler, returned unions and structures are always stored into the stack. */
  /* Returned tables larger than 4 bytes are also stored into the stack. */
  if (is_raisonance_compiler_used(symfile_objfile)) {
	  if (TYPE_CODE (type) == TYPE_CODE_STRUCT
		  || TYPE_CODE (type) == TYPE_CODE_UNION
		  || TYPE_LENGTH(type) > 4) {
		CORE_ADDR sp = Raisonance_get_returned_structure_address();
		read_memory_nobpt(sp, valbuf, TYPE_LENGTH(type));
		return;
	  }
  }
  /* With the Cosmic compiler, returned unions and structures are stored into the stack if their size is larger than 2. */
  if (is_cosmic_compiler_used(symfile_objfile)) {
	  if (((TYPE_CODE (type) == TYPE_CODE_STRUCT || TYPE_CODE (type) == TYPE_CODE_UNION) && TYPE_LENGTH(type) > 2)
		  || TYPE_LENGTH(type) > 4) {
		Cosmic_extract_ST7_returned_struct(TYPE_LENGTH(type), valbuf);
		return;
	  }
  }
  switch (TYPE_LENGTH(type))
	{
    case 1:
		valbuf[0] = (char)read_register(A_REGNUM);
		break;
	case 2:
		valbuf[0] = (char)read_register(X_REGNUM);
		valbuf[1] = (char)read_register(A_REGNUM);
      break;
	case 3:
		/* Metrowerks compiler */
		if (is_metrowerks_compiler_used(symfile_objfile)) {
			valbuf[0] = (char)read_register(Y_REGNUM);
			valbuf[1] = (char)read_register(X_REGNUM);
			valbuf[2] = (char)read_register(A_REGNUM);
		/* Raisonance compiler */
		} else if (is_raisonance_compiler_used(symfile_objfile)) {
			Raisonance_extract_3_byte_returned_value(valbuf);
		/* Cosmic compiler */
		} else if (is_cosmic_compiler_used(symfile_objfile)) {
			Cosmic_extract_ST7_3_byte_returned_value(valbuf);
		} else {
			error("GDB7 internal error: %s format not supported; can't compute return value.",
				  bfd_get_target(symfile_objfile->obfd));
		}
		break;
	case 4:
		/* Metrowerks compiler */
		if (is_metrowerks_compiler_used(symfile_objfile)) {
			char *string_arg = "&_R_Z";
			CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
			read_memory_nobpt(sym_addr, valbuf, 1);
			valbuf[1] = (char)read_register(Y_REGNUM);
			valbuf[2] = (char)read_register(X_REGNUM);
			valbuf[3] = (char)read_register(A_REGNUM); 
		/* Raisonance compiler */
		} else if (is_raisonance_compiler_used(symfile_objfile)) {
			Raisonance_extract_4_byte_returned_value(valbuf);
		/* Cosmic compiler */
		} else if (is_cosmic_compiler_used(symfile_objfile)) {
			Cosmic_extract_4_byte_returned_value(valbuf);
		} else {
			error("GDB7 internal error: %s format not supported; can't compute return value.",
				  bfd_get_target(symfile_objfile->obfd));
		}
		break;
    default:
		/* Metrowerks compiler */
		if (is_metrowerks_compiler_used(symfile_objfile)) {
			char *string_arg = "_SEX";
			CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
			read_memory_nobpt(sym_addr, valbuf, TYPE_LENGTH(type));
		/* Cosmic compiler and Raisonance compiler already handled at the start of the function. */
		} else {
			error("GDB7 internal error: %s format not supported; can't compute return value.",
				  bfd_get_target(symfile_objfile->obfd));
		}
		break;
	}
}

/* Extract structure returned by STM8 Cosmic compiler */
static Cosmic_extract_STM8_returned_struct(unsigned int length, char *valbuf)
{
	CORE_ADDR sp = read_sp();
	CORE_ADDR retval_addr;
	unsigned char *uc_valbuf = valbuf;
	read_memory_nobpt(sp+1, uc_valbuf, 2);
	retval_addr = (uc_valbuf[0] << 8) | uc_valbuf[1];
	read_memory_nobpt(retval_addr, valbuf, length);
}

/* 3-byte data are returned in c_x (1 byte) and X (2 bytes) by STM8 Cosmic compiler */
static void Cosmic_extract_STM8_3_byte_returned_value(char *valbuf)
{
	char *string_arg = "&c_x";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	short x_value = (short)read_register(X_REGNUM);
	read_memory_nobpt(sym_addr, valbuf, 1);
	valbuf[1] = (char)(x_value >> 8);
	valbuf[2] = (char)x_value;
}

/* Extract the return value of a function for the STM7P core */
static void stm7p_extract_return_value(struct type *type, char *regbuf, char *valbuf)
{
  if (!symfile_objfile) {
	error("No symbol table is loaded; can't compute return value.");
  }
  if (bfd_get_flavour(symfile_objfile->obfd) != bfd_target_elf_flavour) {
	error("GDB7 internal error: %s format not supported; can't compute return value.",
		  bfd_get_target(symfile_objfile->obfd));
  }
  /* With the Raisonance compiler, returned unions and structures are always stored into the stack. */
  /* Returned tables larger than 4 bytes are also stored into the stack. */
  if (is_raisonance_compiler_used(symfile_objfile)) {
	if (TYPE_CODE (type) == TYPE_CODE_STRUCT
		|| TYPE_CODE (type) == TYPE_CODE_UNION
		|| TYPE_LENGTH(type) > 4) {
		CORE_ADDR sp = Raisonance_get_returned_structure_address();
		read_memory_nobpt(sp, valbuf, TYPE_LENGTH(type));
		return;
	}
  }
  /* With the Cosmic compiler, returned unions and structures are stored into the stack if their size is larger than 2. */
  if (is_cosmic_compiler_used(symfile_objfile)) {
	  if (((TYPE_CODE (type) == TYPE_CODE_STRUCT || TYPE_CODE (type) == TYPE_CODE_UNION) && TYPE_LENGTH(type) > 2)
		  || TYPE_LENGTH(type) > 4) {
		Cosmic_extract_STM8_returned_struct(TYPE_LENGTH(type), valbuf);
		return;
	  }
  }
  switch (TYPE_LENGTH(type))
	{
    case 1:
		valbuf[0] = (char)read_register(A_REGNUM);
		break;
	case 2:
		{
			short x_value = (short)read_register(X_REGNUM);
			valbuf[0] = (char)(x_value >> 8);
			valbuf[1] = (char)x_value;
		}
		break;
	case 3:
		if (is_cosmic_compiler_used(symfile_objfile)) {
			Cosmic_extract_STM8_3_byte_returned_value(valbuf);
		} else if (is_raisonance_compiler_used(symfile_objfile)) {
			Raisonance_extract_3_byte_returned_value(valbuf);
		}
		break;
	case 4:
		if (is_cosmic_compiler_used(symfile_objfile)) {
			Cosmic_extract_4_byte_returned_value(valbuf);
		} else if (is_raisonance_compiler_used(symfile_objfile)) {
			Raisonance_extract_4_byte_returned_value(valbuf);
		}
		break;
    default:
		error("GDB7 internal error: %s format not supported; can't compute return value.",
			  bfd_get_target(symfile_objfile->obfd));
		break;
	}
}

/* Extract the return value of a function
   EXTRACT_RETURN_VALUE is used by the "finish" command
*/
void
extract_return_value(struct type *type, char *regbuf, char *valbuf)
{
	if (mcu_core == STM7P_CORE) {
		stm7p_extract_return_value(type, regbuf, valbuf);
	} else {
		st7_extract_return_value(type, regbuf, valbuf);
	}
}

/* 4-byte returned data are stored in c_lreg by Cosmic compiler */
static void	Cosmic_store_4_bytes_return_value(char *valbuf)
{
	char *string_arg = "&c_lreg";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	write_memory(sym_addr, valbuf, 4);
}

/* 4-byte returned data are stored in BH BL CH CL by Raisonance compiler */
static void Raisonance_store_4_bytes_return_value(char *valbuf)
{
	char *string_arg = "&'?BH'";
	CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
	write_memory(sym_addr, valbuf, 1);
	string_arg = "&'?BL'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	write_memory(sym_addr, valbuf+1, 1);
	string_arg = "&'?CH'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	write_memory(sym_addr, valbuf+2, 1);
	string_arg = "&'?CL'";
	sym_addr = parse_and_eval_address_1(&string_arg);
	write_memory(sym_addr, valbuf+3, 1);
}

/*
   Store the return value of a function in the same way as the ST7/STM7 compiler does
*/
void st7_store_return_value(struct type *type, char *valbuf)
{
  switch (TYPE_LENGTH(type))
    {
	case 1:
      write_register(A_REGNUM, (LONGEST)&(valbuf[0]));
      break;
    case 2:
      write_register(X_REGNUM, (LONGEST)&(valbuf[0]));
      write_register(A_REGNUM, (LONGEST)&(valbuf[1]));
      break;
	case 4:
	  if (symfile_objfile)
		{
		  if (is_metrowerks_compiler_used(symfile_objfile))
			{
			   char *string_arg = "&_R_Z";
			   CORE_ADDR sym_addr = parse_and_eval_address_1(&string_arg);
			   write_memory(sym_addr, valbuf, 1);
			   write_register(Y_REGNUM, (LONGEST)&(valbuf[1]));
			   write_register(X_REGNUM, (LONGEST)&(valbuf[2]));
			   write_register(A_REGNUM, (LONGEST)&(valbuf[3]));
			}
		  else if (is_raisonance_compiler_used(symfile_objfile))
			{
			   Raisonance_store_4_bytes_return_value(valbuf);
			}
		  else if (is_cosmic_compiler_used(symfile_objfile))
			{
			   Cosmic_store_4_bytes_return_value(valbuf);
			}
		  else
			error("GDB7 internal error: %s format not supported; can't store return value.",
				  bfd_get_target(symfile_objfile->obfd));
		}
	  else
		error("No symbol table is loaded; can't store return value.");
      break;
    default:
      error("GDB7 internal error: %s type not supported; can't store return value.",
			TYPE_NAME(type));
    }
}

/*
   Store the return value of a function in the same way as the STM7P compiler does
*/
void stm7p_store_return_value(struct type *type, char *valbuf)
{
  switch (TYPE_LENGTH(type))
    {
	case 1:
      write_register(A_REGNUM, (LONGEST)&(valbuf[0]));
      break;
    case 2:
      write_register(X_REGNUM, (LONGEST)&(valbuf[0]));
      break;
	case 4:
	  if (symfile_objfile)
		{
		  if (is_cosmic_compiler_used(symfile_objfile))
			{
			   Cosmic_store_4_bytes_return_value(valbuf);
			}
		  else if (is_raisonance_compiler_used(symfile_objfile))
			{
			   Raisonance_store_4_bytes_return_value(valbuf);
			}
		  else
			error("GDB7 internal error: %s format not supported; can't store return value.",
				  bfd_get_target(symfile_objfile->obfd));
		}
	  else
		error("No symbol table is loaded; can't store return value.");
      break;
    default:
      error("GDB7 internal error: %s type not supported; can't store return value.",
			TYPE_NAME(type));
    }
}

/*
   Store the return value of a function in the same way as the compiler does
   STORE_RETURN_VALUE is used by the "return" command return_command
   but POP_FRAME should be defined to something else than nothing!
*/
void store_return_value(struct type *type, char *valbuf)
{
	if (mcu_core == STM7P_CORE) {
		stm7p_store_return_value(type, valbuf);
	} else {
		st7_store_return_value(type, valbuf);
	}
}

/* Set software breakpoint instruction */
void set_break_insn(unsigned char *break_insn)
{
	if (mcu_core == STM7P_CORE) {
		/* Special opcode reserved for software breakpoint */
		*break_insn = STM7P_BREAKPOINT;
	} else {
		/* TRAP instruction */
		*break_insn = ST7_BREAKPOINT;
	}
}

/* Is it just the name of a type? */
/* When debugging information is described in the DWARF2 debugging format,
   base types such as "unsigned char" or "int" give birth to pseudo global symbols
   at address 0. */
int is_type_name(struct symbol *sym)
{
	if (sym && SYMBOL_VALUE_ADDRESS(sym) == 0) {
		char *symbol_name = SYMBOL_NAME(sym);
		char *type_name = TYPE_NAME(SYMBOL_TYPE(sym));
		if (symbol_name && type_name && strcmp(symbol_name, type_name) == 0) {
			return 1;
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

/* Addresses can be compared only if they have the same address class.
   Check that we have a static address for local symbols.
   Of course, global and static symbols have a static address class.
   We have to filter out local symbols stored in stack memory
   and keep only local symbols stored in shared memory.
   We could recognize local symbols in stack memory for STM7 and STM8 processors
   which have an SP indexed addressing mode
   but we would need more detailed debug information as the stack pointer may vary within a C line.
*/
int is_static_symbol(struct symbol *sym)
{
	return SYMBOL_CLASS(sym) == LOC_STATIC;
}

/*--------------------------------------------------------------------------+
|                                                                            |
|    Look up a variable in specified block and its super blocks              |
|                                                                            |
+--------------------------------------------------------------------------*/
struct symbol *
lookup_var_block (CORE_ADDR address, struct block *block)
{
  struct symbol *sym;
  while (block != NULL)
  {
      QUIT;
      {
        int bot, top;

        top = BLOCK_NSYMS (block);
        bot = 0;

        while (bot < top)
        {
          QUIT;
          sym = BLOCK_SYM (block, bot);
          if (SYMBOL_NAMESPACE (sym) == VAR_NAMESPACE
			  && !is_type_name(sym)
			  && is_static_symbol(sym)
              && (address >= SYMBOL_VALUE (sym)
                  && address < (SYMBOL_VALUE (sym) + TYPE_LENGTH(SYMBOL_TYPE(sym)))))
            return sym;
          bot++;
        }
      }
      block = BLOCK_SUPERBLOCK (block);
  }
  return (struct symbol *) 0;
}

/*--------------------------------------------------------------------------+
|                                                                            |
|    Look up a variable used in instruction at pc                            |
|                                                                            |
+--------------------------------------------------------------------------*/
struct symbol *
lookup_var_pc (CORE_ADDR address, CORE_ADDR pc)
{
	struct block *block;
	struct symbol *sym;

	block = block_for_pc (pc);
	if (!block) {
		return (struct symbol *) 0;
	}
	/* Search for local, static or global variable in specified block and its super blocks. */
	/* Search local variables only for shared memory models. */
	sym = lookup_var_block (address, block);
	return sym;
}

/*--------------------------------------------------------------------------+
|                                                                            |
|    Find a symbol name at a given address.                                  |
|                                                                            |
+--------------------------------------------------------------------------*/
int st_print_symbol (char *buffer,
                     /* Maximum buffer length. */
                     int length,
                     bfd_vma vma,
                     unsigned long pc)
{
  struct symbol *sym;
  struct minimal_symbol *msymbol;
  CORE_ADDR addrval = vma;
  CORE_ADDR pcval = pc;

  /* First look for a local, static or global variable in the current file. */
  sym = lookup_var_pc (addrval, pcval);
  if (sym != NULL)
  {
    strncpy(buffer, SYMBOL_NAME(sym), length - 1);
    if (addrval != SYMBOL_VALUE (sym))
	{
      char tmp_name[10];
	  int current_length;

      sprintf(tmp_name,"+%d", addrval - SYMBOL_VALUE(sym));
	  current_length = strlen(buffer);
      strncat(buffer, tmp_name, (length - 1) - current_length);
    }
    return 1;
  }

  /* Now look for any symbol matching this address. */
  /* Function names are found here in the minimal symbol list
	 but we could use the BLOCK_FUNCTION symbol. */
  msymbol = lookup_minimal_symbol_by_address (addrval);
  if (msymbol && addrval == SYMBOL_VALUE_ADDRESS(msymbol))
  {
    strncpy(buffer, SYMBOL_NAME(msymbol), length - 1);
    return 1; 
  }
  return 0;
}

/*--------------------------------------------------------------------------+
|                                                                            |
|    New multipurpose functions for gdb7                                     |
|                                                                            |
|    is_valid_number, get_symbol_address, get_symbol_size                    |
|                                                                            |
|    Useful functions for symbol to address translation                      |
|                                                                            |
+--------------------------------------------------------------------------*/

int
is_valid_number (const char *str, unsigned long *number)
{
  unsigned long	val;
  char		*end = NULL;

  val = strtol (str, &end, 0);
  if (number)
    *number = val;
  return (end && end != str && (*end == '\0' || isspace (*end)));
}

int
get_symbol_address (const char *sym_name, CORE_ADDR *addr_p)
{
  if (is_valid_number (sym_name, addr_p))
      return 1;
  else
    {
      struct symbol*	sym;
      CORE_ADDR		addr;
  
      sym = lookup_symbol (sym_name, NULL, VAR_NAMESPACE, 0,
			   (struct symtab**) 0);

      if (sym && SYMBOL_CLASS (sym) == LOC_BLOCK)
	{
	  /* SYM_NAME is the a function name. */
	  addr = (CORE_ADDR) BLOCK_START (SYMBOL_BLOCK_VALUE (sym));
	}
      else
	{
	  struct minimal_symbol*	msym = 
	    lookup_minimal_symbol (sym_name, NULL, (struct objfile *) NULL);

	  if (msym)
	    addr = (CORE_ADDR) SYMBOL_VALUE_ADDRESS (msym);
	  else
	    if (sym && SYMBOL_CLASS (sym) == LOC_STATIC)
		  addr = (CORE_ADDR) SYMBOL_VALUE_ADDRESS (sym);
		else
	      return 0;
	}

      *addr_p = addr;
      return 1;
    }
}

int
get_symbol_size (const char *sym_name, CORE_ADDR *addr_p)
{
  if (is_valid_number (sym_name, addr_p))
      return 1;
  else
    {
      struct symbol*	sym;
      unsigned long		size;
  
      sym = lookup_symbol (sym_name, NULL, VAR_NAMESPACE, 0,
			   (struct symtab**) 0);

      if (sym && SYMBOL_CLASS (sym) == LOC_BLOCK)
	{
	  /* SYM_NAME is the a function name. */
	  size = BLOCK_END (SYMBOL_BLOCK_VALUE (sym)) -
	           BLOCK_START (SYMBOL_BLOCK_VALUE (sym));
	}
      else
      if (sym)
	     if (SYMBOL_TYPE (sym)->code == TYPE_CODE_ARRAY)
           size = SCALE_TYPE_LENGTH (SYMBOL_TYPE (sym)) *
                  SCALE_TYPE_LENGTH (SYMBOL_TYPE (sym)->target_type);
		 else
           size = SCALE_TYPE_LENGTH (SYMBOL_TYPE (sym));
	  else
	{
	  struct minimal_symbol*	msym = 
	    lookup_minimal_symbol (sym_name, NULL, (struct objfile *) NULL);

	  if (msym)
	    size = 1; /* ### Limitation: we cannot find the size of a
	                   minimal symbol so we assume a size of 1.
	                   The user should be warned here. */
	  else
	    return 0;
	}

      *addr_p = size;
      return 1;
    }
}

/*--------------------------------------------------------------------------+
|                                                                            |
|    New exit function for gdb7                                              |
|                                                                            |
|    tm-st7.h defines exit as gdb7_exit                                      |
|                                                                            |
|    This function should do some cleanup before really exiting.             |
|                                                                            |
+--------------------------------------------------------------------------*/
#undef exit                /* we want to call the real exit here ! */
void
gdb7_exit(int exit_code)
{
  if (current_target.to_close)
	target_close (-1);
  exit (exit_code);
}

#define SKIP_BLANK(p)	{while (*(p) == ' ') (p)++;}
#define SKIP_LETTERS(p)	{while (*(p) && ! isspace (*(p))) (p)++;}

#define SKIP_TO_END_OF_ARG(from,end)   				\
 {								\
    for ((end) = (from);					\
	isalnum (*(end)) || *(end) == '_' || *(end) == ':';	\
	(end)++);						\
  }

/*--------------------------------------------------------------------------+
|                                                                            |
|    On line assembler command                                               |
|                                                                            |
|                                                                            |
+--------------------------------------------------------------------------*/
int (*stasm) PARAMS ((bfd_vma *, char *, char **));
int st7asm(bfd_vma *, char *, char **);

/* ST7 on-line assembler */
void
st7_asmbl (char *args, int from_tty)
{
  static CORE_ADDR last_asmbl_addr = 0;
  unsigned char *arg_pt, *pt;
  CORE_ADDR from, current;
  char asmline[256];
  struct disassemble_info info;
  char *errmsg;
  
  if (args == 0)
    error ("asmbl requires at least 2 argument");

  strncpy (asmline, args, 256);
  arg_pt = asmline;
  SKIP_BLANK (arg_pt)
  pt = arg_pt;
  SKIP_LETTERS (pt);

  if (!strncmp (arg_pt, "+", 1))
    from = last_asmbl_addr;
  else
  {
    char save = *pt;
	*pt = 0; /* isolate address */
    from = parse_and_eval_address (arg_pt);
    *pt = save;
  }
  current = from;

  SKIP_BLANK (pt);


  if (-1 == (*stasm) (&current, pt, &errmsg))
	{
      printf_filtered ("assembling error\n");
	  error (errmsg);
    }
  else
  {
    (*tm_print_insn) (from, &tm_print_insn_info);
    last_asmbl_addr = (CORE_ADDR) current;
    printf_filtered ("\n0x%0X\n", current);
  }
}

int is_an_interrupt_handler(CORE_ADDR pc)
{
  struct block *bl;
  enum function_call_type call_type;

  bl = block_for_pc(pc);
  if (bl == NULL)
	/* No application loaded or no debug information. */
	return 0;
  call_type = BLOCK_CALL_TYPE(bl);
  return (call_type == INTERRUPT_HANDLER);
}

int is_an_interrupt_handler_stack_offset(CORE_ADDR stack_offset)
{
	return (stack_offset == 5 /* ST7/STM7 */
			|| stack_offset == 6 /* STM7 */
			|| stack_offset == 9 /* STM7P */
			);
}

int is_a_STM7_far_interrupt(void)
{
	/* IEA: Interrupt Extended Address (CCR.6)
		ST7: IEA=1
		STM7: IEA=0 if PCE is pushed on the stack. */
	unsigned char ccr = (unsigned char)read_register(CC_REGNUM);
	return (!(ccr & (1 << CCR_IEA)));
}

/* Called by proceed. */
void get_instruction_mnemonic(CORE_ADDR stop_pc, char *mnemonic, char *first_operand)
{
  char disass_buffer[STRING_LENGTH];
  int instruction_length;
  char *instr_mnemonic;
  char *instr_first_operand;
  int memory_check = check_disassembly;

  instruction_length = disassemble_instruction(stop_pc, disass_buffer, memory_check);
  if (instruction_length == -1) {
	  error("Can't disassemble at 0x%x while resuming execution.", stop_pc);
  }
  instr_mnemonic = strtok(disass_buffer, " \t");
  if (instr_mnemonic) {
	strcpy(mnemonic, instr_mnemonic);
	instr_first_operand = strtok(NULL, " \t");
	if (instr_first_operand) {
		strcpy(first_operand, instr_first_operand);
	} else {
		strcpy(first_operand, "");
	}
  } else {
	strcpy(mnemonic, "");
	strcpy(first_operand, "");
  }
}

/* Called by proceed and wait_for_inferior. */
int is_a_call_instruction(CORE_ADDR stop_pc, CORE_ADDR *next_pc)
{
  char disass_buffer[STRING_LENGTH];
  int instruction_length;
  char *mnemonic;
  int memory_check = check_disassembly;

  instruction_length = disassemble_instruction(stop_pc, disass_buffer, memory_check);
  if (instruction_length == -1) {
	  error("Can't disassemble at 0x%x while resuming execution.", stop_pc);
	  return 0;
  }
  *next_pc = stop_pc + instruction_length;
  mnemonic = strtok(disass_buffer, " \t");
  return is_a_function_call(mnemonic);
}

#define INSTR_NUMBER_MAX 1000
/*
	There should be a return instruction at the end of the function
	but make it sure that we are never going to be stuck in the while loop:
	don't iterate more than 1000 times.
	If there is a read memory error, don't call memory_error.
*/
int detect_user_function(CORE_ADDR non_user_func_addr, enum function_call_type call_type, CORE_ADDR *user_func_addr)
{
	CORE_ADDR memaddr = non_user_func_addr;
	char *myaddr;
	int size_to_read = MAXILEN;
	int status;
	unsigned char disass_instr_buffer[STRING_LENGTH];	/* disassembled instruction */
	char instr_buffer[MAXILEN];
	int length;
	int function_end = 0;
	char ret_instr[10];
	int instr_counter = 0;

	strcpy(ret_instr, "");
	if (call_type == NEAR_CALL) {
		strcpy(ret_instr, "RET");
	} else if (call_type == FAR_CALL) {
		strcpy(ret_instr, "RETF");
	}

	myaddr = instr_buffer;
	while (!function_end && instr_counter < INSTR_NUMBER_MAX) {
		status = target_read_memory (memaddr, myaddr, size_to_read);
		if (status != 0) {
#if 1
			printf_filtered ("Error accessing memory at [0x%x-0x%x].\n",
								memaddr, memaddr + size_to_read - 1);
#else
			memory_error (status, memaddr);
#endif
			return 0;
		}
		length = st7_disass(memaddr, instr_buffer, disass_instr_buffer, 0);
		if (strstr(disass_instr_buffer, "CALL")) {
			char call_instr[20];
			CORE_ADDR call_addr;
			if (sscanf(disass_instr_buffer, "%s 0x%lx", call_instr, &call_addr) == 2) {
				char *func_name;
				CORE_ADDR func_start, func_end;
				find_pc_partial_function (call_addr, &func_name, &func_start, &func_end);
				if (call_addr == func_start) {
					*user_func_addr = call_addr;
					return 1;
				}
			}
		} else if (strcmp(disass_instr_buffer, ret_instr) == 0) {
			function_end = 1;
		}
		memaddr += length;
		instr_counter++;
	}
	return 0;
}

CORE_ADDR
st7_stopped_data_address (void)
{
  if (stop_reason_code == di_stopped_by_data_breakpoint)
	return stopped_data_address;
  else
    return 0;
}

void st7_target_error(void)
{
  warning("Your application has been compiled for ST7 \
and you are connected to an %s debug target.", get_mcu_core_name());
}

void stm7_target_error(void)
{
  warning("Your application has been compiled for STM7 \
and you are connected to an %s debug target.", get_mcu_core_name());
}

void stm7p_target_error(void)
{
  warning("Your application has been compiled for STM8 \
and you are connected to an %s debug target.", get_mcu_core_name());
}

int pathname_eq_no_case(const char *s1, const char *s2)
{
  const char *p1, *p2;

  for (p1 = s1, p2 = s2; *p1 && *p2; p1++, p2++)
	{
	  if (tolower(*p1) != tolower(*p2))
		{
		  /* / and \ are equivalent. */
		  if ((*p1 == '/' && *p2 == '\\')
			  || (*p1 == '\\' && *p2 == '/'))
			continue;
		  break;
		}
	}
  return (*p1 == '\0' && *p2 == '\0');
}

/* Get processor time expressed in clock ticks. */

clock_t get_clock_ticks_of_processor_time()
{
  return clock();
}


/* Compute the duration in seconds from two times expressed in clock ticks. */

double diff_time_clock_ticks(clock_t ct1, clock_t ct0)
{
  return ((double)(ct1 - ct0)) / CLOCKS_PER_SEC;
}

int display_pointer_qualifier = 1;

#include <ctype.h>
#include "st7-utils.h"

static void
display_pointer_qualifier_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;

  if (strcmp(target_shortname, "gdi") != 0)
    error("display_pointer_qualifier not available for current target: %s.", target_shortname);

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);

  if (arg_number > 1)
    error("display_pointer_qualifier can't have more than one argument.");

  if (arg_number == 0)
	printf_filtered("display_pointer_qualifier %s\n",
	                display_pointer_qualifier ? "yes" : "no");
  else
    {
      szarg = next_arg (&arg_list); /* get next argument */
      if (strcmp(szarg, "no") == 0)
		display_pointer_qualifier = 0;
	  else if (strcmp(szarg, "yes") == 0)
		display_pointer_qualifier = 1;
      else
		error("display_pointer_qualifier: unknown option \"%s\".", szarg);
    }
  do_cleanups(old_cleanup_chain);
}

#include "elf-bfd.h"

static void
print_section_table (bfd *abfd, asection *asect, PTR ignore)
{
  flagword flags;

  flags = bfd_get_section_flags (abfd, asect);

  /* FIXME-32x64: Need print_address_numeric with field width.  */
  printf_filtered ("    %s",
		   local_hex_string_custom
		     ((unsigned long) bfd_section_vma (abfd, asect), "08l"));
  printf_filtered ("->%s",
		   local_hex_string_custom
		     ((unsigned long) (bfd_section_vma (abfd, asect)
				       + bfd_section_size (abfd, asect)),
		      "08l"));
  printf_filtered (" at %s",
		   local_hex_string_custom
		     ((unsigned long) asect->filepos, "08l"));
  printf_filtered (": %s", bfd_section_name (abfd, asect));

  if (flags & SEC_ALLOC)
    printf_filtered (" ALLOC");
  if (flags & SEC_LOAD)
    printf_filtered (" LOAD");
  if (flags & SEC_RELOC)
    printf_filtered (" RELOC");
  if (flags & SEC_READONLY)
    printf_filtered (" READONLY");
  if (flags & SEC_CODE)
    printf_filtered (" CODE");
  if (flags & SEC_DATA)
    printf_filtered (" DATA");
  if (flags & SEC_ROM)
    printf_filtered (" ROM");
  if (flags & SEC_CONSTRUCTOR)
    printf_filtered (" CONSTRUCTOR");
  if (flags & SEC_HAS_CONTENTS)
    printf_filtered (" HAS_CONTENTS");
  if (flags & SEC_NEVER_LOAD)
    printf_filtered (" NEVER_LOAD");
  if (flags & SEC_COFF_SHARED_LIBRARY)
    printf_filtered (" COFF_SHARED_LIBRARY");
  if (flags & SEC_IS_COMMON)
    printf_filtered (" IS_COMMON");

  printf_filtered ("\n");
}

static void
sections_info (char *arg, int from_tty)
{
  asection *s;
  bfd *exec_bfd;
  int hiware_elf = 0;

  if (arg != NULL) {
	error("Too many arguments.\n");
  }

  if (symfile_objfile) {
	exec_bfd = symfile_objfile->obfd;
  } else {
	error ("No symbol file loaded.");
  }

  if (bfd_get_flavour(exec_bfd) == bfd_target_hicross_flavour) {
      error ("Hiware format not supported.");
  }

  if (bfd_get_flavour(exec_bfd) == bfd_target_elf_flavour) {
	for (s = exec_bfd->sections; s; s = s->next) {
		Elf_Internal_Shdr *hdr = &elf_section_data (s)->this_hdr;

		if (hdr->sh_type == SHT_NOTE) {
			bfd_size_type size = bfd_get_section_size_before_reloc (s);
			char *buffer;
			struct cleanup *old_chain;
			buffer = xmalloc (size);
			old_chain = make_cleanup (free, buffer);
			bfd_get_section_contents (exec_bfd, s, buffer, 0, size);
			if (strcmp(buffer+12, "HIWARE AG") == 0) {
				hiware_elf = 1;
			}
			do_cleanups (old_chain);
		}
	}
  }

  for (s = exec_bfd->sections; s; s = s->next) {
	if (!(s->flags & SEC_DEBUGGING)) {
      bfd_size_type size = bfd_get_section_size_before_reloc (s);
      if (size > 0) {
		  flagword flags = SEC_NO_FLAGS;
		  if (bfd_get_flavour(exec_bfd) == bfd_target_elf_flavour) {
			Elf_Internal_Shdr *hdr = &elf_section_data (s)->this_hdr;
			int flags_patched = 0;

			if (hiware_elf == 1) {
				/* Metrowerks compiler
				.startData and .copy sections are flagged as code instead of data */
				if (strcmp(bfd_get_section_name (exec_bfd, s), ".startData") == 0
					|| strcmp(bfd_get_section_name (exec_bfd, s), ".copy") == 0) {
					flags |= SEC_DATA;
					flags_patched = 1;
				}
			} else {
				/* Cosmic compiler
				.const and .init sections are flagged as code instead of data */
				if (strcmp(bfd_get_section_name (exec_bfd, s), ".const") == 0) {
					flags |= SEC_DATA;
					flags_patched = 1;
				}
				/* Take more care with .init section
					as a user defined section can be named ".init". */
				if (strcmp(bfd_get_section_name (exec_bfd, s), ".init") == 0) {
					/* Cosmic .init section starts with __idesc__ */
					struct minimal_symbol *msymbol = lookup_minimal_symbol ("__idesc__", NULL, NULL);
					if (msymbol != NULL && SYMBOL_VALUE_ADDRESS(msymbol) == s->lma) {
						flags |= SEC_DATA;
						flags_patched = 1;
					}
				}
			}
			if (flags_patched != 1) {
				if ((hdr->sh_flags & SHF_EXECINSTR) != 0) {
					flags |= SEC_CODE;
				} else if ((hdr->sh_flags & SHF_ALLOC) != 0) {
					flags |= SEC_DATA;
				}
			}
		  } else {
			flags = s->flags;
		  }
		  if ((flags & SEC_CODE) != 0 || (flags & SEC_DATA) != 0) {
			printf_filtered ("section %s, lma ", bfd_get_section_name (exec_bfd, s));
			print_address_numeric (s->lma, 1, gdb_stdout);
			printf_filtered (" size 0x%lx", (unsigned long) size);
			if ((flags & SEC_CODE) != 0) {
				printf_filtered (" code");
			}
			if ((flags & SEC_DATA) != 0) {
				printf_filtered (" data");
			}
			printf_filtered ("\n");
		  }
	  }
	}
  }
}

static void
range_info (char *arg, int from_tty)
{
  CORE_ADDR start_pc, end_pc;
  char *name;
  CORE_ADDR pc;
  asection *section;

  if (!arg) {
	error ("No argument.\n");
  }
  if (strchr (arg, ' ') != NULL) {
	error("Too many arguments.\n");
  }

  pc = parse_and_eval_address (arg);
  if (find_pc_partial_function (pc, &name, &start_pc, &end_pc) == 0)
	error ("No function contains specified address.\n");
  if (overlay_debugging)
	{
	  section = find_pc_overlay (pc);
	  if (pc_in_unmapped_range (pc, section))
	    {
	      /* find_pc_partial_function will have returned start_pc and end_pc
		 relative to the symbolic (mapped) address range.  Need to
		 translate them back to the unmapped range where PC is.  */
	      start_pc  = overlay_unmapped_address (start_pc, section);
	      end_pc = overlay_unmapped_address (end_pc, section);
	    }
	}
  start_pc += FUNCTION_START_OFFSET;
  printf_filtered ("Function %s", name);
  wrap_here ("  ");
  printf_filtered (" starts at address ");
  print_address (start_pc, gdb_stdout);
  wrap_here ("  ");
  printf_filtered (" and ends at ");
  print_address (end_pc, gdb_stdout);
  printf_filtered (".\n");
}

static void
size_info (char *arg, int from_tty)
{
  CORE_ADDR start_pc, end_pc;
  CORE_ADDR size;
  char *name;
  CORE_ADDR pc;
  asection *section;

  if (!arg) {
	error ("No argument.\n");
  }
  if (strchr (arg, ' ') != NULL) {
	error("Too many arguments.\n");
  }

  pc = parse_and_eval_address (arg);
  if (find_pc_partial_function (pc, &name, &start_pc, &end_pc) == 0)
	error ("No function contains specified address.\n");
  if (overlay_debugging)
	{
	  section = find_pc_overlay (pc);
	  if (pc_in_unmapped_range (pc, section))
	    {
	      /* find_pc_partial_function will have returned start_pc and end_pc
		 relative to the symbolic (mapped) address range.  Need to
		 translate them back to the unmapped range where PC is.  */
	      start_pc  = overlay_unmapped_address (start_pc, section);
	      end_pc = overlay_unmapped_address (end_pc, section);
	    }
	}
  start_pc += FUNCTION_START_OFFSET;
  size = end_pc - start_pc;
  printf_filtered ("Function %s is %d byte long.\n", name, size);
}

/* Get instruction mnemonic from opcode. */
static void
get_instruction_mnemonic_command(char *args, int from_tty)
{
  int arg_number;
  struct argstruct arg_list;
  struct cleanup *old_cleanup_chain;
  unsigned char *szarg;
  unsigned long opcode;
  char *string_end = NULL;
  char instruction_mnemonic[10];

  if (strcmp(target_shortname, "gdi") != 0)
    error("get_instruction_mnemonic not available for current target: %s.", target_shortname);

  arg_number = parse_argument(args, &arg_list, isspace);
  old_cleanup_chain = make_cleanup((void (*) (void *))free, arg_list.arglist);

  if (arg_number != 1)
    error("get_instruction_mnemonic has one argument.");

  szarg = next_arg (&arg_list); /* get next argument */
  opcode = strtoul(szarg, &string_end, 0);
  if (*string_end || opcode > 0xffff)
    error("get_instruction_mnemonic: bad argument \"%s\".", szarg);
  if (get_instr_mnemonic((unsigned short)opcode, instruction_mnemonic))
    printf_filtered ("%s\n", instruction_mnemonic);
  else
    error("unknown opcode \"%s\".", szarg);
  do_cleanups(old_cleanup_chain);
}

void
_initialize_st7_tdep ()
{
  tm_print_insn = print_insn_st7;
  stasm = st7asm;

  /* create new command class st7 */  
  add_cmd ("st7", class_st7, NO_FUNCTION, "Special ST7 commands.", &cmdlist);

 /*
  *  on line assembler
  */
   
  add_com ("asmbl", class_st7,
	   (void (*) (char *, int))st7_asmbl,
	   "assemble one instruction on-line\n\
Usage:\n\
asmbl (<from>|+)  <mnemonic> {<operands>}\n\
Basic parameters :\n\
assemble one instruction on-line\n\
<from> specifies the start address of the instruction to assemble or\n\
+ indicates to use the address following the last instruction assembled.\n\
<mnemonic> specifies the mnemonic of the ST7 instruction.\n\
<operands>  specifies the operands of the ST7 instruction. The 2nd and next\n\
operands must start with a comma separator. An operand address may be a symbol.\n\
The mnemonics and operands are those of the ST7 Programming Manual, as they\n\
appear in the disassembling commands.\n\
The ouput of this command is the disassembling result of this command,\n\
followed by the address of the following instruction in the next line.");

    add_com ("display_pointer_qualifier", class_st7,
	   (void (*) (char *, int))display_pointer_qualifier_command,
	   "enable or disable display of pointer qualifiers\n\
Usage:\n\
display_pointer_qualifier {|no|yes}\n\
: display pointer qualifier display mode: yes or no\n\
no: don't display pointer qualifiers\n\
yes: display pointer qualifiers.");

	add_info ("sections", sections_info,
				"Display information on sections.");

	add_info ("range", range_info,
				"Display the address range of a function.");

	add_info ("size", size_info,
				"Display the size of a function.");

    add_com ("get_instruction_mnemonic", class_st7,
	   (void (*) (char *, int))get_instruction_mnemonic_command,
	   "get instruction mnemonic\n\
Usage:\n\
get_instruction_mnemonic <opcode>.");
}
