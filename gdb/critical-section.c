/*	Copyright (C) 2009 STMicroelectronics	*/

/* Add a file for critical section functions in order to avoid a definition conflict
   between bfd.h and wtypes.h for boolean. */

/* This file is part of GDB.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

#ifdef EXIT_CALLBACK

#include <wtypes.h>
#include <winbase.h>
#include "critical-section.h"
#include "defs.h"

CRITICAL_SECTION GlobalCriticalSection;
static unsigned long enter_critical_section_counter = 0;
static unsigned long leave_critical_section_counter = 0;

void InitGlobalCriticalSection(void)
{
   InitializeCriticalSection(&GlobalCriticalSection);
}

void EnterGlobalCriticalSection(void)
{
	EnterCriticalSection(&GlobalCriticalSection);
}

void LeaveGlobalCriticalSection(void)
{
	LeaveCriticalSection(&GlobalCriticalSection);
}

void EnterCommandLoopGlobalCriticalSection(void)
{
	enter_critical_section_counter++;
	if (gdi_spy && gdi_spy_stream)
	{
		fprintf(gdi_spy_stream, "gdb_command_loop:EnterGlobalCriticalSection %u\n", enter_critical_section_counter);
		fflush(gdi_spy_stream);
	}
	EnterGlobalCriticalSection();
}

void LeaveCommandLoopGlobalCriticalSection(void)
{
	leave_critical_section_counter++;
	if (gdi_spy && gdi_spy_stream) {
		fprintf(gdi_spy_stream, "gdb_command_loop:LeaveGlobalCriticalSection %u\n", leave_critical_section_counter);
		fflush(gdi_spy_stream);
	}
	LeaveGlobalCriticalSection();
}

#endif
