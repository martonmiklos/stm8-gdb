/*	Copyright (C) 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

#ifndef __ST7_INCLUDE__
#define __ST7_INCLUDE__

typedef enum core_type_enum {
  NO_CORE,
  ST7_CORE,
  STM7_CORE,
  STM7P_CORE,
  STM7PV1_CORE,
  STM7PV2_CORE
} core_type;

extern core_type mcu_core; /* ST7, STM7 or STM7P */
extern core_type mcu_core_variant; /* ST7, STM7, STM7PV1 or STM7PV2 */
extern core_type compiler_target; /* ST7, STM7 or STM7P */

#endif

