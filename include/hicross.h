/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

#ifndef HICROSS_H
#define HICROSS_H

/* HI-CROSS Standard "HI-CROSS Object File Format"  Revision 2.8.2, HIWARE */

/* should be included after "bfd.h" */

#define HICROSSTAG 254

#define ST7_PROCESSOR_FAMILY 10
#define MC68HC05_PROCESSOR_FAMILY 8

#define FILEID_OBJECT   128
#define FILEID_ABSOLUTE 129
#define FILEID_LIBRARY  130

#define LABELTAG         'L'
#define DIRECTORYTAG     'D'
#define OBJLISTTAG       'O'
#define UNITLISTTAG      'U'
#define FIXUPTAG         'F'
#define REFERENCEPARTTAG 'R'
#define SYMBOLTABLETAG   'S'
#define STRUCTUNIONTAG   'X'
#define ENUMERATIONTAG   'E'
#define SEGMENTLISTTAG   'L'
#define EXTENSIONTAG     'X'
#define ABSFILETAG       'A'
#define ROMLIBTAG        'R'
#define VARIABLETAG 'V'
#define STRINGTAG 'C'
#define CONSTTAG 'K'
#define PROCEDURETAG 'P'
#define PROGRAMTAG '0'

typedef struct {
  unsigned  char FileId;
  unsigned short ProgVersion;
  unsigned short FormatVers;
  unsigned short Flag;
  unsigned short ProcFamily;
  unsigned short ProcType;
  char * Unit;
  char * User;
  char * ProgramDate;
  char * Time;
  char * Copyright;
} hicross_header_type ;

typedef struct {
  char Target;
  char MemModel;
  bfd_vma CodeBeg;
  bfd_vma CodeEnd;
  bfd_vma DataBeg;
  bfd_vma DataEnd;
  unsigned long NBofModules;
  unsigned long TotalNBofObjects;
} hicross_moduledir_type ;

typedef struct hicross_module {
  char *ModuleName;
  char Language;
  unsigned long NBofObjects;
  unsigned long NBofProcs;
  unsigned long NBofVars;
  unsigned long objlist_offset;
  bfd_vma LowAddr;  /* NO LONGER USED */
  bfd_vma HighAddr; /* NO LONGER USED */
} hicross_module_type;

typedef struct hicross_directory {
  unsigned long NBofEnumerations;
  unsigned long NBofStructUnions;
  char Model;
  char ProgrammingLanguage;
  char Processor;
  unsigned long NBofUnits;
  unsigned long NBofSegments;
  unsigned long NBofObjects;
} hicross_directory_type;

typedef struct hicross_source_position {
  unsigned long Unit;
  unsigned long SourceStart;
  unsigned long SourceEnd;
  unsigned long NBofSourceCodePairs;
} hicross_source_position_type;

typedef struct hicross_proc_source_code_pair {
  long DeltaSourcePos;
  long DeltaCodePos;
} hicross_proc_source_code_pair_type;

typedef struct hicross_proc_PC_SP_pair {
  unsigned long PC;
  unsigned long SP;
} hicross_proc_PC_SP_pair_type;

typedef struct hicross_proc_PC_SP_list {
  unsigned long NBofPC_SP_pairs;
  hicross_proc_PC_SP_pair_type *PC_SP_pairs;
} hicross_proc_PC_SP_list_type;

typedef struct hicross_proc_locals {
  unsigned long first;
  unsigned long last;
} hicross_proc_locals_type;

typedef struct hicross_object_summary {
  char *name;
  char kind;
  unsigned long FilePosition;
} hicross_object_summary_type;

struct hicross_position {
  unsigned long Size;
  bfd_vma Address;
  unsigned long PosInFile;
};

struct hicross_variable {
  unsigned long Size;
  bfd_vma Address;
  unsigned long NBofInits;
  struct hicross_position *pos;
};

struct hicross_string {
  struct hicross_position Position;
};

struct hicross_proc {
  struct hicross_position Position;
  bfd_vma FrameOffset;
};

struct hicross_label {
  bfd_vma Address;
};

struct hicross_const {
  unsigned long Value;
};

struct hicross_kind_object {
  char kind;
  union {
    struct hicross_variable v;
    struct hicross_string s;
    struct hicross_proc p;
    struct hicross_label l;
    struct hicross_const k;
  } u;
};

struct hicross_typedef {
  char *name;
};

struct hicross_enum {
  unsigned long Size;
  unsigned long Index;
};

struct hicross_set {
  unsigned long Size;
};

struct hicross_subrange {
  struct hicross_type_component *type;
  unsigned long Low;
  unsigned long High;
};

struct hicross_function {
  struct hicross_type_component *params;
  struct hicross_type_component *Return;
};

struct hicross_array {
  unsigned long ElementNB;
  unsigned long RangeBeg;
  unsigned long SizeOfArray;
};

struct hicross_struct_or_union {
  unsigned long Index;
};
  
typedef struct hicross_type_component {
  char kind;
  struct hicross_type_component *next;
  union {
    struct hicross_typedef def;
    struct hicross_enum enu;
    struct hicross_set set;
    struct hicross_subrange sub;
    struct hicross_function  fun;
    struct hicross_array arr;
    struct hicross_struct_or_union su;
  } u;
} hicross_type_component_type; 

typedef struct hicross_object {
  unsigned long ObjNB;
  boolean exported;
  char * ObjName;
  unsigned long objtype_offset;
  struct hicross_kind_object k;
} hicross_object_type;

typedef struct hicross_unit {
  char * UnitName;
  unsigned long UnitKey;
} hicross_unit_type;

typedef struct hicross_name_value_pair {
  char * Name;
  unsigned long Value;
} hicross_name_value_pair_type;

typedef struct hicross_enumeration {
  unsigned long NBofPairs;
} hicross_enumeration_type;

typedef struct hicross_struct_union {
  unsigned long TotalSize;
  unsigned long NBofObjects;
} hicross_struct_union_type;

typedef struct hicross_field {
  char * ObjectName;
  boolean bitfield;
  unsigned long Offset;
  unsigned long Size;
  unsigned long BitFieldWidth;
  unsigned long LeasSignificantBit;
  struct hicross_type_component *ObjectType;
} hicross_field_type;      

/********************** SWAPPING **********************/

/* The hicross code needs to be able to swap debugging
   information in and out in the specific format.
   This structure provides the information
   needed to do this.  */

struct hicross_debug_swap
{
  /* Function to swap in modules. */
  void (*swap_module_in)
    PARAMS ((bfd *, unsigned long *, hicross_module_type *));

  /* Function to swap in objects. */
  void (*swap_object_in)
    PARAMS ((bfd *, unsigned long *, hicross_object_type *));

  /* Function to swap in compilation units. */
  void (*swap_unit_in)
    PARAMS ((bfd *, unsigned long *, hicross_unit_type *));

  /* Function to swap in source information for a procedure. */
  boolean (*swap_proc_source_position_in)
    PARAMS ((bfd *, unsigned long *, hicross_source_position_type *));

  /* Function to swap in a source code pair. */
  void (*swap_proc_source_code_pair_in)
    PARAMS ((bfd *, unsigned long *, hicross_proc_source_code_pair_type *));

  /* Function to swap object summary. */
  void (*swap_object_summary_in)
    PARAMS ((bfd *, unsigned long *, hicross_object_summary_type *));

  /* Function to swap in function local objects. */
  void (*swap_proc_locals_in)
    PARAMS ((bfd *, unsigned long *, hicross_proc_locals_type *));

  /* Function to swap in (SP, PC) list. */
  void (*swap_proc_SP_PC_list_in)
	PARAMS ((bfd *, unsigned long *, hicross_proc_PC_SP_list_type *));

  /* Function to swap type component. */
  void (*swap_objtype_in)
    PARAMS ((bfd *, unsigned long *, hicross_type_component_type **));

  /* Function to swap in enums. */
  void (*swap_enum_in)
    PARAMS ((bfd *, unsigned long *, hicross_enumeration_type *));

  /* Function to swap in (name, value) pairs. */
  void (*swap_name_value_pair_in)
    PARAMS ((bfd *, unsigned long *, hicross_name_value_pair_type *));

  /* Function to swap in structs and unions. */
  void (*swap_struct_union_in)
    PARAMS ((bfd *, unsigned long *, hicross_struct_union_type *));
  
  /* Function to swap in fields. */
  void (*swap_field_in)
    PARAMS ((bfd *, unsigned long *, hicross_field_type *));
};

/********************** SYMBOLS **********************/

/* For efficiency, gdb deals directly with the unswapped symbolic
   information (that way it only takes the time to swap information
   that it really needs to read).
   This structure holds
   pointers to the (mostly) unswapped symbolic information.  */

struct hicross_debug_info
{
  /* Pointers to the unswapped symbolic information. */
  unsigned char *line;
};

#endif /* ! defined (HICROSS_H) */
