/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/*
 *******************************************************************************
 * ST7 Disassembler
 *******************************************************************************
 */

#include "ins7.h"

#include "dis-asm.h"
#include "bfd.h"

static unsigned long internal_instr_addr;

char **mcucore_tokens;
struct inst7 *mcucore_instr;

/* Select instruction table in function of MCU core */
void select_instruction_table(core_type mcu_core_variant)
{
	switch(mcu_core_variant) {
	case ST7_CORE:
		mcucore_tokens = st7_tokens;
		mcucore_instr = st7_instr;
		break;
	case STM7_CORE:
		mcucore_tokens = stm7_tokens;
		mcucore_instr = stm7_instr;
		break;
	case STM7PV1_CORE:
		mcucore_tokens = stm7pv1_tokens;
		mcucore_instr = stm7pv1_instr;
		break;
	case STM7PV2_CORE:
		mcucore_tokens = stm7pv2_tokens;
		mcucore_instr = stm7pv2_instr;
		break;
	}
}

static
#ifdef TARGET_STM7
int
#else
uchar *
#endif
numval_disass(uchar type, uchar *value_ptr, char *disass_value_string, int length, int symbolic_info);

enum disass_mode_flag {
	DISASS_SOURCE 		= (1 << 0), /* make source lines appear */
	DISASS_DUMP			= (1 << 1), /* make hexa dump appear */
	DISASS_HEXA			= (1 << 2), /* make instruction dump appear */ 
	DISASS_SYMBOL		= (1 << 3), /* make symbols appear in instruction dump */
	DISASS_BREAKPOINT	= (1 << 4), /* make software breakpoints appear */
	DISASS_SYNCHRO		= (1 << 8)	/* only for disassembling */
};

extern int st_print_symbol (char *, int, bfd_vma, unsigned long);

static int print_symbol (int symbolic_info, char *buf, int length, bfd_vma vma, unsigned long pc)
{
  if (symbolic_info)
    return st_print_symbol (buf, length, vma, pc);
  else
    return 0;
}

/*
	Get instruction mnemonic from operation code
	instruction mnemonic is copied to 'mnemonic'
	returns 1 if 'opcode' is an existing operation code
	returns 0 otherwise
*/
int get_instr_mnemonic(unsigned short opcode, char *mnemonic)
{
	struct inst7 *instr_desc;	/* instruction description pointer */

	for (instr_desc = mcucore_instr; instr_desc->codop != DESFIN; instr_desc++) { 
		if (instr_desc->codop == opcode) {
			strcpy(mnemonic, mcucore_tokens[instr_desc->mnemonic]);
			return 1;
		}
	}
	return 0;
}

/****************************/
/*   DESASSEMBLEUR DU ST7   */
/****************************/
/*
	instr_addr: instruction address
	instr_buffer: pointeur debut de l'instruction a desassembler
	disass_instr_buffer: pointeur debut chaine instruction desassemblee
	symbolic_info: symbolic information
*/
int st7_disass
(unsigned long instr_addr, unsigned char *instr_buffer, char *disass_instr_buffer, int symbolic_info)
{ 
  char *disass_instr_ptr;	/* pointeur chaine instruction desassemblee */
  char *disass_instr_src;	/* pointeur chaine source instruction desassemblee */
  uchar *instr_ptr;			/* pointeur de l'instruction desassemblee */
  ushort opcode;			/* code operation a desassembler */
  struct inst7 *instr_desc;	/* pointeur des descripteurs des instructions */
  char disass_value_buffer[STRING_LENGTH];		/* chaine de la valeur desassemblee */
  int current_length = 0;
#ifdef TARGET_STM7
  uchar *after_opcode_ptr;
  int operands_size = 0;
#endif

  internal_instr_addr = instr_addr;

  instr_ptr = instr_buffer;    
  memset(disass_instr_buffer, '\0', STRING_LENGTH);
  opcode = *instr_ptr++;
  internal_instr_addr++;
  if (opcode == PRECOD1 || opcode == PRECOD2 || opcode == PRECOD3
#ifdef TARGET_STM7
	  || opcode == PRECOD4
#endif
	  )
  {
	opcode = opcode << 8 | *instr_ptr++;
    internal_instr_addr++;
  }
#ifdef TARGET_STM7
  after_opcode_ptr = instr_ptr;
#endif
  for (disass_instr_ptr = disass_instr_buffer, instr_desc = mcucore_instr;
	   instr_desc->codop != DESFIN;
	   instr_desc++)
  { 
	if (instr_desc->codop == opcode)
    { 
	  for (disass_instr_src = mcucore_tokens[instr_desc->mnemonic];
		   *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
		   current_length++)
		*disass_instr_ptr++ = *disass_instr_src++;
      if (instr_desc->prefix1 != END)
      {
		for (;
		     current_length < OPERAND && current_length < (STRING_LENGTH - 1);
			 current_length++)
		  *disass_instr_ptr++ = ' ';
        if (instr_desc->prefix1 != EMPTY)
        { 
		  for(disass_instr_src = mcucore_tokens[instr_desc->prefix1];
			  *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
			  current_length++)
			*disass_instr_ptr++ = *disass_instr_src++;
        }
        if (instr_desc->type1 != EMPTY)
        {
#ifdef TARGET_STM7
		  instr_ptr = after_opcode_ptr + instr_desc->position1;
		  operands_size += numval_disass(instr_desc->type1, instr_ptr, disass_value_buffer, STRING_LENGTH, symbolic_info);
#else
		  instr_ptr = numval_disass(instr_desc->type1, instr_ptr, disass_value_buffer, STRING_LENGTH, symbolic_info);
#endif        
		  for (disass_instr_src = disass_value_buffer;
			   *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
			   current_length++)
			*disass_instr_ptr++ = *disass_instr_src++;
        }
        if (instr_desc->postfix1 != EMPTY)
        { 
		  for (disass_instr_src = mcucore_tokens[instr_desc->postfix1];
			   *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
			   current_length++)
			*disass_instr_ptr++ = *disass_instr_src++;
        }
        if (instr_desc->prefix2 != END)
        { 
		  if (current_length < (STRING_LENGTH - 1))
		  {
		    *disass_instr_ptr++ = SEPARATOR;
			current_length++;
		  }
          if (instr_desc->prefix2 != EMPTY)
          { 
			for (disass_instr_src = mcucore_tokens[instr_desc->prefix2];
				 *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
				 current_length++)
			  *disass_instr_ptr++ = *disass_instr_src++;
          }
          if (instr_desc->type2 != EMPTY)
          {
#ifdef TARGET_STM7
			instr_ptr = after_opcode_ptr + instr_desc->position2;
			operands_size += numval_disass(instr_desc->type2, instr_ptr, disass_value_buffer, STRING_LENGTH, symbolic_info);
#else
			instr_ptr = numval_disass(instr_desc->type2, instr_ptr, disass_value_buffer, STRING_LENGTH, symbolic_info);
#endif
            for (disass_instr_src = disass_value_buffer;
				 *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
				 current_length++)
			  *disass_instr_ptr++ = *disass_instr_src++;
          }
          if (instr_desc->postfix2 != EMPTY)
          { 
			for (disass_instr_src = mcucore_tokens[instr_desc->postfix2];
				 *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
				 current_length++)
			  *disass_instr_ptr++ = *disass_instr_src++;
          }
          if (instr_desc->type3 != END)
          {
			if (current_length < (STRING_LENGTH - 1))
			{
			  *disass_instr_ptr++ = SEPARATOR;
			  current_length++;
			}
#ifdef TARGET_STM7
			instr_ptr = after_opcode_ptr + instr_desc->position3;
			operands_size += numval_disass(instr_desc->type3, instr_ptr, disass_value_buffer, STRING_LENGTH, symbolic_info);
#else
            instr_ptr = numval_disass(instr_desc->type3, instr_ptr, disass_value_buffer, STRING_LENGTH, symbolic_info);
#endif
            for (disass_instr_src = disass_value_buffer;
				 *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
				 current_length++)
			  *disass_instr_ptr++ = *disass_instr_src++;
		  }
		}
	  }
      break;
	}
  }
  if (instr_desc->codop == DESFIN)
  { 
	for (disass_instr_src = mcucore_tokens[instr_desc->mnemonic];
	     *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
		 current_length++)
	  *disass_instr_ptr++ = *disass_instr_src++;
	for (;
		 current_length < OPERAND && current_length < (STRING_LENGTH - 1);
		 current_length++)
	  *disass_instr_ptr++ = ' ';
    if ((opcode & 0xff00) != 0)
    {
	  opcode >>= 8;
      instr_ptr--;
      internal_instr_addr--;
    }
    sprintf(disass_value_buffer, "%02X", opcode);
    for (disass_instr_src = disass_value_buffer;
		 *disass_instr_src != '\0' && current_length < (STRING_LENGTH - 1);
		 current_length++)
	  *disass_instr_ptr++ = *disass_instr_src++;
  }
#ifdef TARGET_STM7
  return after_opcode_ptr + operands_size - instr_buffer;
#else
  return instr_ptr - instr_buffer;
#endif
}


/********************************************/
/*   DESASSEMBLAGE D'UNE VALEUR NUMERIQUE   */
/********************************************/
/*
  type:	type de la valeur a desassembler.
  value_ptr: pointeur de l'instruction a desassembler.
  disass_value_string: pointeur de la chaine de la valeur desassemblee.
  length: maximum result string length.
  symbolic_info: symbolic information.
*/
#ifdef TARGET_STM7
int
#else
uchar *
#endif
numval_disass
(uchar type, uchar *value_ptr, char *disass_value_string, int length, int symbolic_info)
{
  ulong val = 0;            /* valeur a desassembler */
  char tmp_buffer[10];
  int char_number;
#ifdef TARGET_STM7
  int operand_length;
#endif

  memset(disass_value_string, '\0', length);

  switch(type)
  {
    case A08:
      val = *value_ptr++;
      if (!print_symbol(symbolic_info, disass_value_string, length, val, internal_instr_addr))
	  {
        char_number = sprintf(tmp_buffer, "0x%02x", val);
		if (char_number > (length - 1))
		  char_number = length - 1;
		strncpy(disass_value_string, tmp_buffer, char_number);
	  }

      internal_instr_addr++;
#ifdef TARGET_STM7
	  operand_length = 1;
#endif
      break;
    case A16:
	case A16NA:
      val = (ulong)*value_ptr++ << 8;
      val |= *value_ptr++;
      if (!print_symbol(symbolic_info, disass_value_string, length, val, internal_instr_addr))
	  {
        char_number = sprintf(tmp_buffer,"0x%04x",val);
		if (char_number > (length - 1))
		  char_number = length - 1;
		strncpy(disass_value_string, tmp_buffer, char_number);
	  }
      internal_instr_addr += 2;
#ifdef TARGET_STM7
	  operand_length = 2;
#endif
      break;
#ifdef TARGET_STM7
	case A24:
      val = (ulong)*value_ptr++ << 16;
	  val |= (ulong)*value_ptr++ << 8;
      val |= *value_ptr++;
      if (!print_symbol(symbolic_info, disass_value_string, length, val, internal_instr_addr))
	  {
        char_number = sprintf(tmp_buffer,"0x%06x",val);
		if (char_number > (length - 1))
		  char_number = length - 1;
		strncpy(disass_value_string, tmp_buffer, char_number);
	  }
      internal_instr_addr += 3;
	  operand_length = 3;
      break;
#endif
    case D08:
      val = internal_instr_addr + 1 + *(char *)value_ptr++;
      if (!print_symbol(symbolic_info, disass_value_string, length, val, internal_instr_addr))
	  {
        char_number = sprintf(tmp_buffer,"0x%02x",val);
		if (char_number > (length - 1))
		  char_number = length - 1;
		strncpy(disass_value_string, tmp_buffer, char_number);
	  }
      internal_instr_addr++;
#ifdef TARGET_STM7
	  operand_length = 1;
#endif
      break;
    case I08:
    case N08:
      val = *value_ptr++;
      char_number = sprintf(tmp_buffer, "0x%02x", val);
	  if (char_number > (length - 1))
		char_number = length - 1;
	  strncpy(disass_value_string, tmp_buffer, char_number);
      internal_instr_addr++;
#ifdef TARGET_STM7
	  operand_length = 1;
#endif
      break;
    case I16:
#ifdef TARGET_STM7P
	case N16:
#endif
      val = *value_ptr++<<8;
      val |= *value_ptr++;
      char_number = sprintf(tmp_buffer,"0x%04x",val);
	  if (char_number > (length - 1))
		char_number = length - 1;
	  strncpy(disass_value_string, tmp_buffer, char_number);
      internal_instr_addr += 2;
#ifdef TARGET_STM7
	  operand_length = 2;
#endif
      break;
    case N03:
#ifdef TARGET_STM7
      val = ((*value_ptr) >> 1) & VAL03;
#else
	  val = (*(value_ptr-2)>>1)&VAL03;
#endif
      char_number = sprintf(tmp_buffer,"%1x",val);
	  if (char_number > (length - 1))
		char_number = length - 1;
	  strncpy(disass_value_string, tmp_buffer, char_number);
#ifdef TARGET_STM7
	  operand_length = 0;
#endif
      break;
    default:
#ifdef TARGET_STM7
	  operand_length = 0;
#endif
      break;
  }
#ifdef TARGET_STM7
  return operand_length;
#else
  return value_ptr;
#endif
}


int
print_insn_st7 (bfd_vma memaddr, struct disassemble_info *info)
{
  int i, status, size_to_read;
  int instr_length;							/* instruction length */
  char disass_instr_buffer[STRING_LENGTH];	/* disassembled instruction */
  unsigned char instr_buffer[MAXILEN];

  /* disassemble mode is constant at the moment */
  info->flags  =  DISASS_HEXA | DISASS_DUMP | DISASS_SYMBOL;

  if (info->buffer != NULL && info->buffer_length == MAXILEN) {
	  for (i=0; i<MAXILEN; i++) {
		instr_buffer[i] = *(info->buffer+i);
	  }
  } else {
	size_to_read = MAXILEN;
	status = (*info->read_memory_func) (memaddr, (bfd_byte*) instr_buffer, size_to_read, info);
	if (status != 0) {
		(*info->memory_error_func) (status, memaddr, info);
		return -1;
    }
  }

  /* No symbolic information. */
  instr_length = st7_disass(memaddr, instr_buffer, disass_instr_buffer, 0);

  if (info->flags & DISASS_DUMP) /* make hexa dump appear */ 
  /* Print bytes value in hexa.
   */
    {
      int	i;

      /* Insert 0x prefix */
	  if (instr_length > 0)
        (*info->fprintf_func)(info->stream, "0x");

      for (i = 0; i < instr_length; i++)
		(*info->fprintf_func)(info->stream, "%02X", instr_buffer[i]);

      /* Pad with tabulation */
      (*info->fprintf_func)(info->stream, "\t");
    }


  if (info->flags & DISASS_HEXA)
  { 
	/* make instruction dump appear */
    (*info->fprintf_func) (info->stream, "%s", disass_instr_buffer);
    if (info->flags & DISASS_SYMBOL)
	{
	  /* if we want also symbolic disass, we need tabulation here */
      (*info->fprintf_func)(info->stream, "\t");
	}
     
  }

  if (info->flags & DISASS_SYMBOL)
  {
	/* Request symbolic information. */
    instr_length = st7_disass(memaddr, instr_buffer, disass_instr_buffer, 1);
    (*info->fprintf_func) (info->stream, "%s", disass_instr_buffer);
  }
     
  return instr_length;
}

int
get_disassembled_instruction (bfd_vma memaddr,
                              struct disassemble_info *info,
	                          char *disass_instr_buffer
				              /* cha�ne de l'instruction d�sassembl�e */)
{
  int status, size_to_read;
  register int instr_length;    /* longueur de l'instruction desassemblee */

  unsigned char instr_buffer[MAXILEN];

  /* disassemble mode is constant at the moment */
  info->flags  =  DISASS_HEXA | DISASS_DUMP | DISASS_SYMBOL;

  size_to_read = MAXILEN;

  status = (*info->read_memory_func) (memaddr, (bfd_byte*) instr_buffer, size_to_read, info);
  if (status != 0)
    {
      (*info->memory_error_func) (status, memaddr, info);
      return -1;
    }

  /* No symbolic information. */
  instr_length = st7_disass(memaddr, instr_buffer, disass_instr_buffer, 0);
     
  return instr_length;
}

int
get_code_and_disassembled_instruction (bfd_vma memaddr,
										struct disassemble_info *info,
										char *hexa_instr_buffer,
										char *disass_instr_buffer
										/* disassembled instruction */)
{
  int status, size_to_read;
  register int instr_length;    /* instruction length */

  unsigned char instr_buffer[MAXILEN];
  int i, n;

  /* disassemble mode is constant at the moment */
  info->flags  =  DISASS_HEXA | DISASS_DUMP | DISASS_SYMBOL;

  size_to_read = MAXILEN;

  status = (*info->read_memory_func) (memaddr, (bfd_byte*) instr_buffer, size_to_read, info);
  if (status != 0)
    {
      (*info->memory_error_func) (status, memaddr, info);
      return -1;
    }


  /* No symbolic information. */
  instr_length = st7_disass(memaddr, instr_buffer, disass_instr_buffer, 0);

  n = sprintf(hexa_instr_buffer, "0x");
  for (i = 0; i < instr_length; i++)
  {
	  n += sprintf(hexa_instr_buffer+n, "%02x", instr_buffer[i]);
  }
  return instr_length;
}
