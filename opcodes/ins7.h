/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/***************************************/
/*   DEFINITION FOR ST7 INSTRUCTIONS   */
/***************************************/
#include "../gdb/st7.h"

#define TARGET_STM7
#define TARGET_STM7P

#define uchar  unsigned char
#define ulong  unsigned long
#define ushort unsigned short

#define DESFIN  0xfffd /* Disassembler code for no more instructions */
#define ASSFIN  0xffff /* Assembler code for no more instructions */

#ifdef TARGET_STM7
#define MAXILEN      5 /* Maximum number of bytes of STM7 instructions */
#else
#define MAXILEN      4 /* Maximum number of bytes of ST7 instructions */
#endif

#define STRING_LENGTH     120 /* Maximum length of instruction string */
#define OPERAND      6 /* Position index of operands */
#define PRECOD1   0x90 /* First precode of ST7 instructions */
#define PRECOD2   0x91 /* Second precode of ST7 instructions */
#define PRECOD3   0x92 /* Third precode of ST7 instructions */
#define PRECOD4   0x72 /* Fourth precode of STM7 instructions */

#define EMPTY     254
#define END       255	/* Code for instruction end */


#define UNUSED		0
#define NF_5_UNUSED		0, 0, 0, 0, 0 /* Number of unused fields is 5 */
#define NF_9_UNUSED		0, 0, 0, 0, 0, 0, 0, 0, 0 /* Number of unused fields is 9 */
#define NF_11_UNUSED	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /* Number of unused fields is 11 */

#define SEPARATOR  ',' /* Operand separator */
#define A08        128 /* Code for an 8-bit address */
#define A16        129 /* Code for a 16-bit address */
#define D08        130 /* Code for an 8-bit offset */
#define I08        131 /* Code for an 8-bit index */
#define I16        132 /* Code for a 16-bit index */
#define N03        133 /* Code for a 3-bit number */
#define N08        134 /* Code for a 8-bit number */
#define A24        135 /* Code for a 24-bit address */
#define A16NA	   136 /* A16 NO ALTERNATIVE: A08 not possible
                          there is no instruction dedicated to one-byte operands. */
#ifdef TARGET_STM7P
#define N16        137 /* Code for a 16-bit number */
#endif

#define VAL00 (long)0x00000000 /* Maximum value for 0 bit */
#define VAL03 (long)0x00000007 /* Maximum value for 3 bits */
#define VAL07 (long)0x0000007f /* Maximum value for 7 bits */
#define VAL08 (long)0x000000ff /* Maximum value for 8 bits */
#define VAL16 (long)0x0000ffff /* Maximum value for 16 bits */
#define VAL24 (long)0x00ffffff /* Maximum value for 24 bits */
#define VALN7 (long)0xffffff80 /* Maximum negative value for 7 bits */

#ifdef TARGET_STM7
struct inst7  /* STM7 instruction */
{ 
  ushort codop; /* operation code */
  uchar mnemonic;  /* index of mnemonic string */

  uchar prefix1;  /* index of 1st operand start string */
  uchar type1;  /* type of 1st operand for numbers */
  char position1; /* position of 1st operand:
                    0=just after opcode,
                    1=one byte after opcode,
                    n=n bytes after opcode */
  uchar postfix1;  /* index of 1st operand end string */

  uchar prefix2;  /* index of 2nd operand start string */
  uchar type2;  /* type of 2nd operand for numbers */
  char position2; /* position of 2nd operand */
  uchar postfix2;  /* index of 2nd operand end string */

  uchar type3;  /* type of 3rd operand for numbers */
  char position3; /* position of 3rd operand */
};
#else
struct inst7  /* ST7 instruction */
{
  ushort codop; /* operation code */
  uchar mnemonic;  /* index of mnemonic string */

  uchar prefix1;  /* index of 1st operand start string */
  uchar type1;  /* type of 1st operand for numbers */
  uchar postfix1;  /* index of 1st operand end string */

  uchar prefix2;  /* index of 2nd operand start string */
  uchar type2;  /* type of 2nd operand for numbers */
  uchar postfix2;  /* index of 2nd operand end string */

  uchar type3;  /* type of 3rd operand for numbers */
};
#endif

extern char *st7_tokens[];
extern struct inst7 st7_instr[];
extern char *stm7_tokens[];
extern struct inst7 stm7_instr[];

#ifdef TARGET_STM7P
extern char *stm7pv1_tokens[];
extern struct inst7 stm7pv1_instr[];
extern char *stm7pv2_tokens[];
extern struct inst7 stm7pv2_instr[];
#endif

extern char **mcucore_tokens;
extern struct inst7 *mcucore_instr;

void select_instruction_table(core_type mcu_core_variant);
int get_instr_mnemonic(unsigned short opcode, char *mnemonic);
