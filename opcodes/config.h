
/* This file has been modified by STMicroelectronics on 03 May 2000. */

/* config.h.  Generated automatically by configure.  */
/* config.in.  Generated automatically from configure.in by autoheader.  */

/* Name of package.  */
#define PACKAGE "opcodes"

/* Version of package.  */
#define VERSION "2.8.2"

/* Define if you have the <stdlib.h> header file.  */
#define HAVE_STDLIB_H 1

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H 1

/* Define if you have the <strings.h> header file.  */
#define HAVE_STRINGS_H 1
