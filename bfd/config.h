
/* This file has been modified by STMicroelectronics on 03 May 2000. */

/* config.h.  Generated automatically by configure.  */
/* config.in.  Generated automatically from configure.in by autoheader.  */

/* Name of package.  */
#define PACKAGE "bfd"

/* Version of package.  */
#define VERSION "2.8.3"

/* Whether strstr must be declared even if <string.h> is included.  */
/* #undef NEED_DECLARATION_STRSTR */

/* Whether malloc must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_MALLOC */

/* Whether realloc must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_REALLOC */

/* Whether free must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_FREE */

/* Whether getenv must be declared even if <stdlib.h> is included.  */
/* #undef NEED_DECLARATION_GETENV */

/* Define if you have a working `mmap' system call.  */
/* #undef HAVE_MMAP */

/* Define if you need to in order for stat and other things to work.  */
/* #undef _POSIX_SOURCE */

/* Define if you can safely include both <sys/time.h> and <time.h>.  */
/* #undef TIME_WITH_SYS_TIME */

/* Do we need to use the b modifier when opening binary files?  */
#define USE_BINARY_FOPEN 1

/* Name of host specific header file to include in trad-core.c.  */
/* #undef TRAD_HEADER */

/* Define only if <sys/procfs.h> is available *and* it defines prstatus_t.  */
/* #undef HAVE_SYS_PROCFS_H */

/* Do we really want to use mmap if it's available?  */
/* #undef USE_MMAP */

/* Define if you have the fcntl function.  */
#define HAVE_FCNTL 1

/* Define if you have the fdopen function.  */
#define HAVE_FDOPEN 1

/* Define if you have the getpagesize function.  */
/* #undef HAVE_GETPAGESIZE */

/* Define if you have the madvise function.  */
/* #undef HAVE_MADVISE */

/* Define if you have the mprotect function.  */
/* #undef HAVE_MPROTECT */

/* Define if you have the setitimer function.  */
#define HAVE_SETITIMER 1

/* Define if you have the sysconf function.  */
#define HAVE_SYSCONF 1

/* Define if you have the <fcntl.h> header file.  */
#define HAVE_FCNTL_H 1

/* Define if you have the <stddef.h> header file.  */
#define HAVE_STDDEF_H 1

/* Define if you have the <stdlib.h> header file.  */
#define HAVE_STDLIB_H 1

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H 1

/* Define if you have the <strings.h> header file.  */
#define HAVE_STRINGS_H 1

/* Define if you have the <sys/file.h> header file.  */
/* #undef HAVE_SYS_FILE_H */

/* Define if you have the <sys/time.h> header file.  */
/* #undef HAVE_SYS_TIME_H */

/* Define if you have the <time.h> header file.  */
#define HAVE_TIME_H 1

/* Define if you have the <unistd.h> header file.  */
/* #undef HAVE_UNISTD_H */
