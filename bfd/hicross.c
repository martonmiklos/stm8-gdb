/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* BFD back-end for HI-CROSS objects.
   Written by Michel Cazal.

This file is part of BFD, the Binary File Descriptor library.
*/

/* HI-CROSS format is described in the documentation:
   Specification of the HI-CROSS Object File Formats
   Revision 2.8.2
   Copyright 1994,HIWARE AG */

#include "bfd.h"
#include "sysdep.h"
#include "libbfd.h"
#include "hicross.h"
#include "libhicross.h"
#include "../gdb/st7.h"

/***************************************************************************
Functions for reading from hicross files in the strange way that the
standard requires:
*/

#define hicross_seek(abfd, offset) \
  HICROSS_DATA(abfd)->h.input_p = HICROSS_DATA(abfd)->h.first_byte + offset

#define hicross_skip(abfd, offset) \
  HICROSS_DATA(abfd)->h.input_p = HICROSS_DATA(abfd)->h.input_p + offset

#define hicross_pos(abfd)   HICROSS_DATA(abfd)->h.input_p -HICROSS_DATA(abfd)->h.first_byte 


#define this_byte(hicross) *((hicross)->input_p)
#define next_byte(hicross) ((hicross)->input_p++)
#define this_byte_and_next(hicross) (*((hicross)->input_p++))

#define STOP_BIT 0x80
#define SIGN_BIT 0x40

static long 
DEFUN(decode_number,(hicross),
   common_header_type *hicross)
{
  char byte = this_byte_and_next(hicross);
  unsigned long num = 0;
  unsigned long i = 1;
  while ((byte & STOP_BIT) == 0) {
     num = num + byte * i;
     i = i * 128;
     byte = this_byte_and_next(hicross);
  }
  num = num + (byte & 0x3F) * i;
  if (byte & SIGN_BIT)
    return -num;
  else
    return num;
}

#define ALWAYS_BIT 0x40

#define MAX_HICROSS_NAME_LENGTH 1024 /* ### Should be unlimited, Fixme */

static long 
DEFUN(decode_number1,(hicross),
   common_header_type *hicross)
{
  char byte = this_byte_and_next(hicross);
  unsigned long num = 0;
  unsigned long i = 1;
  while ((byte & STOP_BIT) == 0) {
     num = num + (byte - ALWAYS_BIT) * i;
     i = i * 64;
     byte = this_byte_and_next(hicross);
  }
  num = num + (byte & 0x3F) * i;
  if (byte & SIGN_BIT)
    return -num;
  else
    return num;
}

static unsigned short 
DEFUN(read_word,(hicross),
   common_header_type *hicross)
{
  unsigned  char c1 = this_byte_and_next(hicross);
  unsigned  char c2 = this_byte_and_next(hicross);
  return (c1 * 256 + c2);

}

static unsigned long 
DEFUN(read_long,(hicross),
   common_header_type *hicross)
{
  unsigned  char c1 = this_byte_and_next(hicross);
  unsigned  char c2 = this_byte_and_next(hicross);
  unsigned  char c3 = this_byte_and_next(hicross);
  unsigned  char c4 = this_byte_and_next(hicross);
  return (((((c1 * 256) + c2) * 256) + c3) * 256 + c4);

}

static char *
DEFUN(read_name,(hicross),
  common_header_type *hicross)
{
  size_t length = 0;
  char *string;
  char buffer[MAX_HICROSS_NAME_LENGTH];
  buffer[0] = this_byte_and_next(hicross);
  while (buffer[length] != 0) {
    length = length + 1;
    buffer[length] = this_byte_and_next(hicross);
  }
  /* Buy memory */
  string = bfd_alloc(hicross->abfd, length+1);
  strcpy(string, buffer);
  return string;
}

/************************ local processing functions ****************/

static unsigned int last_index;
static hicross_symbol_type *
DEFUN(get_symbol,(abfd, 
		  hicross,  
		  last_symbol,
		  symbol_count,
		  pptr,
		  max_index
		  ),
      bfd *abfd AND
      hicross_data_type *hicross AND
      hicross_symbol_type *last_symbol AND
      unsigned int *symbol_count AND
      hicross_symbol_type *** pptr AND
      unsigned int *max_index
      )
{
  /* Need a new symbol */
  unsigned int new_index = decode_number(&(hicross->h));
  if (new_index != last_index) { /* ### MC: TO BE REMOVED */
    hicross_symbol_type *  new_symbol =
      (hicross_symbol_type *)bfd_alloc(hicross->h.abfd,
                                       sizeof(hicross_symbol_type));

    new_symbol->index = new_index;
    ( *symbol_count)++;
    ** pptr= new_symbol;
    *pptr = &new_symbol->next;
    if (new_index > *max_index) {
      *max_index = new_index;
    }
    return new_symbol;
  }
  return last_symbol;
}


static void
DEFUN(hicross_slurp_external_symbols,(abfd),
      bfd *abfd)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
  hicross_symbol_type  *symbol = (hicross_symbol_type *)NULL;
  hicross_symbol_type **prev_symbols_ptr = &hicross->external_symbols;
  boolean loop = true;
  unsigned int symbol_count = 0;
  unsigned long count, i, unused;
  unsigned int external_symbol_max_index = 0;
  last_index = 0xffffff;

  hicross_seek(abfd, hicross->emap_file_offset);

  while (this_byte_and_next(&(hicross->h)) == OBJLISTTAG) {
    count = decode_number (&(hicross->h)); /* number of objects */

    unused = decode_number (&(hicross->h)); /* number of procs */
    unused = decode_number (&(hicross->h)); /* number of vars */

    for (i = 0; i < count; i++) {
      
      symbol = get_symbol(abfd, hicross, symbol, &symbol_count,
			  &prev_symbols_ptr, 
			  &external_symbol_max_index);
      
      symbol->symbol.the_bfd = abfd;

      switch this_byte_and_next(&(hicross->h)) {
        case 'P': symbol->symbol.flags = BSF_LOCAL;
                  break;
        case 'E': symbol->symbol.flags = BSF_GLOBAL;
                  break;
        default : symbol->symbol.flags = BSF_NO_FLAGS;
      }

      symbol->symbol.name = read_name(&(hicross->h));
      {
         char * type = read_name(&(hicross->h));
      }

      switch this_byte_and_next(&(hicross->h)) {
        case VARIABLETAG :
          unused = decode_number(&(hicross->h)); /* size */
          symbol->symbol.value = decode_number(&(hicross->h));
          { unsigned long i, nb_inits;
            nb_inits = decode_number(&(hicross->h));
            for (i = 0; i < nb_inits; i++) {
              unused = decode_number(&(hicross->h)); /* size */
              unused = decode_number(&(hicross->h)); /* address */
              unused = decode_number(&(hicross->h)); /* posinfile */
            }
          }
          break;
        case STRINGTAG :
          unused = decode_number(&(hicross->h)); /* size */
          symbol->symbol.value = decode_number(&(hicross->h)); /* address */
          unused = decode_number(&(hicross->h)); /* posinfile */
          break;
        case PROCEDURETAG :
          unused = decode_number(&(hicross->h)); /* size */
          symbol->symbol.value = decode_number(&(hicross->h));
          unused = decode_number(&(hicross->h)); /* posinfile */
          unused = decode_number(&(hicross->h)); /* frameoffset */
          break;
        case LABELTAG :
          symbol->symbol.value = decode_number(&(hicross->h));
          break;
        case CONSTTAG :
          symbol->symbol.value = decode_number(&(hicross->h));
          break;
        default : break;
      }

      symbol->symbol.section = &bfd_abs_section;
      symbol->symbol.udata.p = (PTR)NULL;
    }

  }

  abfd->symcount = symbol_count;

  *prev_symbols_ptr = (hicross_symbol_type *)NULL;

}

void
DEFUN(hicross_slurp_symbol_table,(abfd),
      bfd *abfd)
{
  if (HICROSS_DATA(abfd)->read_symbols == false) {
    hicross_slurp_external_symbols(abfd);
    HICROSS_DATA(abfd)->read_symbols= true;
  }
}

unsigned int
DEFUN(hicross_get_symtab_upper_bound,(abfd),
      bfd *abfd)
{
  hicross_slurp_symbol_table (abfd);

  return (abfd->symcount != 0) ? 
    (abfd->symcount+1) * (sizeof (hicross_symbol_type *)) : 0;
return 0;
}

/* 
Move from our internal lists to the canon table, and insert in
symbol index order
*/

extern bfd_target hicross_vec;
unsigned int
DEFUN(hicross_get_symtab,(abfd, location),
      bfd *abfd AND
      asymbol **location)
{
  hicross_symbol_type *symp;
  unsigned int i = 0;

  if (abfd->symcount) 
  {
    hicross_data_type *hicross = HICROSS_DATA(abfd);
    hicross_slurp_symbol_table(abfd);

    for (symp = HICROSS_DATA(abfd)->external_symbols;
	 symp != (hicross_symbol_type *)NULL;
	 symp = symp->next) {
      /* Place into table at correct index locations */
      location[i++] = &symp->symbol;

    }

   location[abfd->symcount] = (asymbol *)NULL;
  }

  return abfd->symcount;

}

/* Read module information in the executable. */
static void
DEFUN(hicross_swap_module_in,(abfd, offset, module),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_module_type * module)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
  unsigned long objlist_pos;

  /* Read module information in the Module Directory block of the executable. */
  hicross_seek(abfd, *offset);
  module->ModuleName = read_name(&(hicross->h));
  module->Language = this_byte_and_next(&(hicross->h));
  objlist_pos = decode_number(&(hicross->h));
  module->LowAddr = decode_number(&(hicross->h));
  module->HighAddr = decode_number(&(hicross->h));
  *offset = hicross_pos (abfd);

  /* Go to the module object list block within the list of object lists
     in the symbol information block of the executable
     and read more module information. */
  hicross_seek(abfd, objlist_pos);
  if (this_byte_and_next(&(hicross->h)) != OBJLISTTAG) return;
  module->NBofObjects = decode_number(&(hicross->h));
  module->NBofProcs = decode_number(&(hicross->h));
  module->NBofVars = decode_number(&(hicross->h));
  module->objlist_offset = hicross_pos (abfd);
}

static hicross_type_component_type *
DEFUN(hicross_swap_object_type,(hicross),
      hicross_data_type *hicross);

static hicross_type_component_type *
DEFUN(hicross_swap_object_type1,(hicross),
      hicross_data_type *hicross)
{
  hicross_type_component_type *t, *list;
  char kind = this_byte_and_next(&(hicross->h));

  if (kind == 0) return NULL;
  if (kind == ')') return NULL;

  /* Buy memory */
  t = bfd_alloc(hicross->h.abfd, sizeof (hicross_type_component_type));
  t->kind = kind;
  t->next = NULL;
  switch (t->kind) {
    case '/' :
      {
        size_t length = 0;
        char *string;
        char buffer[MAX_HICROSS_NAME_LENGTH];
        buffer[0] = this_byte_and_next(&(hicross->h));
        while (buffer[length] != '/') {
          length = length + 1;
          buffer[length] = this_byte_and_next(&(hicross->h));
        }
        /* Buy memory */
        string = bfd_alloc(hicross->h.abfd, length+1);
        strncpy(string, buffer, length);
	string[length] = 0;
	t->u.def.name = string;
      }
      break;
    case 'E' :
      /* Be careful, there is a problem with 'E' type. In revision
	     2.8.2 of HI-CROSS Object File Formats, enumeration type
		 'E' was followed by Size and Enumeration Index.
		 In revision 2.8.12 Size of enum is removed ! */
      /* removed: t->u.enu.Size = decode_number(&(hicross->h)); */
      t->u.enu.Index = decode_number1(&(hicross->h));
      break;
    case 'S' :
      /* Be careful, there is a problem with 'S' type when
	     changing the format HIWARE 2.7 to HIWARE. In 2.7,
		 unsigned short appeared as unsigned integers 'I'. Now
		 they are 'S' this makes appear a mistake with
		 'Set' type. 'Set' is modula-2 only language according
		 to V2.8.12 Hi-cross Object File Format documentation so
		 we keep 'S' for unsigned integers */
      break;
    case 'R' :
      t->u.sub.type = hicross_swap_object_type1 (hicross);
      t->u.sub.Low = decode_number1(&(hicross->h)); 
      t->u.sub.High = decode_number1(&(hicross->h));
      break;
    case '(' :
      t->u.fun.params = hicross_swap_object_type (hicross);
      t->u.fun.Return = hicross_swap_object_type (hicross);
      /* ### HICROSS bug ??? How to distinguish end of return type ? */
      /* skip back to the null char to indicate end of type string */
      hicross_seek(hicross->h.abfd,
                   (unsigned long) hicross_pos (hicross->h.abfd) - 1);
      break;
    case '[' :
      t->u.arr.ElementNB = decode_number1(&(hicross->h));
      t->u.arr.RangeBeg = decode_number1(&(hicross->h));
      t->u.arr.SizeOfArray = decode_number1(&(hicross->h));
      break;
    case 'X' :
    case 'U' :
      t->u.su.Index = decode_number1(&(hicross->h));
      break;
  }
  return t;
}

/* Read object type in the Object Summary of the Object List
   of the executable file. */
/* An object type may have several object type components;
   the list of object type components ends with a zero. */
static hicross_type_component_type *
DEFUN(hicross_swap_object_type,(hicross),
      hicross_data_type *hicross)
{
  hicross_type_component_type *t, *list;

  list = hicross_swap_object_type1 (hicross);
  t = list;

  while (t != NULL) {
    t->next = hicross_swap_object_type1 (hicross);
    t = t->next;
  }

  return list;
}

/* Read object type in the executable file. */
static void
DEFUN(hicross_swap_objtype_in,(abfd, offset, objecttype),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_type_component_type **objecttype)
      
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
 
  hicross_seek(abfd, *offset);

  *objecttype = hicross_swap_object_type (hicross);
}

/* Read the Object Summary in the Object List of the executable.
   It is composed of an object number, a private/exported flag,
   an object name, an object type and an object kind. */
static void
DEFUN(hicross_swap_object_in,(abfd, offset, object),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_object_type * object)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
 
  hicross_seek(abfd, *offset);
  object->ObjNB = decode_number(&(hicross->h));
  switch (this_byte_and_next(&(hicross->h))) {
    case 'P': object->exported = false;
              break;
    case 'E': object->exported = true;
              break;
    default : return;
  }
  object->ObjName = read_name(&(hicross->h));
  object->objtype_offset = hicross_pos (abfd);
  while (this_byte_and_next(&(hicross->h)) != 0); /* skip type string */
  object->k.kind = this_byte_and_next(&(hicross->h));
  switch (object->k.kind) {
    case VARIABLETAG :
      object->k.u.v.Size = decode_number(&(hicross->h));
      object->k.u.v.Address = decode_number(&(hicross->h));
      object->k.u.v.NBofInits = decode_number(&(hicross->h));
      /* ### NYI parse init positions and alloc them here */
      {
         unsigned long i;
         unsigned long dummy;
         for (i = 0; i < object->k.u.v.NBofInits; i++) {
            dummy = decode_number(&(hicross->h));
            dummy = decode_number(&(hicross->h));
            dummy = decode_number(&(hicross->h));
         }
      }
      break;
    case STRINGTAG :
      object->k.u.s.Position.Size = decode_number(&(hicross->h));
      object->k.u.s.Position.Address = decode_number(&(hicross->h));
      object->k.u.s.Position.PosInFile = decode_number(&(hicross->h));
      break;
    case PROCEDURETAG :
      object->k.u.p.Position.Size = decode_number(&(hicross->h));
      object->k.u.p.Position.Address = decode_number(&(hicross->h));
      object->k.u.p.Position.PosInFile = decode_number(&(hicross->h));
      object->k.u.p.FrameOffset = decode_number(&(hicross->h));
      break;
    case LABELTAG :
      object->k.u.l.Address = decode_number(&(hicross->h));
      break;
    case CONSTTAG :
      object->k.u.k.Value = decode_number(&(hicross->h));
      break;
    default : return;
  }
  *offset = hicross_pos (abfd);

}

/* Unit List is part of the Directory block located at the end of the object file. */
/* Each unit has a name and a key. */
static void
DEFUN(hicross_swap_unit_in,(abfd, offset, unit),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_unit_type * unit)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
 
  hicross_seek(abfd, *offset);
  unit->UnitName = read_name(&(hicross->h));
  unit->UnitKey = decode_number(&(hicross->h));

  *offset = hicross_pos (abfd);
}


/* Return true if a procedure has been found at offset position, return
   false otherwise. Leave offset at the beginning of SourceCodePairs */
static boolean
DEFUN(hicross_swap_proc_source_position_in,(abfd, offset, srcpos),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_source_position_type * srcpos)
{
  /* the only thing we are interested in is to get the offset of the
     procedure source positions */

  unsigned long dummy, i;

  hicross_data_type *hicross = HICROSS_DATA(abfd);

  hicross_seek(abfd, *offset);
  if (this_byte_and_next(&(hicross->h)) != PROCEDURETAG ) return false;

  dummy = decode_number(&(hicross->h));
  dummy = decode_number(&(hicross->h));
  hicross_skip(abfd, dummy); /* skip Code area */
  if (this_byte_and_next(&(hicross->h)) != FIXUPTAG ) return false;
  dummy = decode_number(&(hicross->h));
  /* skip Fixups area */
  for (i = 0; i < dummy; i++) {
    unsigned long anumber;

    next_byte(&(hicross->h)); /* FixupType */
    anumber = decode_number(&(hicross->h)); /* CodeOffset */
    anumber = decode_number(&(hicross->h)); /* Object or MinusOne */
    anumber = decode_number(&(hicross->h)); /* FixupData or ExprIndex */

  }
  if (this_byte_and_next(&(hicross->h)) != REFERENCEPARTTAG ) return false;

  /* Here we are ! */
  srcpos->Unit = decode_number(&(hicross->h));
  srcpos->SourceStart = decode_number(&(hicross->h));
  srcpos->SourceEnd = decode_number(&(hicross->h));
  srcpos->NBofSourceCodePairs = decode_number(&(hicross->h));

  *offset = hicross_pos (abfd);

  return true;
}

static void
DEFUN(hicross_swap_proc_source_code_pair_in,(abfd, offset, source_code_pair),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_proc_source_code_pair_type * source_code_pair)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);

  hicross_seek(abfd, *offset);

  source_code_pair->DeltaSourcePos = decode_number(&(hicross->h));
  source_code_pair->DeltaCodePos = decode_number(&(hicross->h));

  *offset = hicross_pos (abfd);

}

/* Get the ObjectSummary pointed by offset in the object file Directory. 
   Leave offset pointing to the beginning of next ObjectSummary or item */

static void
DEFUN(hicross_swap_object_summary_in,(abfd, offset, objectsummary),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_object_summary_type * objectsummary)
{
  unsigned long dummy, i, j;

  hicross_data_type *hicross = HICROSS_DATA(abfd);

  hicross_seek(abfd, *offset);

  if (this_byte_and_next(&(hicross->h)) == 'E')
    next_byte(&(hicross->h)); /* Remove leading '_' character */
  objectsummary->name = read_name(&(hicross->h));
  while (this_byte_and_next(&(hicross->h)) != 0); /* ObjectType */
  dummy = decode_number(&(hicross->h)); /* ObjectSizeNUMBER */
  objectsummary->kind = this_byte_and_next(&(hicross->h));
  switch (objectsummary->kind) {
    case VARIABLETAG :
      dummy = decode_number(&(hicross->h)); /* NUMBEROfInitParts */
      dummy = decode_number(&(hicross->h)); /* TotalSizeOfAllInitPartsNUMBER */
      dummy = decode_number(&(hicross->h)); /* NUMBEROfReferencedNonLocals */
      /* skip referenced */
      for (i = 0; i < dummy; i++) {
        unsigned long anumber;
        anumber = decode_number(&(hicross->h)); /* ObjectNUMBER */
      }
      break;
    case STRINGTAG :
      break;
    case PROCEDURETAG :
      dummy = decode_number(&(hicross->h)); /* FrameOffsetNUMBER */
      dummy = decode_number(&(hicross->h)); /* NUMBEROfReferencedNonLocals */
      /* skip referenced */
      for (i = 0; i < dummy; i++) {
        unsigned long anumber;
        anumber = decode_number(&(hicross->h)); /* ObjectNUMBER */
      }
      break;
    case LABELTAG :
      dummy = decode_number(&(hicross->h)); /* ObjectNUMBER */
      dummy = decode_number(&(hicross->h)); /* OffsetNUMBER */
      break;
    case CONSTTAG :
      dummy = decode_number(&(hicross->h)); /* ConstNUMBER */
      break;
    default : return;
  }
  dummy = decode_number(&(hicross->h)); /* ObjectSegmentNUMBER */
  objectsummary->FilePosition = decode_number(&(hicross->h));
  dummy = decode_number(&(hicross->h)); /* NUMBEROfAdditionalBytes */
  for (i = 0; i < dummy; i++) next_byte(&(hicross->h)); /* Byte */
  *offset = hicross_pos (abfd);
}

/* Start from the end of source position and get local variables for
   the procedure :
     first: start index of static objects local to the procedure.
     last: end index of static objects local to the procedure. 
   (first > last) means no static object has been found. */

static void
DEFUN(hicross_swap_proc_locals_in,(abfd, offset, proc_info),
      bfd * abfd AND
      unsigned long * offset AND
      hicross_proc_locals_type * proc_info)
{
  unsigned long dummy, i;

  hicross_data_type *hicross = HICROSS_DATA(abfd);

  proc_info->first = 0xFFFFFFFF;
  proc_info->last = 0; /* initialized to 'no static object found' */

  hicross_seek(abfd, *offset);

  if (this_byte_and_next(&(hicross->h)) != SYMBOLTABLETAG ) return;
  dummy = decode_number(&(hicross->h));
  dummy = decode_number(&(hicross->h));
  /* parse object area */
  for (i = 0; i < dummy; i++) {
    unsigned long anumber;
    boolean isbitfield;

    while (this_byte_and_next(&(hicross->h)) != 0); /* skip name */
    switch (this_byte_and_next(&(hicross->h))) {
      case 'S':
        anumber = decode_number(&(hicross->h)); /* Object Index */
		if (anumber < proc_info->first) proc_info->first = anumber;
		if (anumber > proc_info->last) proc_info->last = anumber;
        anumber = decode_number(&(hicross->h)); /* Size */
        break;
      case 'L':
      case 'F':
        anumber = decode_number(&(hicross->h)); /* Offset */
        anumber = decode_number(&(hicross->h)); /* Size */
        break;
      case 'B':
        anumber = decode_number(&(hicross->h)); /* Offset */
        anumber = decode_number(&(hicross->h)); /* Size */
        anumber = decode_number(&(hicross->h)); /* BitFieldWidth */
        anumber = decode_number(&(hicross->h)); /* BitLSB */
		break;
      default: return;
    }
    while (this_byte_and_next(&(hicross->h)) != 0); /* skip objecttype */
  }
  
  *offset = hicross_pos (abfd);
}

static void
DEFUN(hicross_swap_proc_SP_PC_list_in,(abfd, offset, proc_info),
      bfd * abfd AND
      unsigned long * offset AND
      hicross_proc_PC_SP_list_type * proc_info)
{
  unsigned long dummy, i, j;
  hicross_proc_PC_SP_pair_type *PC_SP_pairs;

  hicross_data_type *hicross = HICROSS_DATA(abfd);
  hicross_seek(abfd, *offset);
 
  /* skip optimize */
  dummy = decode_number(&(hicross->h));
  for (i = 0; i < dummy; i++) {
    unsigned long anumber;
    while (this_byte_and_next(&(hicross->h)) != 0); /* skip name */
    anumber = decode_number(&(hicross->h)); /* Register */
    anumber = decode_number(&(hicross->h)); /* CodeBeg */
    anumber = decode_number(&(hicross->h)); /* CodeEnd */
  }
  dummy = decode_number(&(hicross->h));
  for (i = 0; i < dummy; i++) {
    unsigned long anumber;
    anumber = decode_number(&(hicross->h)); /* Reg */
    anumber = decode_number(&(hicross->h)); /* OffsetToFP */
  }

  /* skip NestedProcPart */
  dummy = decode_number(&(hicross->h));
  for (i = 0; i < dummy; i++) {
    unsigned long anumber;
    while (this_byte_and_next(&(hicross->h)) != 0); /* skip name */
    anumber = decode_number(&(hicross->h)); /* CodeBeg */
    anumber = decode_number(&(hicross->h)); /* CodeEnd */
    anumber = decode_number(&(hicross->h)); /* SrcBeg */
    anumber = decode_number(&(hicross->h)); /* SrcEnd */
    anumber = decode_number(&(hicross->h)); /* FrameOffset */
    /* skip symbol table */
    if (this_byte_and_next(&(hicross->h)) != SYMBOLTABLETAG ) return;
    dummy = decode_number(&(hicross->h));
    dummy = decode_number(&(hicross->h));
    /* skip object area */
    for (j = 0; j < dummy; j++) {
      unsigned long anothernumber;
      boolean isbitfield;

      while (this_byte_and_next(&(hicross->h)) != 0); /* skip name */
      isbitfield = (this_byte_and_next(&(hicross->h)) == 'B');
      anothernumber = decode_number(&(hicross->h)); /* Offset */
      anothernumber = decode_number(&(hicross->h)); /* Size */
      if (isbitfield) {
        anothernumber = decode_number(&(hicross->h)); /* BitFieldWidth */
        anothernumber = decode_number(&(hicross->h)); /* BitLSB */
      }
      while (this_byte_and_next(&(hicross->h)) != 0); /* skip objecttype */
    }
  }

  /* skip NestedBlockInfo */
  dummy = decode_number(&(hicross->h));
  for (i = 0; i < dummy; i++) {
    unsigned long anumber;
    anumber = decode_number(&(hicross->h)); /* SrcPosDeclBeg */
    anumber = decode_number(&(hicross->h)); /* SrcPosDeclEnd */
    anumber = decode_number(&(hicross->h)); /* CodePosDeclBeg */
    anumber = decode_number(&(hicross->h)); /* CodePosDeclEnd */
    anumber = decode_number(&(hicross->h)); /* CodePosBlockEnd */
  }

  /* SP_PC_Pairs */
  proc_info->NBofPC_SP_pairs = decode_number(&(hicross->h));
  if (proc_info->NBofPC_SP_pairs)
	{
	  proc_info->PC_SP_pairs =
	    (hicross_proc_PC_SP_pair_type *)
	      xmalloc(proc_info->NBofPC_SP_pairs * sizeof(hicross_proc_PC_SP_pair_type));
      PC_SP_pairs = proc_info->PC_SP_pairs;
      for (i = 0; i < proc_info->NBofPC_SP_pairs; i++) {
        PC_SP_pairs->PC = decode_number(&(hicross->h)); /* PC */
        PC_SP_pairs->SP = decode_number(&(hicross->h)); /* SPoffsetToReturn */
	    PC_SP_pairs++;
	  }
	}
  
  *offset = hicross_pos (abfd);
}

static void
DEFUN(hicross_swap_enum_in,(abfd, offset, enumeration),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_enumeration_type * enumeration)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
 
  hicross_seek(abfd, *offset);
  enumeration->NBofPairs = decode_number(&(hicross->h));
  *offset = hicross_pos (abfd);
}

static void
DEFUN(hicross_swap_struct_union_in,(abfd, offset, struct_union),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_struct_union_type * struct_union)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
 
  hicross_seek(abfd, *offset);
  if (this_byte_and_next(&(hicross->h)) != SYMBOLTABLETAG ) return;
  struct_union->TotalSize = decode_number(&(hicross->h));
  struct_union->NBofObjects = decode_number(&(hicross->h));
  *offset = hicross_pos (abfd);
}

static void
DEFUN(hicross_swap_name_value_pair_in,(abfd, offset, enumeration_pair),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_name_value_pair_type * enumeration_pair)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
 
  hicross_seek(abfd, *offset);
  enumeration_pair->Name = read_name(&(hicross->h));
  enumeration_pair->Value = decode_number(&(hicross->h));
  *offset = hicross_pos (abfd);
}

static void
DEFUN(hicross_swap_field_in,(abfd, offset, field),
      bfd *abfd AND
      unsigned long * offset AND
      hicross_field_type * field)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
 
  hicross_seek(abfd, *offset);
  field->ObjectName = read_name(&(hicross->h));
  switch (this_byte_and_next(&(hicross->h))) {
    case 'F': field->bitfield = false;
              break;
    case 'B': field->bitfield = true;
              break;
    default : return;
  }
  field->Offset = decode_number(&(hicross->h));
  field->Size = decode_number(&(hicross->h));
  if (field->bitfield) {
    field->BitFieldWidth = decode_number(&(hicross->h));
    field->LeasSignificantBit = decode_number(&(hicross->h));
  }
  field->ObjectType = hicross_swap_object_type (hicross);
  *offset = hicross_pos (abfd);

}

static asymbol *
DEFUN(hicross_make_empty_symbol, (abfd),
      bfd*abfd)
{
  asymbol *new=  (asymbol *)bfd_zalloc (abfd, sizeof (asymbol));
  new->the_bfd = abfd;
  return new;
}

/* This is the HI-CROSS backend structure.  The backend field of the
 * target vector points to this.
 */

static const struct hicross_backend_data hicross_backend_data =
{
  /* Supported architecture.  */
  bfd_arch_st7,
  {
    /* Functions to swap in external symbolic data.  */
    hicross_swap_module_in,
    hicross_swap_object_in,
    hicross_swap_unit_in,
    hicross_swap_proc_source_position_in,
	hicross_swap_proc_source_code_pair_in,
    hicross_swap_object_summary_in,
	hicross_swap_proc_locals_in,
	hicross_swap_proc_SP_PC_list_in,
    hicross_swap_objtype_in,
	hicross_swap_enum_in,
	hicross_swap_name_value_pair_in,
    hicross_swap_struct_union_in,
    hicross_swap_field_in 
  }
};


/* Default or unused standard target routines (see bfd.h) */
#define FOO PROTO
#define hicross_core_file_failing_command (char *(*)())(bfd_nullvoidptr)
#define hicross_core_file_failing_signal (int (*)())bfd_0
#define hicross_core_file_matches_executable_p ( FOO(boolean, (*),(bfd *, bfd *)))bfd_false
#define hicross_slurp_armap bfd_true
#define hicross_slurp_extended_name_table bfd_true
#define hicross_truncate_arname (void (*)())bfd_nullvoidptr
#define hicross_write_armap  (FOO( boolean, (*),(bfd *, unsigned int, struct orl *, unsigned int, int))) bfd_nullvoidptr
#define hicross_close_and_cleanup bfd_generic_close_and_cleanup
#define hicross_set_section_contents (FOO( boolean, (*),(bfd *, sec_ptr, PTR, file_ptr, bfd_size_type))) bfd_false
#define hicross_new_section_hook _bfd_dummy_new_section_hook
#define hicross_get_reloc_upper_bound (FOO( unsigned int, (*),(bfd *, sec_ptr))) bfd_0u 
#define hicross_canonicalize_reloc (FOO( unsigned int, (*),(bfd *, sec_ptr, arelent **,asymbol **))) bfd_0u
#define hicross_print_symbol _bfd_nosymbols_print_symbol
#define hicross_get_symbol_info _bfd_nosymbols_get_symbol_info
#define hicross_bfd_is_local_label_name _bfd_nosymbols_bfd_is_local_label_name
#define hicross_get_lineno _bfd_nosymbols_get_lineno
#define hicross_set_arch_mach bfd_default_set_arch_mach
#define hicross_openr_next_archived_file ( FOO(bfd *, (*),(bfd *, bfd *)))(bfd_nullvoidptr) 
#define hicross_find_nearest_line (FOO( boolean, (*),(bfd *, struct sec *, asymbol **, bfd_vma, CONST char **, CONST char **, unsigned int *))) bfd_false
#define hicross_generic_stat_arch_elt ( FOO(int, (*),(bfd *, struct stat *)))bfd_0
#define hicross_sizeof_headers ( FOO(int, (*),(bfd *, boolean)))bfd_0
#define hicross_bfd_debug_info_start bfd_void
#define hicross_bfd_debug_info_end bfd_void
#define hicross_bfd_debug_info_accumulate (FOO( void, (*),(bfd *, struct sec *))) bfd_void
#define hicross_bfd_get_relocated_section_contents  bfd_generic_get_relocated_section_contents
#define hicross_bfd_relax_section bfd_generic_relax_section
#define hicross_bfd_reloc_type_lookup \
  ((CONST struct reloc_howto_struct *(*) PARAMS ((bfd *, bfd_reloc_code_real_type))) bfd_nullvoidptr)
#define hicross_bfd_make_debug_symbol \
  ((asymbol *(*) PARAMS ((bfd *, void *, unsigned long))) bfd_nullvoidptr)
#define hicross_bfd_link_hash_table_create _bfd_generic_link_hash_table_create
#define hicross_bfd_link_add_symbols _bfd_generic_link_add_symbols
#define hicross_bfd_final_link _bfd_generic_final_link
#define hicross_bfd_is_local_label _bfd_nosymbols_bfd_is_local_label
#define hicross_read_minisymbols _bfd_nosymbols_read_minisymbols
#define hicross_minisymbol_to_symbol _bfd_nosymbols_minisymbol_to_symbol
#define hicross_bfd_link_split_section _bfd_nolink_bfd_link_split_section 


/* Check format routine */


static boolean
DEFUN(hicross_get_section_contents,(abfd, section, location, offset, count),
      bfd *abfd AND
      sec_ptr section AND
      PTR location AND
      file_ptr offset AND
      bfd_size_type count)
{
  hicross_seek(abfd, section->filepos);
  (void)  memcpy((PTR)location,
                 (PTR)(HICROSS_DATA(abfd)->h.input_p + offset),
                 (unsigned)count);
  return true;
}

static asection *
DEFUN(get_section_entry,(abfd, hicross,index),
 bfd *abfd AND
     hicross_data_type *hicross AND
      unsigned int index)
{
  asection *section;
    char *tmp = bfd_alloc(abfd,11);
    sprintf(tmp," fsec%4d", index);
    section = bfd_make_section(abfd, tmp);
    section->flags = SEC_NO_FLAGS;
    section->target_index = index;
  return section;
}

static void
DEFUN(hicross_slurp_sections,(abfd),
      bfd *abfd)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
  unsigned short section_size;
  asection *section = (asection *)NULL;
  char *name;
  unsigned int section_index = 0 ;

  hicross_seek(abfd, hicross->section_file_offset);

  section_size = read_word (&(hicross->h));

    while (section_size > 0) {
      section = get_section_entry(abfd, hicross, section_index);
      section->flags = SEC_LOAD | SEC_ALLOC | SEC_HAS_CONTENTS;
    
      section->_raw_size = section_size;
      section->vma = read_long(&(hicross->h));
      section->lma = section->vma;
      section->filepos = hicross_pos(abfd);
      /* skip the datas */
      hicross_skip(abfd, section_size);

      section_index++;
      section_size = read_word (&(hicross->h));
    }
}

static boolean
DEFUN(hicross_mkobject,(abfd),
      bfd *abfd)
{ 
abfd->tdata.hicross_data = (hicross_data_type *)bfd_zalloc(abfd,sizeof(hicross_data_type));
  

  return true;
}

/* Get the absolute module directory information located at the end of the file. */
void
DEFUN(slurp_absolute_module_dir,(abfd),
      bfd *abfd)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
  unsigned long dirsize;
  unsigned long dummy;
  hicross_seek(abfd, hicross->file_size - 4);
  dirsize = read_long(&(hicross->h));
  hicross_seek(abfd, hicross->file_size - dirsize);

  if (this_byte_and_next(&(hicross->h)) != ABSFILETAG) return;
  switch (this_byte_and_next(&(hicross->h))) {
    case ROMLIBTAG :
      dummy = decode_number (&(hicross->h));
      break;
    case PROGRAMTAG :
      dummy = decode_number (&(hicross->h));
      dummy = decode_number (&(hicross->h));
      break;
    default :
      return;
  }

  hicross->typesizes_offset = decode_number(&(hicross->h));
  hicross->hm.Target = this_byte_and_next(&(hicross->h));
  hicross->hm.MemModel = this_byte_and_next(&(hicross->h));
  hicross->hm.CodeBeg = decode_number (&(hicross->h));
  hicross->hm.CodeEnd = decode_number (&(hicross->h));
  hicross->hm.DataBeg = decode_number (&(hicross->h));
  hicross->hm.DataEnd = decode_number (&(hicross->h));
  hicross->hm.NBofModules = decode_number (&(hicross->h));
  hicross->hm.TotalNBofObjects = decode_number (&(hicross->h));
  hicross->module_list_file_offset = hicross_pos (abfd);
}

/* Get the object module directory information located at the end of the file. */
void
DEFUN(slurp_object_module_dir,(abfd),
      bfd *abfd)
{
  hicross_data_type *hicross = HICROSS_DATA(abfd);
  unsigned long dirsize, i;
  unsigned long dummy;
  unsigned long struct_union_pos, enumeration_pos;

  hicross_seek(abfd, hicross->file_size - 4);
  dirsize = read_long(&(hicross->h));
  hicross_seek(abfd, hicross->file_size - dirsize);

  if (this_byte_and_next(&(hicross->h)) != DIRECTORYTAG) return;

  enumeration_pos = decode_number(&(hicross->h));
  struct_union_pos = decode_number(&(hicross->h));
  hicross->typesizes_offset = decode_number(&(hicross->h));
  hicross->hd.Model = this_byte_and_next(&(hicross->h));
  hicross->hd.ProgrammingLanguage = this_byte_and_next(&(hicross->h));
  hicross->hd.Processor = this_byte_and_next(&(hicross->h));

  /* Unit list */
  if (this_byte_and_next(&(hicross->h)) != UNITLISTTAG) return;
  hicross->hd.NBofUnits = decode_number (&(hicross->h));
  hicross->unit_list_file_offset = hicross_pos (abfd);
  for (i = 0; i < hicross->hd.NBofUnits; i++) {
    unsigned long anumber;

    while (this_byte_and_next(&(hicross->h)) != 0); /* skip UnitName */
    anumber = decode_number(&(hicross->h)); /* UnitKey */
  }

  /* Segment list */
  if (this_byte_and_next(&(hicross->h)) != SEGMENTLISTTAG) return;
  hicross->hd.NBofSegments = decode_number (&(hicross->h));
  for (i = 0; i < hicross->hd.NBofSegments; i++) {
    while (this_byte_and_next(&(hicross->h)) != 0); /* skip SegmentName */
	next_byte(&(hicross->h)); /* Segment type */
	next_byte(&(hicross->h)); /* Segment attribute */
  }

  /* Object list */
  if (this_byte_and_next(&(hicross->h)) != OBJLISTTAG) return;
  hicross->hd.NBofObjects = decode_number (&(hicross->h));
  hicross->object_list_file_offset = hicross_pos (abfd);

  hicross_seek(abfd, enumeration_pos);
  if (this_byte_and_next(&(hicross->h)) != ENUMERATIONTAG) return;
  hicross->hd.NBofEnumerations = decode_number (&(hicross->h));
  hicross->enumeration_offset = hicross_pos (abfd);

  hicross_seek(abfd, struct_union_pos);
  if (this_byte_and_next(&(hicross->h)) != STRUCTUNIONTAG) return;
  hicross->hd.NBofStructUnions = decode_number (&(hicross->h));
  hicross->struct_union_offset = hicross_pos (abfd);
}

const bfd_target *
DEFUN(hicross_object_p,(abfd),
      bfd *abfd)
{
  short processor;
  hicross_data_type *hicross;
  hicross_data_type *save = HICROSS_DATA(abfd);
  unsigned char buffer[300];

  int buffer_size;

  abfd->tdata.hicross_data = 0;
  hicross_mkobject(abfd);
  
  hicross = HICROSS_DATA(abfd);
  hicross->h.abfd = abfd;
  hicross->file_size = bfd_get_size (abfd);

  /* Don't read more bytes than available. */
  if (hicross->file_size < sizeof (buffer))
	buffer_size = hicross->file_size;
  else
	buffer_size = sizeof (buffer);

  bfd_seek(abfd, (file_ptr) 0, SEEK_SET);
  /* Read the first few bytes in to see if it makes sense */
#if 1
  if (buffer_size != bfd_read((PTR)buffer, 1, buffer_size, abfd))
    goto got_wrong_format;
#else
  if (sizeof (buffer) != bfd_read((PTR)buffer, 1, sizeof(buffer), abfd))
    goto got_wrong_format;
#endif

  /* Header parsing */
  hicross->h.input_p = buffer;
  if (this_byte_and_next(&(hicross->h)) != HICROSSTAG) goto fail;

  hicross->read_symbols= false;
  hicross->read_data= false;
  hicross->hh.FileId = this_byte_and_next(&(hicross->h));
  hicross->hh.ProgVersion = read_word(&(hicross->h));
  hicross->hh.FormatVers = read_word(&(hicross->h));
  hicross->hh.Flag = read_word(&(hicross->h));
  hicross->hh.ProcFamily = read_word(&(hicross->h));
  if ((hicross->hh.ProcFamily == ST7_PROCESSOR_FAMILY)
		/* support Motorola MC68HC05 files */
      || (hicross->hh.ProcFamily == MC68HC05_PROCESSOR_FAMILY))
	compiler_target = ST7_CORE;
  else {
	compiler_target = NO_CORE;
    goto fail;
  }
  hicross->hh.ProcType = read_word(&(hicross->h));
  hicross->hh.Unit = read_name(&(hicross->h));
  if (abfd->filename == (CONST char *)NULL) {
    abfd->filename =  hicross->hh.Unit;
  }
  hicross->hh.User = read_name(&(hicross->h));
  hicross->hh.ProgramDate = read_name(&(hicross->h));
  hicross->hh.Time = read_name(&(hicross->h));
  hicross->hh.Copyright = read_name(&(hicross->h));

  /* By now we know that this is a real HICROSS file, we're going to read
   the whole thing into memory so that we can run up and down it
   quickly.
  */
  hicross->h.first_byte = (unsigned char *) bfd_alloc(hicross->h.abfd,
                                                      hicross->file_size);
  bfd_seek(abfd, (file_ptr) 0, SEEK_SET);
  bfd_read((PTR)(hicross->h.first_byte), 1, hicross->file_size, abfd);

  if (hicross->hh.FileId == FILEID_ABSOLUTE) {
    abfd->flags = HAS_SYMS | EXEC_P;

    hicross->StartAddress = read_long(&(hicross->h));
    abfd->start_address = hicross->StartAddress;

    /* remember the section file offset on which we are now */
    hicross->section_file_offset = hicross->h.input_p - buffer;

    hicross_slurp_sections(abfd);

    hicross->emap_file_offset = hicross_pos (abfd);

    /* Now get the module dir informations located at the end of the file */
    slurp_absolute_module_dir(abfd);

  }

  else if (hicross->hh.FileId == FILEID_OBJECT) {
    abfd->flags = HAS_LINENO | HAS_RELOC;

    /* remember the section file offset on which we are now */
    hicross->procedure_file_offset = hicross->h.input_p - buffer;

    /* Now get the module dir informations located at the end of the file */
    slurp_object_module_dir(abfd);
  }
  else goto fail;
  

  return abfd->xvec;
got_wrong_format:
  bfd_set_error (bfd_error_wrong_format);
fail:
  (void)  bfd_release(abfd, hicross);
  abfd->tdata.hicross_data = save;
  return (bfd_target *)NULL;

}

bfd_target hicross_vec =
{
  "hicross",			/* name */
  bfd_target_hicross_flavour,
  BFD_ENDIAN_BIG,				/* target byte order */
  BFD_ENDIAN_BIG,				/* target headers byte order */
  (HAS_RELOC | EXEC_P |		/* object flags */
   HAS_LINENO | HAS_DEBUG |
   HAS_SYMS | HAS_LOCALS | WP_TEXT | D_PAGED),
  ( SEC_CODE|SEC_DATA|SEC_ROM|SEC_HAS_CONTENTS
   |SEC_ALLOC | SEC_LOAD | SEC_RELOC), /* section flags */
   0,				/* no leading underscore */
  ' ',				/* ar_pad_char */
  16,				/* ar_max_namelen */
bfd_getb64, bfd_getb_signed_64, bfd_putb64,
    bfd_getb32, bfd_getb_signed_32, bfd_putb32,
    bfd_getb16, bfd_getb_signed_16, bfd_putb16, /* data */
bfd_getb64, bfd_getb_signed_64, bfd_putb64,
    bfd_getb32, bfd_getb_signed_32, bfd_putb32,
    bfd_getb16, bfd_getb_signed_16, bfd_putb16, /* hdrs */

  { _bfd_dummy_target,
     hicross_object_p,		/* bfd_check_format */
     _bfd_dummy_target,
    _bfd_dummy_target,
     },
  {
    bfd_false,
    bfd_false, 
    bfd_false,
    bfd_false
    },
  {
    bfd_false,
    bfd_false,
    bfd_false,
    bfd_false,
  },
   BFD_JUMP_TABLE_GENERIC (_bfd_generic),
   BFD_JUMP_TABLE_COPY (_bfd_generic),
   BFD_JUMP_TABLE_CORE (_bfd_nocore),
   BFD_JUMP_TABLE_ARCHIVE (_bfd_noarchive),
   BFD_JUMP_TABLE_SYMBOLS (hicross),
   BFD_JUMP_TABLE_RELOCS (_bfd_norelocs),
   BFD_JUMP_TABLE_WRITE (_bfd_generic),
   BFD_JUMP_TABLE_LINK (hicross),
   BFD_JUMP_TABLE_DYNAMIC (_bfd_nodynamic),
  (PTR) &hicross_backend_data
};

