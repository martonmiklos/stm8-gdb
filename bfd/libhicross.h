/*	Copyright (C) 1998, 1999, 2000, 2001, 2002, 2003, 2004 STMicroelectronics	*/

/* This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. */

/* HI-CROSS object file formats:  definitions internal to BFD.
This file is part of BFD, the Binary File Descriptor library. */



/* This is the backend information kept for HI-CROSS files.  This
   structure is constant for a particular backend. */

#define HICROSS_BACKEND(abfd) \
  ((struct hicross_backend_data *) (abfd)->xvec->backend_data)

struct hicross_backend_data
{
  /* Supported architecture.  */
  enum bfd_architecture arch;
  /* How to swap debugging information.  */
  struct hicross_debug_swap debug_swap;
};


typedef struct hicross_symbol 
{
  asymbol symbol;
  struct hicross_symbol *next;

  unsigned int index;
} hicross_symbol_type;


typedef struct {
  unsigned  char *input_p;
  unsigned char *first_byte;
  bfd *abfd;
} common_header_type ;


typedef struct hicross_data_struct
{
  common_header_type h;
  hicross_header_type hh;
  unsigned long StartAddress;
  boolean read_symbols;
  boolean read_data;
  unsigned long file_size;

  /* List of GLOBAL EXPORT symbols */
  hicross_symbol_type *external_symbols;

  unsigned long typesizes_offset;


  /* absolute file informations */	
  unsigned long section_file_offset;	
  unsigned long emap_file_offset;	
  /* moduledir HI-CROSS absolute file informations */
  unsigned long module_list_file_offset;
  hicross_moduledir_type hm;

  /* object file informations */
  unsigned long procedure_file_offset;	
  unsigned long enumeration_offset;
  unsigned long struct_union_offset;
  /* directory HI-CROSS object file informations */
  unsigned long unit_list_file_offset;
  unsigned long object_list_file_offset;
  hicross_directory_type hd;

  
  struct hicross_debug_info debug_info;
} hicross_data_type;


#define HICROSS_DATA(abfd) ((abfd)->tdata.hicross_data)
#define HICROSS_AR_DATA(abfd) ((abfd)->tdata.hicross_ar_data)

#define ptr(abfd) (hicross_data(abfd)->input_p)
